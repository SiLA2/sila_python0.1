
# **SiLA Python** for **Raspberry Pi**  

This is the **SiLA Python** for **Raspberry Pi** section.

## Installation of the Python SiLA library on the Pi

sudo apt install sila2lib

## Quicktest

To test that the silalib is installed, start python3

    python3
    >>>import sila2lib
    
    # no error should appear

## Quickstart

running your first SiLA server-client

1. clone the sila_python repository

    git clone https://gitlab.com/sila_python

2. change directory to

    cd sila_python/examples/HelloSiLA

    python3 Hello_sila_server.py
    
    in another shell:
    
    python3 Hello_sila_client.py
        
## Remote connection to the Pi

    

