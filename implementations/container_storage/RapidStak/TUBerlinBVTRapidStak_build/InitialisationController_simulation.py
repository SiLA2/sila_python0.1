"""
________________________________________________________________________

:PROJECT: SiLA2_python

*initialisationcontroller_server_simulation *

:details: initialisationcontroller_server_simulation: 
        This Feature describes the Initialisation behaviour of a SiLA Server. It is kept as general as possible to cover as many use-cases as possible.
        Currently a list of strings can be transferred as init parameters.
        In the future a structure might be chosen to achieve more flexibility.
    . 
           
:file:    initialisationcontroller_server_simulation.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-06-08T22:13:59.070081
:date: (last modification) 2019-06-08T22:13:59.070081

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.5"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import InitialisationController_pb2 as pb2
import InitialisationController_pb2_grpc as pb2_grpc


class InitialisationControllerSimulation():
    """ InitialisationControllerSimulation - 
#        This Feature describes the Initialisation behaviour of a SiLA Server. It is kept as general as possible to cover as many use-cases as possible.
#        Currently a list of strings can be transferred as init parameters.
#        In the future a structure might be chosen to achieve more flexibility.
#     """
    def __init__ (self):
        """ InitialisationControllerSimulation class initialiser """
        logging.debug("init class: InitialisationControllerSimulation ")



    def InitialiseServer(self, request, context):
        """This Initialises the Server.
            :param request: gRPC request
            :param context: gRPC context
            :param request.InitParameterList: List of Parameters (currently Stings only) used for initialisation.

        """
        logging.debug("InitialiseServer - Mode: simulation ")

        #~ return_val = request.InitParameterList.value
        #~ return pb2.InitialiseServer_Responses(ServerInitialised=fwpb2.Boolean(value=False))

    def Get_InitialisationStatus(self, request, context):
        """Status of Initialisation process of Service.
            :param request: gRPC request
            :param context: gRPC context
            :param response.InitialisationStatus: Status of Initialisation process of Service.

        """
        logging.debug("Get_InitialisationStatus - Mode: simulation ")

        #~ return_val = request.InitialisationStatus.value
        #~ return pb2.InitialisationStatus_Responses( InitialisationStatus=pb2.DataType_InitialisationStatus(InitialisationStatus=fwpb2.String(value="DEFAULTstring" + return_val)) )




