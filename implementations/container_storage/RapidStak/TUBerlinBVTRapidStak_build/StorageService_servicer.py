"""
________________________________________________________________________

:PROJECT: SiLA2_python

*storageservice_server_simulation *

:details: storageservice_server_simulation: This Service provides commands and informations neede to control a storage device. 
           
:file:    storageservice_server_simulation.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-06-08T22:13:59.070081
:date: (last modification) 2019-06-08T22:13:59.070081

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.5"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import StorageService_pb2 as pb2
import StorageService_pb2_grpc as pb2_grpc
import RapidStak

class StorageService(pb2_grpc.StorageServiceServicer):
    """ StorageService - This Service provides commands and informations neede to control a storage device """
    def __init__ (self):
        """ StorageService class initialiser """
        logging.debug("init class: StorageService ")
        self.AttainablePositions    = 2;
        self.HandoverNumber         = 2
        self.LabwareInformation      = ([1, 3]);
        logging.info("Start StorageService with: AttainablePositions = {},  HandoverNumbers = {}".format(self.AttainablePositions, self.HandoverNumber))
        logging.info("Default Labware is set to: Pos 1: {}, Pos 2: {}".format(self.LabwareInformation[0], self.LabwareInformation[1]))
        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())

        #self.implementation = None # this corresponds to the simple, fallback simulation mode
        self.RS = RapidStak.Control()
        self.implementation = True

    # dependency injection - setter injection s. https://en.wikipedia.org/wiki/Dependency_injection
    def injectImplementation(self, implementation):
        self.implementation = implementation

    def GetLabwareInformation(self, request, context):
        """Which labware is mounted in the differnt tower of the device
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Retuns the information of the labware at the requested position as Labware Type according to the SiLA Labware Specification Standard (Version 1.0)

        """
        logging.debug("GetLabwareInformation")

        if self.implementation is not None:
            requestedPos = request.AttainablePosition.value - 1
            logging.info("Request LabwareInformation for AttainablePosition {}".format(requestedPos))
            if requestedPos < self.AttainablePositions:
                logging.info("Request LabwareInformation for AttainablePosition {}: {}".format(requestedPos, self.LabwareInformation[requestedPos]))
                return pb2.GetLabwareInformation_Responses( LabwareType=fwpb2.Integer(value=self.LabwareInformation[requestedPos]))
                pass
            else:
                logging.warning("Requested position out of range")
        else:
            #pass #~ return_val = request.AttainablePosition.value

            #retVal = 
            #return pb2.GetLabwareInformation_Responses( LabwareType=fwpb2.Integer(value=0) )
            pass
            
    def SetLabwareInformation(self, request, context):
        """Which labware is mounted in the different tower of the device
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Sets the information of the labware of the at the addressed position as Labware Type according to the SiLA Labware Specification Standard (Version 1.0)

        """
        logging.debug("SetLabwareInformation - Mode: simulation ")

        if self.implementation is not None:
            requestedPos = request.AttainablePosition.value - 1
            if requestedPos < self.AttainablePositions:
                logging.info("Set LabwareInformation for AttainablePosition {} to {}".format(requestedPos, request.HandoverNumber.value))
                self.LabwareInformation[requestedPos] = request.HandoverNumber.value;
                logging.debug("succes")
                pass
            else: 
                logging.warning("Requested position out of range")
                pass
        else:
            pass #~ return_val = request.AttainablePosition.value
            #~ return pb2.SetLabwareInformation_Responses(  )

    def GetFromPosition(self, request, context):
        """Get a Labware from the specified position and presents it to the addressed handover position
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Postion of the labware in the storage device

        """
        logging.debug("GetFromPosition - Mode: simulation ")

        if self.implementation is not None:
            logging.debug(request.AttainablePosition.value)
            # parse input
            inputVar = str(request.AttainablePosition.value)
            AttainablePos = int(inputVar[0])
            HandoverPos = int(inputVar[1])
            logging.info("Request Plate from AttainablePos: {} to HandoverPos: {}".format(AttainablePos, HandoverPos))
            # command
            if AttainablePos == 1: # Tower A
                if HandoverPos == 1:
                    self.RS.plateFromTowerAtoRight()
                elif HandoverPos == 2:
                    self.RS.plateFromTowerAtoLeft()
                else:
                    logging.warning('AttainablePos position out of range')
            elif AttainablePos == 2: # Tower B
                if HandoverPos == 1:
                    self.RS.plateFromTowerBtoRight()
                elif HandoverPos == 2:
                    self.RS.plateFromTowerBtoLeft()
                else:
                    logging.warning('AttainablePos position out of range')
            else :
                logging.warning('Attainalbe position out of range')
                
            pass
            #return self.implementation.GetFromPosition(request, context)
        else:
            pass #~ return_val = request.AttainablePosition.value
            #~ return pb2.GetFromPosition_Responses(  )

    def PutToPosition(self, request, context):
        """Take a labware from the addressed handover position an stores it at the specified position
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Postion in the storage device where the labware will be stored

        """
        logging.debug("PutToPosition")

        if self.implementation is not None:
            if request.HandoverNumber.value < self.HandoverNumbers and request.AttainablePosition.value < self.AttainablePositions:                
                return self.implementation.PutToPosition(request, context)
        else:
            pass #~ return_val = request.AttainablePosition.value
            #~ return pb2.PutToPosition_Responses(  )

    def Get_HandoverNumber(self, request, context):
        """The number of positions where a labware could be committed
            :param request: gRPC request
            :param context: gRPC context
            :param response.HandoverNumber: The number of positions where a labware could be committed

        """
        logging.debug("Get_HandoverNumber = {}".format(self.HandoverNumbers))
        #~ return_val = request.HandoverNumber.value
        return pb2.HandoverNumber_Responses( HandoverNumber=fwpb2.Integer(value=self.HandoverNumbers) )

    def Get_AttainablePositions(self, request, context):
        """The number of positions which could be attained directly by the device
            :param request: gRPC request
            :param context: gRPC context
            :param response.AttainablePositions: The number of positions which could be attained directly by the device

        """
        logging.debug("Get_AttainablePositions = {]".format(self.AttainablePositions))
        return pb2.AttainablePositions_Responses( AttainablePositions=fwpb2.Integer(value=self.AttainablePositions) )




