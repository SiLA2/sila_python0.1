#!/usr/bin/env python
import os
import sys
import logging
import serial

class Control:
    def __init__(self):
        self.connect()
           
    def connect(self):
        if ~hasattr(self, 'ser'):
            self.ser = serial.Serial()
            self.ser.baudrate = 9600
            self.ser.port = 'COM1'
            self.ser.timeout = 10
            self.ser.open()
            logging.info("port name: {}".format(self.ser.name))         # check which port was really used
            self.ser.write(b'host\r\n')
            logging.info("Host name: {}".format(self.ser.readline()))
            self.ser.write(b'home\r\n')
        
    def moveToHome(self):
        self.connect()
        self.ser.write(b'home\r\n')
        logging.info("Executing: Move to Home...".format(self.ser.readline()))

    def reboot(self):
        self.ser.write(b'reboot\r\n')
        self.ser.readline()
        self.ser.close()
        del self.ser

    def stop(self):
        self.ser.write(b'stop\r\n')
        self.ser.readline()
        #ser.close()

    def getPlateFromStackA(self):
        logging.info(b'Get Plate from Stacker A')
        #self.ser.write(b'home\r\n')
        #self.ser.readline()
        #move to home pos.
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x1\r\n')
        self.ser.readline()

        #load a plate
        self.ser.write(b'move yf\r\n')
        self.ser.readline()
        self.ser.write(b'move cam unstack\r\n')
        self.ser.readline()

        #plate to home
        self.ser.write(b'move yp\r\n')
        self.ser.readline()
        #plate pickup
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zc\r\n')
        self.ser.readline()
        self.ser.write(b'grip close\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()

        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()

    def getPlateFromStackB(self):
        logging.info(b'Get Plate from Stacker B')
        #self.ser.write(b'home\r\n')
        #self.ser.readline()

        #move to home pos.
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x1\r\n')
        self.ser.readline()

        #load a plate
        self.ser.write(b'move yb\r\n')
        self.ser.readline()
        self.ser.write(b'move cam unstack\r\n')
        self.ser.readline()

        #plate to home
        self.ser.write(b'move yp\r\n')
        self.ser.readline()
        #plate pickup
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zc\r\n')
        self.ser.readline()
        self.ser.write(b'grip close\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()

        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()

    def putPlateToStackA(self):
        logging.info(b'Put Plate from Stacker A')
        #move to home pos
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x1\r\n')
        self.ser.readline()
        self.ser.write(b'move yp\r\n')
        self.ser.readline()
        # place plate
        self.ser.write(b'move zc\r\n')
        self.ser.readline()
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        #store a plate
        self.ser.write(b'move yf\r\n')
        self.ser.readline()
        self.ser.write(b'move cam stack\r\n')
        self.ser.readline()

        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()

    def putPlateToStackB(self):
        logging.info(b'Put Plate from Stacker A')
        #move to home pos.
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x1\r\n')
        self.ser.readline()
        self.ser.write(b'move yp\r\n')
        self.ser.readline()
        #place plate
        self.ser.write(b'move zc\r\n')
        self.ser.readline()
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        #store a plate
        self.ser.write(b'move yb\r\n')
        self.ser.readline()
        self.ser.write(b'move cam stack\r\n')
        self.ser.readline()

        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()



    def putPlateToLeft(self):
        logging.info('Put Plate to Left')
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        # move to left position
        self.ser.write(b'move xs1\r\n')
        self.ser.readline()
        self.ser.write(b'move zs1\r\n')
        self.ser.readline()
        self.ser.write(b'move xi1\r\n')
        self.ser.readline()
        self.ser.write(b'move zi1\r\n')
        self.ser.readline()
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zs1\r\n')
        self.ser.readline()
        self.ser.write(b'move xs1\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()
        #self.ser.write(b'home\r\n')

        logging.info('Task is completed')

    def putPlateToRight(self):
        logging.info('Put Plate to Right')
        #move to safe high
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        # move to Right position
        self.ser.write(b'move xs2\r\n')
        self.ser.readline()
        self.ser.write(b'move zs2\r\n')
        self.ser.readline()
        self.ser.write(b'move xi2\r\n')
        self.ser.readline()
        self.ser.write(b'move zi2\r\n')
        self.ser.readline()
        # place plate
        self.ser.write(b'grip open\r\n')
        self.ser.readline()
        self.ser.write(b'move zs2\r\n')
        self.ser.readline()
        self.ser.write(b'move xs2\r\n')
        self.ser.readline()
        self.ser.write(b'move zt\r\n')
        self.ser.readline()
        self.ser.write(b'move x abs 1108\r\n')
        self.ser.readline()

    def plateFromTowerAtoLeft(self):
        self.getPlateFromStackA()
        self.putPlateToLeft()

    def plateFromTowerAtoRight(self):
        self.getPlateFromStackA()
        self.putPlateToRight()

    def plateFromTowerBtoLeft(self):
        self.getPlateFromStackB()
        self.putPlateToLeft()

    def plateFromTowerBtoRight(self):
        self.getPlateFromStackB()
        self.putPlateToRight()

if __name__ == "__main__":
    rapidStack = Control()
    #rapidStack.initialize()
    #rapidStack.moveToHome()
    #rapidStack.reboot()
    #rapidStack.stop()
    #rapidStack.plateFromTowerAtoLeft()
    #rapidStack.plateFromTowerAtoRight()
    #rapidStack.plateFromTowerBtoLeft()
    #rapidStack.plateFromTowerBtoRight()





