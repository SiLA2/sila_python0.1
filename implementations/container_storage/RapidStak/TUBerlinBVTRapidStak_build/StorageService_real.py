"""
________________________________________________________________________

:PROJECT: SiLA2_python

*storageservice_server_real *

:details: storageservice_server_real: This Service provides commands and informations neede to control a storage device. 
           
:file:    storageservice_server_real.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-06-08T22:13:59.070081
:date: (last modification) 2019-06-08T22:13:59.070081

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.5"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import StorageService_pb2 as pb2
import StorageService_pb2_grpc as pb2_grpc


class StorageServiceReal():
    """ StorageServiceReal - This Service provides commands and informations neede to control a storage device """
    def __init__ (self):
        """ StorageServiceReal class initialiser """
        logging.debug("init class: StorageServiceReal ")



    def GetLabwareInformation(self, request, context):
        """Which labware is mounted in the differnt tower of the device
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Retuns the information of the labware at the requested position as Labware Type according to the SiLA Labware Specification Standard (Version 1.0)

        """
        logging.debug("GetLabwareInformation - Mode: real ")

        #~ return_val = request.AttainablePosition.value
        #~ return pb2.GetLabwareInformation_Responses(LabwareType=fwpb2.Integer(value=0))

    def SetLabwareInformation(self, request, context):
        """Which labware is mounted in the different tower of the device
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Sets the information of the labware of the at the addressed position as Labware Type according to the SiLA Labware Specification Standard (Version 1.0)

        """
        logging.debug("SetLabwareInformation - Mode: real ")

        #~ return_val = request.AttainablePosition.value
        #~ return pb2.SetLabwareInformation_Responses()

    def GetFromPosition(self, request, context):
        """Get a Labware from the specified position and presents it to the addressed handover position
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Postion of the labware in the storage device

        """
        logging.debug("GetFromPosition - Mode: real ")

        #~ return_val = request.AttainablePosition.value
        #~ return pb2.GetFromPosition_Responses()

    def PutToPosition(self, request, context):
        """Take a labware from the addressed handover position an stores it at the specified position
            :param request: gRPC request
            :param context: gRPC context
            :param request.AttainablePosition: Postion in the storage device where the labware will be stored

        """
        logging.debug("PutToPosition - Mode: real ")

        #~ return_val = request.AttainablePosition.value
        #~ return pb2.PutToPosition_Responses()

    def Get_HandoverNumber(self, request, context):
        """The number of positions where a labware could be committed
            :param request: gRPC request
            :param context: gRPC context
            :param response.HandoverNumber: The number of positions where a labware could be committed

        """
        logging.debug("Get_HandoverNumber - Mode: real ")

        #~ return_val = request.HandoverNumber.value
        #~ return pb2.HandoverNumber_Responses( HandoverNumber=fwpb2.Integer(value=0) )

    def Get_AttainablePositions(self, request, context):
        """The number of positions which could be attained directly by the device
            :param request: gRPC request
            :param context: gRPC context
            :param response.AttainablePositions: The number of positions which could be attained directly by the device

        """
        logging.debug("Get_AttainablePositions - Mode: real ")

        #~ return_val = request.AttainablePositions.value
        #~ return pb2.AttainablePositions_Responses( AttainablePositions=fwpb2.Integer(value=0) )




