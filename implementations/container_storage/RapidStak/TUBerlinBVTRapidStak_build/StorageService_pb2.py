# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: StorageService.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='StorageService.proto',
  package='sila2.org.silastandard.storing.storageservice.v1',
  syntax='proto3',
  serialized_pb=_b('\n\x14StorageService.proto\x12\x30sila2.org.silastandard.storing.storageservice.v1\x1a\x13SiLAFramework.proto\"_\n GetLabwareInformation_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"W\n\x1fGetLabwareInformation_Responses\x12\x34\n\x0bLabwareType\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"_\n SetLabwareInformation_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"!\n\x1fSetLabwareInformation_Responses\"Y\n\x1aGetFromPosition_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1b\n\x19GetFromPosition_Responses\"W\n\x18PutToPosition_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x19\n\x17PutToPosition_Responses\"\x1b\n\x19HandoverNumber_Parameters\"S\n\x18HandoverNumber_Responses\x12\x37\n\x0eHandoverNumber\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\" \n\x1e\x41ttainablePositions_Parameters\"]\n\x1d\x41ttainablePositions_Responses\x12<\n\x13\x41ttainablePositions\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer2\xd9\x08\n\x0eStorageService\x12\xbe\x01\n\x15GetLabwareInformation\x12R.sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Parameters\x1aQ.sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Responses\x12\xbe\x01\n\x15SetLabwareInformation\x12R.sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Parameters\x1aQ.sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Responses\x12\xac\x01\n\x0fGetFromPosition\x12L.sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Parameters\x1aK.sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Responses\x12\xa6\x01\n\rPutToPosition\x12J.sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Parameters\x1aI.sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Responses\x12\xad\x01\n\x12Get_HandoverNumber\x12K.sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Parameters\x1aJ.sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Responses\x12\xbc\x01\n\x17Get_AttainablePositions\x12P.sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Parameters\x1aO.sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Responsesb\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETLABWAREINFORMATION_PARAMETERS = _descriptor.Descriptor(
  name='GetLabwareInformation_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=95,
  serialized_end=190,
)


_GETLABWAREINFORMATION_RESPONSES = _descriptor.Descriptor(
  name='GetLabwareInformation_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='LabwareType', full_name='sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Responses.LabwareType', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=192,
  serialized_end=279,
)


_SETLABWAREINFORMATION_PARAMETERS = _descriptor.Descriptor(
  name='SetLabwareInformation_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=281,
  serialized_end=376,
)


_SETLABWAREINFORMATION_RESPONSES = _descriptor.Descriptor(
  name='SetLabwareInformation_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=378,
  serialized_end=411,
)


_GETFROMPOSITION_PARAMETERS = _descriptor.Descriptor(
  name='GetFromPosition_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=413,
  serialized_end=502,
)


_GETFROMPOSITION_RESPONSES = _descriptor.Descriptor(
  name='GetFromPosition_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=504,
  serialized_end=531,
)


_PUTTOPOSITION_PARAMETERS = _descriptor.Descriptor(
  name='PutToPosition_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=533,
  serialized_end=620,
)


_PUTTOPOSITION_RESPONSES = _descriptor.Descriptor(
  name='PutToPosition_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=622,
  serialized_end=647,
)


_HANDOVERNUMBER_PARAMETERS = _descriptor.Descriptor(
  name='HandoverNumber_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=649,
  serialized_end=676,
)


_HANDOVERNUMBER_RESPONSES = _descriptor.Descriptor(
  name='HandoverNumber_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='HandoverNumber', full_name='sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Responses.HandoverNumber', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=678,
  serialized_end=761,
)


_ATTAINABLEPOSITIONS_PARAMETERS = _descriptor.Descriptor(
  name='AttainablePositions_Parameters',
  full_name='sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=763,
  serialized_end=795,
)


_ATTAINABLEPOSITIONS_RESPONSES = _descriptor.Descriptor(
  name='AttainablePositions_Responses',
  full_name='sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePositions', full_name='sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Responses.AttainablePositions', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=797,
  serialized_end=890,
)

_GETLABWAREINFORMATION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_GETLABWAREINFORMATION_RESPONSES.fields_by_name['LabwareType'].message_type = SiLAFramework__pb2._INTEGER
_SETLABWAREINFORMATION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_GETFROMPOSITION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_PUTTOPOSITION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_HANDOVERNUMBER_RESPONSES.fields_by_name['HandoverNumber'].message_type = SiLAFramework__pb2._INTEGER
_ATTAINABLEPOSITIONS_RESPONSES.fields_by_name['AttainablePositions'].message_type = SiLAFramework__pb2._INTEGER
DESCRIPTOR.message_types_by_name['GetLabwareInformation_Parameters'] = _GETLABWAREINFORMATION_PARAMETERS
DESCRIPTOR.message_types_by_name['GetLabwareInformation_Responses'] = _GETLABWAREINFORMATION_RESPONSES
DESCRIPTOR.message_types_by_name['SetLabwareInformation_Parameters'] = _SETLABWAREINFORMATION_PARAMETERS
DESCRIPTOR.message_types_by_name['SetLabwareInformation_Responses'] = _SETLABWAREINFORMATION_RESPONSES
DESCRIPTOR.message_types_by_name['GetFromPosition_Parameters'] = _GETFROMPOSITION_PARAMETERS
DESCRIPTOR.message_types_by_name['GetFromPosition_Responses'] = _GETFROMPOSITION_RESPONSES
DESCRIPTOR.message_types_by_name['PutToPosition_Parameters'] = _PUTTOPOSITION_PARAMETERS
DESCRIPTOR.message_types_by_name['PutToPosition_Responses'] = _PUTTOPOSITION_RESPONSES
DESCRIPTOR.message_types_by_name['HandoverNumber_Parameters'] = _HANDOVERNUMBER_PARAMETERS
DESCRIPTOR.message_types_by_name['HandoverNumber_Responses'] = _HANDOVERNUMBER_RESPONSES
DESCRIPTOR.message_types_by_name['AttainablePositions_Parameters'] = _ATTAINABLEPOSITIONS_PARAMETERS
DESCRIPTOR.message_types_by_name['AttainablePositions_Responses'] = _ATTAINABLEPOSITIONS_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetLabwareInformation_Parameters = _reflection.GeneratedProtocolMessageType('GetLabwareInformation_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _GETLABWAREINFORMATION_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Parameters)
  ))
_sym_db.RegisterMessage(GetLabwareInformation_Parameters)

GetLabwareInformation_Responses = _reflection.GeneratedProtocolMessageType('GetLabwareInformation_Responses', (_message.Message,), dict(
  DESCRIPTOR = _GETLABWAREINFORMATION_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.GetLabwareInformation_Responses)
  ))
_sym_db.RegisterMessage(GetLabwareInformation_Responses)

SetLabwareInformation_Parameters = _reflection.GeneratedProtocolMessageType('SetLabwareInformation_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _SETLABWAREINFORMATION_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Parameters)
  ))
_sym_db.RegisterMessage(SetLabwareInformation_Parameters)

SetLabwareInformation_Responses = _reflection.GeneratedProtocolMessageType('SetLabwareInformation_Responses', (_message.Message,), dict(
  DESCRIPTOR = _SETLABWAREINFORMATION_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.SetLabwareInformation_Responses)
  ))
_sym_db.RegisterMessage(SetLabwareInformation_Responses)

GetFromPosition_Parameters = _reflection.GeneratedProtocolMessageType('GetFromPosition_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _GETFROMPOSITION_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Parameters)
  ))
_sym_db.RegisterMessage(GetFromPosition_Parameters)

GetFromPosition_Responses = _reflection.GeneratedProtocolMessageType('GetFromPosition_Responses', (_message.Message,), dict(
  DESCRIPTOR = _GETFROMPOSITION_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.GetFromPosition_Responses)
  ))
_sym_db.RegisterMessage(GetFromPosition_Responses)

PutToPosition_Parameters = _reflection.GeneratedProtocolMessageType('PutToPosition_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _PUTTOPOSITION_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Parameters)
  ))
_sym_db.RegisterMessage(PutToPosition_Parameters)

PutToPosition_Responses = _reflection.GeneratedProtocolMessageType('PutToPosition_Responses', (_message.Message,), dict(
  DESCRIPTOR = _PUTTOPOSITION_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.PutToPosition_Responses)
  ))
_sym_db.RegisterMessage(PutToPosition_Responses)

HandoverNumber_Parameters = _reflection.GeneratedProtocolMessageType('HandoverNumber_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _HANDOVERNUMBER_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Parameters)
  ))
_sym_db.RegisterMessage(HandoverNumber_Parameters)

HandoverNumber_Responses = _reflection.GeneratedProtocolMessageType('HandoverNumber_Responses', (_message.Message,), dict(
  DESCRIPTOR = _HANDOVERNUMBER_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.HandoverNumber_Responses)
  ))
_sym_db.RegisterMessage(HandoverNumber_Responses)

AttainablePositions_Parameters = _reflection.GeneratedProtocolMessageType('AttainablePositions_Parameters', (_message.Message,), dict(
  DESCRIPTOR = _ATTAINABLEPOSITIONS_PARAMETERS,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Parameters)
  ))
_sym_db.RegisterMessage(AttainablePositions_Parameters)

AttainablePositions_Responses = _reflection.GeneratedProtocolMessageType('AttainablePositions_Responses', (_message.Message,), dict(
  DESCRIPTOR = _ATTAINABLEPOSITIONS_RESPONSES,
  __module__ = 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.storing.storageservice.v1.AttainablePositions_Responses)
  ))
_sym_db.RegisterMessage(AttainablePositions_Responses)



_STORAGESERVICE = _descriptor.ServiceDescriptor(
  name='StorageService',
  full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=893,
  serialized_end=2006,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetLabwareInformation',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.GetLabwareInformation',
    index=0,
    containing_service=None,
    input_type=_GETLABWAREINFORMATION_PARAMETERS,
    output_type=_GETLABWAREINFORMATION_RESPONSES,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetLabwareInformation',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.SetLabwareInformation',
    index=1,
    containing_service=None,
    input_type=_SETLABWAREINFORMATION_PARAMETERS,
    output_type=_SETLABWAREINFORMATION_RESPONSES,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetFromPosition',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.GetFromPosition',
    index=2,
    containing_service=None,
    input_type=_GETFROMPOSITION_PARAMETERS,
    output_type=_GETFROMPOSITION_RESPONSES,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='PutToPosition',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.PutToPosition',
    index=3,
    containing_service=None,
    input_type=_PUTTOPOSITION_PARAMETERS,
    output_type=_PUTTOPOSITION_RESPONSES,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Get_HandoverNumber',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.Get_HandoverNumber',
    index=4,
    containing_service=None,
    input_type=_HANDOVERNUMBER_PARAMETERS,
    output_type=_HANDOVERNUMBER_RESPONSES,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Get_AttainablePositions',
    full_name='sila2.org.silastandard.storing.storageservice.v1.StorageService.Get_AttainablePositions',
    index=5,
    containing_service=None,
    input_type=_ATTAINABLEPOSITIONS_PARAMETERS,
    output_type=_ATTAINABLEPOSITIONS_RESPONSES,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_STORAGESERVICE)

DESCRIPTOR.services_by_name['StorageService'] = _STORAGESERVICE

# @@protoc_insertion_point(module_scope)
