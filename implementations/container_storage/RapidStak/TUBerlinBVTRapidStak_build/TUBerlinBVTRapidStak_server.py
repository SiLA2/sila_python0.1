#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*TUBerlinBVTRapidStak *

:details: TUBerlinBVTRapidStak: This is a plate stacker service. 
           
:file:    TUBerlinBVTRapidStak.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-06-08T22:13:59.070081
:date: (last modification) 2019-06-08T22:13:59.070081

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.5"

import logging
import argparse
import time

import sila2lib.sila_server as slss

import InitialisationController_pb2
import InitialisationController_pb2_grpc
import StorageService_pb2
import StorageService_pb2_grpc


from InitialisationController_servicer import InitialisationController

from InitialisationController_simulation import InitialisationControllerSimulation 
from InitialisationController_real import InitialisationControllerReal 
from StorageService_servicer import StorageService

from StorageService_simulation import StorageServiceSimulation 
from StorageService_real import StorageServiceReal 


class TUBerlinBVTRapidStakServer(slss.SiLA2Server):
    """ Class doc """
    def __init__ (self, parsed_args):
        super().__init__(name=parsed_args.server_name,
                         description=parsed_args.description,
                         server_type=parsed_args.server_type,
                         version=__version__, 
                         vendor_URL="www.bioprocess.tu-berlin.de/htbd")

        """ Class initialiser """
        # registering features
        self.InitialisationController_servicer = InitialisationController()
        InitialisationController_pb2_grpc.add_InitialisationControllerServicer_to_server(self.InitialisationController_servicer, self.grpc_server)
        self.addFeature('InitialisationController', '.')

        self.StorageService_servicer = StorageService()
        StorageService_pb2_grpc.add_StorageServiceServicer_to_server(self.StorageService_servicer, self.grpc_server)
        self.addFeature('StorageService', '.')


            
        # starting and running the gRPC/SiLA2 server
        self.run()
        
    def switchToSimMode(self):
        """overwriting base class method"""
        self.simulation_mode = True
        self.InitialisationController_servicer.injectImplementation(InitialisationControllerSimulation()) # or use 'None' for default simulation implementation
        self.StorageService_servicer.injectImplementation(StorageServiceSimulation()) # or use 'None' for default simulation implementation

        success = True
        logging.debug("switched to sim mode {}".format(success) )

    def switchToRealMode(self):
        """overwriting base class method"""
        self.simulation_mode = False
        self.InitialisationController_servicer.injectImplementation(InitialisationControllerReal())
        self.StorageService_servicer.injectImplementation(StorageServiceReal())

        success = True
        logging.debug("switched to real mode {}".format(success) )
        

def parseCommandLine():
    """ just looking for commandline arguments ...
       :param - : -"""
    help = "SiLA2 service: TUBerlinBVTRapidStak"
    
    parser = argparse.ArgumentParser(description="A SiLA2 service: TUBerlinBVTRapidStak")
    parser.add_argument('-s','--server-name', action='store',
                         default="TUBerlinBVTRapidStak", help='start SiLA server with [server-name]' )
    parser.add_argument('-t','--server-type', action='store',
                         default="TestServer", help='start SiLA server with [server-type]' )
    parser.add_argument('-d','--description', action='store',
                         default="This is a plate stacker service", help='SiLA server description' )
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    return parser.parse_args()
    
        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()
                
    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.server_name))
        
        # generate SiLAserver
        sila_server = TUBerlinBVTRapidStakServer(parsed_args=parsed_args )
        
