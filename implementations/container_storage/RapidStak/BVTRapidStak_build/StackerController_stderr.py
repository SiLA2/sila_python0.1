"""
________________________________________________________________________

:PROJECT: SiLA2_python

*stackercontroller_server_real *

:details: stackercontroller_server_real: 
        This feature is a generic stacker controller. It should be usable with any devices that stack containers in one or multiple stacks.
        Containers / Plates stacks can be organised by stackers or any other kind of physical device.
        Some stacker devices might only grant sequential access to the plates (non-random-access stacks).
        In case there is a random container access, one can specify the container/plate by providing the stack and the plate position in the stack.
        Mind that in some cases the physical arrangement might not correspond to the logical representation: there might be a device with
        two stacks, containing 16 plates each, but it could be grouped to one logic stack accessing the plates from positions 1-32.
        The container is moved from the stack to a transfer position.
        The device might have multiple transfer positions.
        Reorganising of the containers within the stacker might be possible - commands for container reorganisation might follow in the future.
    . 
           
:file:    stackercontroller_server_real.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-02-14T21:12:52.276333
:date: (last modification) 2019-02-14T21:12:52.276333

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.4"




# Std Errors ... 
