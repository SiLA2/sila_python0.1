"""
________________________________________________________________________

:PROJECT: SiLA2_python

*initialisationcontroller_server_simulation *

:details: initialisationcontroller_server_simulation: 
        This Feature describes the Initialisation behaviour of a SiLA Server. It is kept as general as possible to cover as many use-cases as possible.
        Currently a list of strings can be transferred as init parameters.
        In the future a structure might be chosen to achieve more flexibility.
    . 
           
:file:    initialisationcontroller_server_simulation.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-02-14T21:12:52.276333
:date: (last modification) 2019-02-14T21:12:52.276333

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.4"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import InitialisationController_pb2 as pb2
import InitialisationController_pb2_grpc as pb2_grpc


class InitialisationController(pb2_grpc.InitialisationControllerServicer):
    """ InitialisationController - 
#        This Feature describes the Initialisation behaviour of a SiLA Server. It is kept as general as possible to cover as many use-cases as possible.
#        Currently a list of strings can be transferred as init parameters.
#        In the future a structure might be chosen to achieve more flexibility.
#     """
    def __init__ (self):
        """ InitialisationController class initialiser """
        logging.debug("init class: InitialisationController ")

        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())

        self.implementation = None # this corresponds to the simple, fallback simulation mode

    # dependency injection - setter injection s. https://en.wikipedia.org/wiki/Dependency_injection
    def injectImplementation(self, implementation):
        self.implementation = implementation

    def InitialiseServer(self, request, context):
        """This Initialises the Server.
            :param request: gRPC request
            :param context: gRPC context
            :param request.InitParameterList: List of Parameters (currently Stings only) used for initialisation.

        """
        logging.debug("InitialiseServer - Mode: simulation ")

        if self.implementation is not None:
            return self.implementation.InitialiseServer(request, context)
        else:
            pass #~ return_val = request.InitParameterList.value
            #~ return pb2.InitialiseServer_Responses( ServerInitialised=fwpb2.Boolean(value=False) )

    def Get_InitialisationStatus(self, request, context):
        """Status of Initialisation process of Service.
            :param request: gRPC request
            :param context: gRPC context
            :param response.InitialisationStatus: Status of Initialisation process of Service.

        """
        logging.debug("Get_InitialisationStatus - Mode: simulation ")

        if self.implementation is not None:
            return self.implementation.Get_InitialisationStatus(request, context)
        else:
            #~ return_val = request.InitialisationStatus.value
            pass #~ return pb2.InitialisationStatus_Responses( InitialisationStatus=pb2.DataType_InitialisationStatus(InitialisationStatus=fwpb2.String(value="DEFAULTstring" + return_val)) )




