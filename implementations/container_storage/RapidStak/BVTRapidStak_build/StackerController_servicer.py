"""
________________________________________________________________________

:PROJECT: SiLA2_python

*stackercontroller_server_simulation *

:details: stackercontroller_server_simulation: 
        This feature is a generic stacker controller. It should be usable with any devices that stack containers in one or multiple stacks.
        Containers / Plates stacks can be organised by stackers or any other kind of physical device.
        Some stacker devices might only grant sequential access to the plates (non-random-access stacks).
        In case there is a random container access, one can specify the container/plate by providing the stack and the plate position in the stack.
        Mind that in some cases the physical arrangement might not correspond to the logical representation: there might be a device with
        two stacks, containing 16 plates each, but it could be grouped to one logic stack accessing the plates from positions 1-32.
        The container is moved from the stack to a transfer position.
        The device might have multiple transfer positions.
        Reorganising of the containers within the stacker might be possible - commands for container reorganisation might follow in the future.
    . 
           
:file:    stackercontroller_server_simulation.py
:authors: Sebastian Hans, Shaon Debnath, Mark Doerr

:date: (creation)          2019-02-14T21:12:52.276333
:date: (last modification) 2019-02-14T21:12:52.276333

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.4"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import StackerController_pb2 as pb2
import StackerController_pb2_grpc as pb2_grpc


class StackerController(pb2_grpc.StackerControllerServicer):
    """ StackerController - 
#        This feature is a generic stacker controller. It should be usable with any devices that stack containers in one or multiple stacks.
#        Containers / Plates stacks can be organised by stackers or any other kind of physical device.
#        Some stacker devices might only grant sequential access to the plates (non-random-access stacks).
#        In case there is a random container access, one can specify the container/plate by providing the stack and the plate position in the stack.
#        Mind that in some cases the physical arrangement might not correspond to the logical representation: there might be a device with
#        two stacks, containing 16 plates each, but it could be grouped to one logic stack accessing the plates from positions 1-32.
#        The container is moved from the stack to a transfer position.
#        The device might have multiple transfer positions.
#        Reorganising of the containers within the stacker might be possible - commands for container reorganisation might follow in the future.
#     """
    def __init__ (self):
        """ StackerController class initialiser """
        logging.debug("init class: StackerController ")

        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())

        self.implementation = None # this corresponds to the simple, fallback simulation mode

    # dependency injection - setter injection s. https://en.wikipedia.org/wiki/Dependency_injection
    def injectImplementation(self, implementation):
        self.implementation = implementation

    def GetPlateFromStacker(self, request, context):
        """
           Gets a plate from stacker and moves it onto a transfer position.
        
            :param request: gRPC request
            :param context: gRPC context
            :param request.StackNumber: Select, which stack shall be used - integer

        """
        logging.debug("GetPlateFromStacker - Mode: simulation ")

        if self.implementation is not None:
            return self.implementation.GetPlateFromStacker(request, context)
        else:
            pass #~ return_val = request.StackNumber.value
            #~ return pb2.GetPlateFromStacker_Responses( PlateMovedToTransfer=fwpb2.Integer(value=0) )

    def PutPlateToStacker(self, request, context):
        """Puts plate from transfer position to stacker, specified by stack and position 
            :param request: gRPC request
            :param context: gRPC context
            :param request.StackNumber: Select, which stack shall be used - integer

        """
        logging.debug("PutPlateToStacker - Mode: simulation ")

        if self.implementation is not None:
            return self.implementation.PutPlateToStacker(request, context)
        else:
            pass #~ return_val = request.StackNumber.value
            #~ return pb2.PutPlateToStacker_Responses( PlateMovedToTransferPosition=fwpb2.Integer(value=0) )




