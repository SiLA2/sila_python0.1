#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SimpleThermometer client *

:details: SimpleThermometer client: This ia a simple Thermometer Service. 
           
:file:    SimpleThermometer_client.py
:authors: mark doerr

:date: (creation)          20181231
:date: (last modification) 20181231

.. note:: - 0.1.0
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import argparse
import time

from threading import Thread

import grpc

from sila2lib.sila_service_detection import SiLA2ServiceDetection

from sila2lib import SiLAFramework_pb2 as fwpb2
from sila2lib import SiLAService_pb2 as spb
from sila2lib import SiLAService_pb2_grpc as spbg


import TemperatureProvider_pb2
import TemperatureProvider_pb2_grpc


class SimpleThermometerClient:
    """ Class doc """
    def __init__ (self, service_name=None, sila_server_name="localhost", 
                  ip='127.0.0.1', port=50001, cert=None):
        """ Class initialiser 
            param cert=None: server certificate filename, e.g. 'sila_server.crt'  """
        
        # auto service detection
        if service_name is not None:
            # automatic connection
            connect_param = SiLA2ServiceDetection.findServiceByName(service_name)
            
            sila_server_name = connect_param['server_name']
            port = connect_param['port']
            
        if sila_server_name != "" and port != 0 :
            # manual connection
            if cert is not None: # encryption
                credentials = grpc.ssl_channel_credentials(open(cert, 'rb').read()) #grpc.ssl_channel_credentials(root_certificates=trusted_certs)
                self.channel = grpc.secure_channel(':'.join((sila_server_name, str(port) )), credentials)
            else:
                self.channel = grpc.insecure_channel(':'.join((sila_server_name, str(port))))
                
        self.SiLAService_serv_stub = spbg.SiLAServiceStub(self.channel)
                
                
        self.TemperatureProvider_serv_stub = TemperatureProvider_pb2_grpc.TemperatureProviderStub(self.channel)

    
    def run(self):
        try:
            print("SiLA client / client SimpleThermometer -------- commands ------------")
            
            # --> put your remote calls here, e.g. (this is really only an example, please use the your corresponding calls)
            
            # calling SiLAService
            response = self.SiLAService_serv_stub.Get_ImplementedFeatures(spb.ImplementedFeatures_Parameters() )
            for feature_id in response.ImplementedFeatures :
                logging.debug("implemented feature:{}".format(feature_id.Identifier.value) )
            
            try:
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb.GetFeatureDefinition_Parameters(
                                   QualifiedFeatureIdentifier=spb.DataType_Identifier(Identifier=fwpb2.String(value="SiLAService") )) )
                #~ logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            try: 
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb.GetFeatureDefinition_Parameters(
                                    QualifiedFeatureIdentifier=spb.DataType_Identifier(Identifier=fwpb2.String(value="NoFeature") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            response = self.SiLAService_serv_stub.Get_ServerDisplayName(spb.ServerDisplayName_Parameters() )
            logging.debug("display name:{}".format(response.ServerDisplayName.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerDescription(spb.ServerDescription_Parameters())
            logging.debug("description:{}".format(response.ServerDescription.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerVersion(spb.ServerVersion_Parameters())
            logging.debug("version:{}".format(response.ServerVersion.value) )
            
            # testing commands --------------
            
            # --> calling TemperatureProvider
            try :
                response = self.TemperatureProvider_serv_stub.SetMaxTemperature(TemperatureProvider_pb2.SetMaxTemperature_Parameters(MaxTemperature=fwpb2.Real(value=42.0)))
                logging.debug("response:{}".format(response.MaxTemperatureSet.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                pass #~ response = self.TemperatureProvider_serv_stub.SetMinTemperature(TemperatureProvider_pb2.SetMinTemperature_Parameters(MinTemperature=#module.Real(value="Max")))
                #~ logging.debug("response:{}".format(response.MaxTemperatureSet.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )


            # testing properties ------------
            
            try :
                response = self.TemperatureProvider_serv_stub.Get_CurrentTemperature(TemperatureProvider_pb2.CurrentTemperature_Parameters())
                logging.debug("response:{}".format(response.CurrentTemperature.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                response = self.TemperatureProvider_serv_stub.Get_MaxSetTemperature(TemperatureProvider_pb2.MaxSetTemperature_Parameters())
                logging.debug("response:{}".format(response.MaxSetTemperature.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                pass #~ response = self.TemperatureProvider_serv_stub.Get_MinSetTemperature(TemperatureProvider_pb2.MinSetTemperature_Parameters())
                #~ logging.debug("response:{}".format(response.MinSetTemperature.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                pass #~ response = self.TemperatureProvider_serv_stub.Get_MinDeviceTemperature(TemperatureProvider_pb2.MinDeviceTemperature_Parameters())
                #~ logging.debug("response:{}".format(response.MinDeviceTemperature.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                pass #~ response = self.TemperatureProvider_serv_stub.Get_MaxDeviceTemperature(TemperatureProvider_pb2.MaxDeviceTemperature_Parameters())
                #~ logging.debug("response:{}".format(response.MaxDeviceTemperature.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )

            
        except grpc._channel._Rendezvous as err :
            self.error_handler(err)

    def error_handler(self, errors):
        logging.error(errors._state)    

if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    # command line argument parsing
    help = "SiLA2 service: SimpleThermometer"
    
    parser = argparse.ArgumentParser(description="A SiLA2 client: SimpleThermometer")
    
    parser.add_argument('-s','--sila-service', action='store', dest='servicename',
                         help='SiLA service to connect with [servicename]' ) # default="SimpleThermometer"

    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
        
    parsed_args = parser.parse_args()

    if parsed_args.servicename :
        logging.info("starting SiLA2 client with service name: {servicename}".format(servicename=parsed_args.servicename))
        #~ serve(parsed_args.servicename)
        
    else :
        logging.info("starting SiLA2 client: SimpleThermometer")
        sila_client = SimpleThermometerClient(ip='127.0.0.1', port=55001 )
        sila_client.run()
