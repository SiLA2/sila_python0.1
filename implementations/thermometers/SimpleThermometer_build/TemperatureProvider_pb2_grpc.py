# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import TemperatureProvider_pb2 as TemperatureProvider__pb2


class TemperatureProviderStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SetMaxTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/SetMaxTemperature',
        request_serializer=TemperatureProvider__pb2.SetMaxTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.SetMaxTemperature_Responses.FromString,
        )
    self.SetMinTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/SetMinTemperature',
        request_serializer=TemperatureProvider__pb2.SetMinTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.SetMinTemperature_Responses.FromString,
        )
    self.Get_CurrentTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/Get_CurrentTemperature',
        request_serializer=TemperatureProvider__pb2.CurrentTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.CurrentTemperature_Responses.FromString,
        )
    self.Get_MaxSetTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/Get_MaxSetTemperature',
        request_serializer=TemperatureProvider__pb2.MaxSetTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.MaxSetTemperature_Responses.FromString,
        )
    self.Get_MinSetTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/Get_MinSetTemperature',
        request_serializer=TemperatureProvider__pb2.MinSetTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.MinSetTemperature_Responses.FromString,
        )
    self.Get_MinDeviceTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/Get_MinDeviceTemperature',
        request_serializer=TemperatureProvider__pb2.MinDeviceTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.MinDeviceTemperature_Responses.FromString,
        )
    self.Get_MaxDeviceTemperature = channel.unary_unary(
        '/sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider/Get_MaxDeviceTemperature',
        request_serializer=TemperatureProvider__pb2.MaxDeviceTemperature_Parameters.SerializeToString,
        response_deserializer=TemperatureProvider__pb2.MaxDeviceTemperature_Responses.FromString,
        )


class TemperatureProviderServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def SetMaxTemperature(self, request, context):
    """Set the Maximum Temperature of the thermometer
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetMinTemperature(self, request, context):
    """Set the Minimum Temperature of the thermometer
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_CurrentTemperature(self, request, context):
    """remark: Propterty Parameter is always Empty
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_MaxSetTemperature(self, request, context):
    """remark: Propterty Parameter is always Empty
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_MinSetTemperature(self, request, context):
    """remark: Propterty Parameter is always Empty
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_MinDeviceTemperature(self, request, context):
    """remark: Propterty Parameter is always Empty
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_MaxDeviceTemperature(self, request, context):
    """remark: Propterty Parameter is always Empty
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_TemperatureProviderServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SetMaxTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.SetMaxTemperature,
          request_deserializer=TemperatureProvider__pb2.SetMaxTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.SetMaxTemperature_Responses.SerializeToString,
      ),
      'SetMinTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.SetMinTemperature,
          request_deserializer=TemperatureProvider__pb2.SetMinTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.SetMinTemperature_Responses.SerializeToString,
      ),
      'Get_CurrentTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_CurrentTemperature,
          request_deserializer=TemperatureProvider__pb2.CurrentTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.CurrentTemperature_Responses.SerializeToString,
      ),
      'Get_MaxSetTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_MaxSetTemperature,
          request_deserializer=TemperatureProvider__pb2.MaxSetTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.MaxSetTemperature_Responses.SerializeToString,
      ),
      'Get_MinSetTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_MinSetTemperature,
          request_deserializer=TemperatureProvider__pb2.MinSetTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.MinSetTemperature_Responses.SerializeToString,
      ),
      'Get_MinDeviceTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_MinDeviceTemperature,
          request_deserializer=TemperatureProvider__pb2.MinDeviceTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.MinDeviceTemperature_Responses.SerializeToString,
      ),
      'Get_MaxDeviceTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_MaxDeviceTemperature,
          request_deserializer=TemperatureProvider__pb2.MaxDeviceTemperature_Parameters.FromString,
          response_serializer=TemperatureProvider__pb2.MaxDeviceTemperature_Responses.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'sila2.org.silastandard.core.temperatureprovider.v1.TemperatureProvider', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
