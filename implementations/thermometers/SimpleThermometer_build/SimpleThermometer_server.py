#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SimpleThermometer *

:details: SimpleThermometer: This ia a simple Thermometer Service. 
           
:file:    SimpleThermometer.py
:authors: mark doerr

:date: (creation)          20181231
:date: (last modification) 20181231

.. note:: - 0.1.0
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys
import logging
import argparse
import time
from configparser import ConfigParser, NoSectionError, NoOptionError
import uuid

from grpc import server as GRPCServer
from concurrent.futures import ThreadPoolExecutor

import sila2lib.sila_server as slss

import TemperatureProvider_pb2
import TemperatureProvider_pb2_grpc


import TemperatureProvider_simulation
import TemperatureProvider_real

        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    # reading config file
    USER_HOME_DIR = os.environ.get('HOME')
    config_dir = default_answer=os.path.join(USER_HOME_DIR, '.config', 'sila2', 'SimpleThermometer')
    config_filename = default_answer=os.path.join(config_dir, 'SimpleThermometer' + '.conf') 
                    
    try: 
        sila2_config = ConfigParser()
        sila2_config.read(config_filename)
        
        server_uuid = sila2_config['server']['UUID']
        
    except NoSectionError as err :
        sys.stderr.write("ERROR: Please add section in your config file {}".format(err) )
        server_uuid = uuid.uuid4() # random uuid  uuid.uuid5(SimpleThermometer, 'sila2')
        
        #~ sila2_config.set('server','UUID',server_uuid)
        #~ sila2_config.write()
        
    except NoOptionError as err:
        sys.stderr.write("ERROR: Cannot read config file option in {}".format(err) )
    
        server_uuid = uuid.uuid4() # random uuid  uuid.uuid5(SimpleThermometer, 'sila2')
        #~ sila2_config.set('server','UUID',server_uuid)

    except KeyError as err:
        sys.stderr.write("ERROR: Cannot read config file option in {}".format(err) )
        
        server_uuid = uuid.uuid4() # random uuid  uuid.uuid5(SimpleThermometer, 'sila2')
        sila2_config['server'] = {}
        sila2_config['server']['UUID'] = str(server_uuid)
        
        if not os.path.exists(config_dir):
                os.mkdir(config_dir)
                
        with open(config_filename, 'w') as configfile:
            sila2_config.write(configfile)
    
    # parsing command line
    help = "SiLA2 service: SimpleThermometer"
    
    parser = argparse.ArgumentParser(description="A SiLA2 service: SimpleThermometer")
    
    parser.add_argument('-s','--start-service', action='store', dest='servicename',
                         default="SimpleThermometer", help='start SiLA service with [servicename]' )

    parser.add_argument('-g','--gui', action='store_true', help='activate graphical user interface mode')
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
        
    parsed_args = parser.parse_args()
    
    if parsed_args.gui :
        logging.info("trying to open installer GUI ....")
        logging.error("sorry, no GUI implemented yet :o( . Exiting now ...")
        exit(0)
                
    if not parsed_args.gui :
        #~ logging.info("text mode service")
                    
        if parsed_args.servicename :
            logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.servicename))
            
            # generate SiLAservice
            description = "This ia a simple Thermometer Service"
            version = "0.0.1"
            vendor_url = "lara.uni-greifswald.de"
            sila_server = slss.SiLA2Server(name=parsed_args.servicename, description=description, version=version, vendor_url=vendor_url)
            
            TemperatureProvider_server_sim = TemperatureProvider_simulation.TemperatureProvider()
            TemperatureProvider_server_real = TemperatureProvider_real.TemperatureProvider()
            TemperatureProvider_pb2_grpc.add_TemperatureProviderServicer_to_server(TemperatureProvider_server_sim, sila_server.grpc_server)


            sila_server.addFeature('TemperatureProvider', '.')

            
            # starting and running the gRPC/SiLA2 server
            sila_server.run()
