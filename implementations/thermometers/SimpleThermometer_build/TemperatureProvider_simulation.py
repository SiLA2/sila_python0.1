"""
________________________________________________________________________

:PROJECT: SiLA2_python

*temperatureprovider_server_simulation *

:details: temperatureprovider_server_simulation: 
        Retrieving the temperature of a thermometer-like device.
        Based on TemperatureController developed by Stefan Koch ? / Maximilian Schulz ?
    . 
           
:file:    temperatureprovider_server_simulation.py
:authors: mark doerr

:date: (creation)          20181231
:date: (last modification) 20181231

.. note:: code generated automatically by SiLA2codegenerator !


           - 0.1.0
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import TemperatureProvider_pb2 as pb2
import TemperatureProvider_pb2_grpc as pb2_grpc


class TemperatureProvider(pb2_grpc.TemperatureProviderServicer):
    """ TemperatureProvider - 
#        Retrieving the temperature of a thermometer-like device.
#        Based on TemperatureController developed by Stefan Koch ? / Maximilian Schulz ?
#     """
    def __init__ (self):
        """ TemperatureProvider class initialiser """
        logging.debug("init class: TemperatureProvider ")

    def SetMaxTemperature(self, request, context):
        """Set the Maximum Temperature of the thermometer
        :param request.MaxTemperature: 
                The target temperature that the server will try to reach.
                Note that the command might be completed at a temperature that it evaluates to be close enough.
            
            MaxTemperature - constraints: 
            MaxTemperature - max lenth: -1
        """
        logging.debug("SetMaxTemperature - Mode: simulation ")

        self.max_set_temp = request.MaxTemperature.value
        
        return pb2.SetMaxTemperature_Responses(MaxTemperatureSet=fwpb2.Boolean(value=42.0 ))

    def SetMinTemperature(self, request, context):
        """Set the Minimum Temperature of the thermometer
        :param request.MinTemperature: 
                The target temperature that the server will try to reach.
                Note that the command might be completed at a temperature that it evaluates to be close enough.
            
            MinTemperature - constraints: 
            MinTemperature - max lenth: -1
        """
        logging.debug("SetMinTemperature - Mode: simulation ")

        #~ return pb2.SetMinTemperature_Responses(# add response parameter and argument here,e.g. MaxTemperatureSet=#module.Boolean(value="Command response: Hello SiLA2 world {} !".format(request.MinTemperature.value) ))

    def Get_CurrentTemperature(self, request, context):
        """Current Temperature as measured by the controller.
            :param request: gRPC request
            :param context: gRPC context    :param response.CurrentTemperature: Current Temperature as measured by the controller.
            CurrentTemperature - constraints: 
            CurrentTemperature - max lenth: -1
        """
        logging.debug("Get_CurrentTemperature - Mode: simulation ")
        
        curr_temp = self.max_set_temp -3.3

        return pb2.CurrentTemperature_Responses(CurrentTemperature=fwpb2.Real(value=curr_temp))

    def Get_MaxSetTemperature(self, request, context):
        """Current Maximum Temperature Set on the controller.
            :param request: gRPC request
            :param context: gRPC context    :param response.MaxSetTemperature: Current Maximum Temperature Set on the controller.
            MaxSetTemperature - constraints: 
            MaxSetTemperature - max lenth: -1
        """
        logging.debug("Get_MaxSetTemperature - Mode: simulation ")
        
        

        return pb2.MaxSetTemperature_Responses(MaxSetTemperature=fwpb2.Real(value=self.max_set_temp))

    def Get_MinSetTemperature(self, request, context):
        """Current Minimum Temperature Set on the controller.
            :param request: gRPC request
            :param context: gRPC context    :param response.MinSetTemperature: Current Minimum Temperature Set on the controller.
            MinSetTemperature - constraints: 
            MinSetTemperature - max lenth: -1
        """
        logging.debug("Get_MinSetTemperature - Mode: simulation ")

        #~ return pb2.MinSetTemperature_Responses(# add response parameter and argument here,e.g. MinSetTemperature=#module.Real(value="Property response: Hello SiLA2 world !"))

    def Get_MinDeviceTemperature(self, request, context):
        """Minimum Temperature that can be measured by device.
            :param request: gRPC request
            :param context: gRPC context    :param response.MinDeviceTemperature: Minimum Temperature that can be measured by device.
            MinDeviceTemperature - constraints: 
            MinDeviceTemperature - max lenth: -1
        """
        logging.debug("Get_MinDeviceTemperature - Mode: simulation ")

        #~ return pb2.MinDeviceTemperature_Responses(# add response parameter and argument here,e.g. MinDeviceTemperature=#module.Real(value="Property response: Hello SiLA2 world !"))

    def Get_MaxDeviceTemperature(self, request, context):
        """Maximum Temperature that can be measured by device.
            :param request: gRPC request
            :param context: gRPC context    :param response.MaxDeviceTemperature: Maximum Temperature that can be measured by device.
            MaxDeviceTemperature - constraints: 
            MaxDeviceTemperature - max lenth: -1
        """
        logging.debug("Get_MaxDeviceTemperature - Mode: simulation ")

        #~ return pb2.MaxDeviceTemperature_Responses(# add response parameter and argument here,e.g. MaxDeviceTemperature=#module.Real(value="Property response: Hello SiLA2 world !"))

    
