#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*ThermoCytomat2 *

:details: ThermoCytomat2: Thermo Cytomat2 Service. 
           
:file:    ThermoCytomat2.py
:authors: Mark Doerr (Uni-Greifswald), Sebastian Hans (TU-Berlin)

:date: (creation)          2019-03-08T15:59:07.077359
:date: (last modification) 2019-03-08T15:59:07.077359

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import argparse

import sila2lib.sila_server as slss

import InitialisationController_pb2
import InitialisationController_pb2_grpc
import StackerController_pb2
import StackerController_pb2_grpc
import TemperatureController_pb2
import TemperatureController_pb2_grpc


from InitialisationController_servicer import InitialisationController

from InitialisationController_simulation import InitialisationControllerSimulation 
from InitialisationController_real import InitialisationControllerReal 
from StackerController_servicer import StackerController

from StackerController_simulation import StackerControllerSimulation 
from StackerController_real import StackerControllerReal 
from TemperatureController_servicer import TemperatureController

from TemperatureController_simulation import TemperatureControllerSimulation 
from TemperatureController_real import TemperatureControllerReal 


class ThermoCytomat2Server(slss.SiLA2Server):
    """ Class doc """
    def __init__ (self, parsed_args):
        super().__init__(name=parsed_args.server_name,
                         description=parsed_args.description,
                         server_type=parsed_args.server_type,
                         version=__version__, 
                         vendor_URL="lara.uni-greifswald.de.de")
        
        """ Class initialiser """
        # registering features
        self.InitialisationController_servicer = InitialisationController()
        InitialisationController_pb2_grpc.add_InitialisationControllerServicer_to_server(self.InitialisationController_servicer, self.grpc_server)
        self.addFeature('InitialisationController', '.')

        self.StackerController_servicer = StackerController()
        StackerController_pb2_grpc.add_StackerControllerServicer_to_server(self.StackerController_servicer, self.grpc_server)
        self.addFeature('StackerController', '.')

        self.TemperatureController_servicer = TemperatureController()
        TemperatureController_pb2_grpc.add_TemperatureControllerServicer_to_server(self.TemperatureController_servicer, self.grpc_server)
        self.addFeature('TemperatureController', '.')


            
        # starting and running the gRPC/SiLA2 server
        self.run()
        
    def switchToSimMode(self):
        """overwriting base class method"""
        self.simulation_mode = True
        self.InitialisationController_servicer.injectImplementation(InitialisationControllerSimulation()) # or use 'None' for default simulation implementation
        self.StackerController_servicer.injectImplementation(StackerControllerSimulation()) # or use 'None' for default simulation implementation
        self.TemperatureController_servicer.injectImplementation(TemperatureControllerSimulation()) # or use 'None' for default simulation implementation

        success = True
        logging.debug("switched to sim mode {}".format(success) )

    def switchToRealMode(self):
        """overwriting base class method"""
        self.simulation_mode = False
        self.InitialisationController_servicer.injectImplementation(InitialisationControllerReal())
        self.StackerController_servicer.injectImplementation(StackerControllerReal())
        self.TemperatureController_servicer.injectImplementation(TemperatureControllerReal())

        success = True
        logging.debug("switched to real mode {}".format(success) )
        

def parseCommandLine():
    """ just looking for commandline arguments ...
       :param - : -"""
    help = "SiLA2 service: ThermoCytomat2"
    
    parser = argparse.ArgumentParser(description="A SiLA2 service: ThermoCytomat2")
    parser.add_argument('-s','--server-name', action='store',
                         default="ThermoCytomat2", help='start SiLA server with [server-name]' )
    parser.add_argument('-t','--server-type', action='store',
                         default="TestServer", help='start SiLA server with [server-type]' )
    parser.add_argument('-d','--description', action='store',
                         default="Thermo Cytomat2 Service", help='SiLA server description' )
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    return parser.parse_args()
    
        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()
                
    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.server_name))
        
        # generate SiLAserver
        sila_server = ThermoCytomat2Server(parsed_args=parsed_args )
        
