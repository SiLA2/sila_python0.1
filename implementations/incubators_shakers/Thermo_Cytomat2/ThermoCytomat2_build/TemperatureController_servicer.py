"""
________________________________________________________________________

:PROJECT: SiLA2_python

*temperaturecontroller_server_simulation *

:details: temperaturecontroller_server_simulation: 
        Controlling and retrieving the temperature.
    . 
           
:file:    temperaturecontroller_server_simulation.py
:authors: Mark Doerr (Uni-Greifswald), Sebastian Hans (TU-Berlin)

:date: (creation)          2019-03-08T15:59:07.077359
:date: (last modification) 2019-03-08T15:59:07.077359

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import TemperatureController_pb2 as pb2
import TemperatureController_pb2_grpc as pb2_grpc


class TemperatureController(pb2_grpc.TemperatureControllerServicer):
    """ TemperatureController - 
#        Controlling and retrieving the temperature.
#     """
    def __init__ (self):
        """ TemperatureController class initialiser """
        logging.debug("init class: TemperatureController ")

        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())

        self.implementation = None # this corresponds to the simple, fallback simulation mode

    # dependency injection - setter injection s. https://en.wikipedia.org/wiki/Dependency_injection
    def injectImplementation(self, implementation):
        self.implementation = implementation

    def ControlTemperature(self, request, context):
        """Control the Temperature gradually to a set target
            :param request: gRPC request
            :param context: gRPC context
            :param request.TargetTemperature: 
                The target temperature that the server will try to reach.
                Note that the command might be completed at a temperature that it evaluates to be close enough.
            

        """
        logging.debug("ControlTemperature - Mode: simulation ")

        if self.implementation is not None:
            return self.implementation.ControlTemperature(request, context)
        else:
            pass #~ return_val = request.TargetTemperature.value
            #~ return pb2.ControlTemperature_Responses(  )




