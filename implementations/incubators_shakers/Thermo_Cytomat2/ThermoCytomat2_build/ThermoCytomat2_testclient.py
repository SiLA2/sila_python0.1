#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*ThermoCytomat2 client *

:details: ThermoCytomat2 client: Thermo Cytomat2 Service. 
           
:file:    ThermoCytomat2_client.py
:authors: Mark Doerr (Uni-Greifswald), Sebastian Hans (TU-Berlin)

:date: (creation)          2019-03-08T15:59:07.077359
:date: (last modification) 2019-03-08T15:59:07.077359

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import argparse
import time

import grpc

import sila2lib.sila_client as s2client
from sila2lib.sila_service_detection import SiLA2ServiceDetection

from sila2lib import SiLAFramework_pb2 as fwpb2
from sila2lib.std_features import SiLAService_pb2 as spb2
from sila2lib.std_features import SiLAService_pb2_grpc as spb2g
from sila2lib.std_features import SimulationController_pb2 as scpb2
from sila2lib.std_features import SimulationController_pb2_grpc as scpb2g

import InitialisationController_pb2
import InitialisationController_pb2_grpc
import StackerController_pb2
import StackerController_pb2_grpc
import TemperatureController_pb2
import TemperatureController_pb2_grpc


class ThermoCytomat2Client(s2client.SiLA2Client):
    """ Class doc """
    def __init__ (self, name="ThermoCytomat2Client", service_name=None, sila_hostname="localhost", 
                  description="Description: This is a SiLA2 test client",
                  UUID = None, version="0.0", vendor_URL="lara.uni-greifswald.de",
                  ip='127.0.0.1', port=50051, key=None, cert=None):
        super().__init__(name=name,
                        service_name=service_name,
                        description=description,
                        UUID = UUID,
                        version=version,
                        sila_hostname=sila_hostname,
                        vendor_URL=vendor_URL,
                        ip=ip, port=port, 
                        key=key, cert=cert)
        
        """ Class initialiser 
            param cert=None: server certificate filename, e.g. 'sila_server.crt'  """
                
        self.InitialisationController_serv_stub = InitialisationController_pb2_grpc.InitialisationControllerStub(self.channel)
        self.StackerController_serv_stub = StackerController_pb2_grpc.StackerControllerStub(self.channel)
        self.TemperatureController_serv_stub = TemperatureController_pb2_grpc.TemperatureControllerStub(self.channel)

    
    def run(self):
        try:
            print("SiLA client / client ThermoCytomat2 -------- commands ------------")
            
            # --> put your remote calls here, e.g. (this is really only an example, please use the your corresponding calls)
            
            # calling SiLAService
            response = self.SiLAService_serv_stub.Get_ImplementedFeatures(spb2.ImplementedFeatures_Parameters() )
            for feature_id in response.ImplementedFeatures :
                logging.debug("implemented feature:{}".format(feature_id.FeatureIdentifier.value) )
            
            try:
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                   QualifiedFeatureIdentifier=spb2.DataType_FeatureIdentifier(FeatureIdentifier=fwpb2.String(value="SiLAService") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            try: 
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                    QualifiedFeatureIdentifier=spb2.DataType_FeatureIdentifier(FeatureIdentifier=fwpb2.String(value="NoFeature") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            response = self.SiLAService_serv_stub.Get_ServerName(spb2.ServerName_Parameters() )
            logging.debug("display name:{}".format(response.ServerName.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerDescription(spb2.ServerDescription_Parameters())
            logging.debug("description:{}".format(response.ServerDescription.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerVersion(spb2.ServerVersion_Parameters())
            logging.debug("version:{}".format(response.ServerVersion.value) )
            
            # testing commands --------------
            
            # --> calling InitialisationController
            try :
                pass #~ response = self.InitialisationController_serv_stub.InitialiseServer(InitialisationController_pb2.InitialiseServer_Parameters(InitParameterList=InitialisationController_pb2.InitParameterList(InitParameterList=fwpb2.String(value="DEFAULTstring" + return_val))))
                #~ logging.debug("InitialiseServer response:{}".format(response.ServerInitialised.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            # --> calling StackerController
            try :
                pass #~ response = self.StackerController_serv_stub.GetPlateFromStacker(StackerController_pb2.GetPlateFromStacker_Parameters(StackNumber=fwpb2.Integer(value=0)))
                #~ logging.debug("GetPlateFromStacker response:{}".format(response.PlateMovedToTransfer.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                pass #~ response = self.StackerController_serv_stub.PutPlateToStacker(StackerController_pb2.PutPlateToStacker_Parameters(StackNumber=fwpb2.Integer(value=0)))
                #~ logging.debug("PutPlateToStacker response:{}".format(response.PlateMovedToTransferPosition.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            # --> calling TemperatureController
            try :
                pass #~ response = self.TemperatureController_serv_stub.ControlTemperature(TemperatureController_pb2.ControlTemperature_Parameters(TargetTemperature=fwpb2.Real(value=0.0)))
                #~ logging.debug("ControlTemperature response:{}".format(response.Void.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )


            # testing properties ------------
            
            try :
                response = self.InitialisationController_serv_stub.Get_InitialisationStatus(InitialisationController_pb2.InitialisationStatus_Parameters())
                #~ logging.debug("Get_InitialisationStatus response:{}".format(response.InitialisationStatus.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                response = self.StackerController_serv_stub.Get_FreePositions(StackerController_pb2.FreePositions_Parameters())
                #~ logging.debug("Get_FreePositions response:{}".format(response.FreePositions.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            try :
                response = self.StackerController_serv_stub.Get_NextFreePosition(StackerController_pb2.NextFreePosition_Parameters())
                #~ logging.debug("Get_NextFreePosition response:{}".format(response.NextFreePosition.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )

            
        except grpc._channel._Rendezvous as err :
            self.error_handler(err)

    def error_handler(self, errors):
        logging.error(errors._state)    

def parseCommandLine():
    """ just looking for command line arguments ...
       :param - : - """
    help = "SiLA2 service: ThermoCytomat2"
    parser = argparse.ArgumentParser(description="A SiLA2 client: ThermoCytomat2")
    parser.add_argument('-s','--server-name', action='store',
                         help='SiLA server to connect with [server-name]', default="ThermoCytomat2")
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    
    return parser.parse_args()
    
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()

    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 client with service name: {servername}".format(servername=parsed_args.server_name))
        
        logging.info("starting SiLA2 client: ThermoCytomat2")
        sila_client = ThermoCytomat2Client(ip='127.0.0.1', port=50051 )
        sila_client.run()
