"""
________________________________________________________________________

:PROJECT: SiLA2_python

*initialisationcontroller_server_real *

:details: initialisationcontroller_server_real: 
        This Feature describes the Initialisation behaviour of a SiLA Server. It is kept as general as possible to cover as many use-cases as possible.
        Currently a list of strings can be transferred as init parameters.
        In the future a structure might be chosen to achieve more flexibility.
    . 
           
:file:    initialisationcontroller_server_real.py
:authors: Mark Doerr (Uni-Greifswald), Sebastian Hans (TU-Berlin)

:date: (creation)          2019-03-08T15:59:07.077359
:date: (last modification) 2019-03-08T15:59:07.077359

.. note:: Code generated automatically by SiLA2codegenerator v0.1.9!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"




# Std Errors ... 
