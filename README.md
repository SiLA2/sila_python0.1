**Important**: 

** !!! A brand new Version of sila_python is currently developed and will be opened for the public very soon !!! **

This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point.
It might not comply with the latest version of the Standard, and much of its content might change in the future.

**The release of SiLA 2 is scheduled for Q1/2019.**

For more general information about the standard, we refer to the [sila_features](https://gitlab.com/SiLA2/sila_features) repository.

# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

# SiLA Python3 Repository

This is currently the fastest route to get SiLA2 experience.
With the SiLA2 python installer  - *sila2install.py* (s. [Installation](#installation)) - you will be able to run a first SiLA2 HelloWorld Service within *5 minutes* !

## Maintainer
This repository is jointly maintained by [University of Greifswald](https://www.uni-greifswald.de/) and [TU München](http://tum.de). please contact:

- Mark Doerr ([doerrm@uni-greifswald.de](mailto:doerrm@uni-greifswald.de))
- Timm Severin ([timm.severin@tum.de](mailto:timm.severin@tum.de))

## Components

### sila_library
A python library to aid the development of SiLA Servers.
The library contains, e.g. a Server Detection, SiLA Service and the  Error Handling library. 

### examples
A collection of example applications that demonstrate important concepts of SiLA2.
It also provides reference implementations for typical lab devices and other common tasks. 

* Hello_SiLA2 - a minimalistic "Hello World" Service. 


### sila_tools
Tools to support the rapid development of SiLA2 Servers/Services.
It is highly recommended to start each individual project with the powerful codegenerator, 
since it already provides you with a functional framework:

* codegenerator - this is a powerful tool to generate a complete SiLA2 set of server/client, 
                  solely based on Python3, out of a few configuration files.

### implementation_concepts
Proof-of-Concept (POC) implementations - for demonstration purposes

### raspberry-pi
All you need to run SiLA2 apps on a Raspberry Pi

## Installation

To run the code in this repository it is highly recommended to use *Python 3.5* or higher.

***Note***: This code is developed under Ubuntu. It should work with other operating systems, but has not been tested extensively yet.

To install the library and development tools an automatic setup routine exists. Alternatively a manual installation is possible. 

Prerequisites for the installation are:
 - **Python3**
    - ![windows][windows-logo] Windows: Use a python implementation (e.g. [CPython](https://www.python.org/downloads/))
    - ![linux][linux-logo] Linux[^1]: Oftentimes `python3` is included in the base installation, otherwise run   
    `sudo apt-get install python3` 
 - **pip (package manager for Python)**  
    Pythons package manager is used to automatically download and install the packages necessary to run the SiLA client and server applications. This requires a working internet connection.  
    Installation routines:
    - ![windows][windows-logo] Windows: Should be included in the implementations installer
    - ![linux][linux-logo] Linux[^1]: `sudo apt-get install pip3` 
 - ***Recommended:* git**   
    Git is used to download this repositories contents. If you are not familiar with git, GitLab has a documentation on [how to get started](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).    
    Installation routines:
    - ![windows][windows-logo] Windows: Either find your favorite GUI, or install the [command line version](https://git-scm.com/download/win).
    - ![linux][linux-logo] Linux[^1]: `sudo apt-get install git`
    
    Once you have installed git, clone this repository
    ```bash
    # create a dirctory for the source code and change into it
    mkdir sila_python & cd sila_python
    # clone into the current directory
    git clone https://gitlab.com/SiLA2/sila_python.git .
    ```
    
    Otherwise you can download the current version on the projects [main page](https://gitlab.com/SiLA2/sila_python) using the button on the top right: ![download button](docs/images/installation/gitlab-download.png)
 - ***Recommended:* A [virtual environment](https://wiki.ubuntuusers.de/virtualenv/) for SiLA.**  
 It is recommended to set-up a virtual environment (venv) for the installation, at least in the development phase. While the automatic installation routine can theoretically create the environment for you, it is (as of now) more reliable to do this manually beforehand.
 If you have no idea what a virtual environment is, above link and possibly further literature is recommended. 
 Remember to activate the environment before the installation (`[venv]/bin/activate` in Linux ![linux][linux-logo], `[venv]/Scripts/activate.bat` in Windows ![windows][windows-logo])
 
[^1]: The commands given here assume a Debian based distribution (e.g. Ubuntu). Other users should be experienced enough to adjust the commands.

### Automatic (guided) installation

The automatic installation can be run with the command 

```bash
python3 sila2install.py
```

It will guide through the complete installation, including the download of all required packages, the installation of the SiLA2 library and further tools. 
If you manually activated your virtual environment choose `[N]` at the corresponding question.

### Manual installation

The manual installation is only recommended for experienced users. 
Make sure your virtual environment is set-up and activated. 

#### SiLA Library

The following commands will install the SiLA2 library in the currently active python path:

```bash
cd sila_library
pip3 install -r requirements_base.txt
pip3 install .

cd ..
```

Once the installation has completed, it can be imported into any python code using the statement

```python
import sila2lib
```

#### SiLA Tools (Code generator)

The SilA tools are not necessarily required to run a SiLA client/server. However, they are quite useful in developing both. To install, run the following commands

```bash
# install SiLA tool requirements
cd sila_tools
pip3 install -r requirements.txt

# install the code generator
cd sila2codegenerator
pip3 install .
cd ..

cd ..
``` 

#### Create a directory for config files

Client/server applications use `.conf` files to store information. They are written to `$HOME/.config/sila2`. This directory should be created during the installation:

![linux][linux-logo] In Linux:
```bash
mkdir -p ~/.config/sila2
```

![windows][windows-logo] In Windows:
```console
mkdir %userprofile%\.config\sila2
```


## Quickstart Tutorials

Get SiLA2 running in less than 10 Minutes with the

* Hello_SiLA2 example 
* Rapid creation of a complete Python3 based SiLA2 server/client pair with the codegenerator
* Installing a Python3 SiLA2 Server on a Raspberry Pi3 

[linux-logo]: docs/images/icons/linux.png
[windows-logo]: docs/images/icons/windows.png
