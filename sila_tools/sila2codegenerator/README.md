# SiLA2 code generator - completely written in Python3

The codegenerator supports the *SiLA2 server/client* developer in two major tasks: 

1. it converts the Feature Description File (FDL/XML) into a Protobuf .proto file
2. it generates a completely running SiLA server/client ( in simulation mode), 
    where only the real/hardware specific code needs to be added/subclassed to the generated prototype code.

s.[Tutorial](#codegenerator-demo-and-tutorial) for a fast introduction

## Features

  * .proto file generation out of FeatureDescription Files FDL/XML
  * .proto file compilation
  * complete server/client generation out of a project directory, containing a service_description file (s.hello_sila_project)

  * support for Service/Device detection
  * support for Service/Device feature detection
  * switching between simulation and real (hardware) mode (+)
  * default / standard Features (SiLAService, Sim, runcontrol, deviceinfo, ...) (+)
  * default error handling (+)
  * QT5 support (+)
  * sila-python-django support (+)
  
(+) under development

## Installation

s. Installation section of the sila_python repository README

## Testing

To validate that the basic machinery is working correctly, please run the unittests:

    cd [dir of codegenerator]

    python3 -m unittest

## Proto file generation

The main function of the operator generator is to convert a SiLA2 Feature Description File (FDL) into a .proto file:

    python3 sila2codegenerator.py [feature_descripton_file.xml]
    
This will result in a proto file with the output filename feature_descripton_file.proto

The output proto filename can be set with the -p option.

Example:

    python3 sila2codegenerator.py -f [feature_descripton_file.xml] -p my_protofilename.proto

## Automated SiLA2 Server / Client Generation

To generate a completely working SiLA2 python sever/client pair, create a folder that contains the Feature Description Files (FDL) of all your features the device should have,
furthermore add a JSON file that describe the operator (s. the hello_sila2_project as an example).

Calling the codegenerator by specifying the project directory with the -d option

        python3 sila2codegenerator.py -d [your_operator_project_dir]

will result in all proto files that are described by the a new directory, that is called [servicename_operator]. This directory contains 

  - a SiLA service file that calls all the features (this file can be used to start SiLA server) (_server.py)
  - the probuf/gRPC helper classes of the proto compiler
  - a base class for each feature that can be used as simulation mode part of the service (_simulation.py)
  - a derived class that can be used to implement the real / hardware connection (_real.py)

The Service instance will automatically activate and address the Device and Feature detection mechanism of SiLA2 via the SiLAService Feature - no extra code needs to be added.
Further functionality, like standard error handling and QT5 support are under development.
 
## Installation 

For the installation of the code generator, please change to sila_python repository root directory and run ....

    python3 sila2install.py
    
This will install all required libraries and packages.

Alternatively, if you directly wish to install the codegenerator,
change to the sila2codegenerator directory and run:

    python3 setup.py install
    
This will only install the sila2codegenerator.

It is highly recommended to work in the python3 virtual environment (default proposal of the installer):
This ensures that the system Python setup is not affected. To activate the virtual environment, type

    source [venv]/bin/activate
    
to deactivate it again, simply type (mind: here is no path required)

    deactivate

## Runnig the codegenerator

Within the *sila2codegen* directory you can call it by typing

    python3 sila2codegenerator.py --help

If you have installed the codegenerator correctly (by runnig *setup.py*), then you can call the codegenerator
from anywhere with

    python -m sila2codegen.sila2codegenerator --help 
 
## Codegenerator Demo and Tutorial

To run the demo, make sure, you are in a folder containing the hello_sila_project folder and run:

```bash
    source [venv]/bin/activate    # this activates the virtual python environment 

    cd [folder containing HelloSiLA2_project] 
    
    python3 sila2codegenerator.py -d HelloSiLA2_project
```
    
This will create a directory, called "HelloSiLA2_build".
It will contain the following files:

  * HelloSiLA_server.py - the main "SiLA2 Server/Device file": run this to start the SiLA2 Server [python3 HelloSiLA2_server.py]
  * HelloSiLA_testclient.py - the "SiLA2 client/orchestrator": run this to send commands to the SiLA Server [python3 HelloSiLA2_testclient.py]

  * GreetingProvider_pb2.py - the underlying  Protobuf / gRPC bindings  
  * GreetingProvider_pb2_grpc.py - the underlying gRPC bindings

  * GreetingProvider_simulation.py - this code can be used for the simulation mode
  * GreetingProvider_real.py - this code can be used for writing real-life / hardware connections (in case the service is connected to hardware) 
  
  To actually see data transfer between the SiLA2 Server and the SiLA2 client, please uncomment/change the following lines:
  
      in HelloSiLA2_simulation.py / sayHello() add:
         return pb2.SayHelloResponses(Greeting="Hello world !")
         
      in HelloSiLA2_testclient.py / run() change:
         result = self.HelloSiLA_serv.SayHello(HelloSiLA_pb2.SayHelloParameters(Name="SiLA2"))
         logging.info(result) # this will display the answer from the server


## Future Plans

 - library of standard features, that are already connected (SiLAService, ServiceControl, Simulation, ...)
 - django app to control the device remotely through the web
 - QT5 client / server apps 
