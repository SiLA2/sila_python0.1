"""
________________________________________________________________________

:PROJECT: SiLA2_python

*gRPC proto file builder*

:details: gRPC protobuf file generator. : This class generates a proto file 
          based on a SiLA Feature Description (FDL) file. 
          It can even compile the output to the ready-to use 
          gRPC client-server pair 
          
         - proto file output
         - gRPC code
         - adjusted to new XML schema

:file:    proto_builder.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180523
:date: (last modification) 20190816

.. note:: - 
            
.. todo:: - check that namespaces are handled correctly
          - java part of proto file header - is it correct
          
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.10"

import os
import sys
import datetime
import logging

import pkg_resources
from grpc_tools import protoc
    
class ProtoBuilder():
    """gRPC proto file builder/generator class 
    """
    def __init__ (self, fdl_parser=None, proto_filename="unknown.proto", service_descr_dic=None, 
                        project_dir=".", output_dir=None, import_basic_types=True):
        """:param fdl_parser: fdl_parser object
           :param proto_filename: name of output .proto file
           :param project_dir: directory of current project (location of FDL files) 
                               .proto and location of the 
           :param output_dir: destination of generated python stubs (result of proto compliation)
           :param import_basic_types: import basic data types from sila_std.poto into target proto file
        """
        #~ logging.info("proto gen init")
        
        self.fdl_parser = fdl_parser
                
        self.project_dir = project_dir
        
        if service_descr_dic is not None and output_dir is None :
            self.output_dir = os.path.join('.', service_descr_dic["service_name"]+"_build")
        elif service_descr_dic is not None and output_dir is not None :
             self.output_dir = os.path.join(output_dir, service_descr_dic["service_name"]+"_build")
        elif service_descr_dic is None and output_dir is not None : self.output_dir = output_dir
        else : self.output_dir = "."
        
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)
        
        self.proto_output_filename = os.path.join(project_dir, proto_filename)
        
        if fdl_parser is not None:
            self.fdl_tree = fdl_parser.tree
            self.sila_feature = fdl_parser.sila_feature
            self.data_type_dic = fdl_parser.data_type_dic
            self.import_basic_types = import_basic_types
            
            self.cmd_parameter_dic = self.fdl_parser.cmd_parameter_dic
            self.resp_parameter_dic = self.fdl_parser.resp_parameter_dic
            self.intermed_resp_parameter_dic = self.fdl_parser.intermed_resp_parameter_dic
            self.prop_resp_parameter_dic = self.fdl_parser.prop_resp_parameter_dic
            
            self.proto_base_namespace =  self.fdl_parser.proto_base_namespace
            self.proto_namespace = self.fdl_parser.proto_namespace
            self.sila_framework_namespace = self.fdl_parser.sila_framework_namespace
            
            #~ self.std_err_dic = fdl_parser.std_err_dic
            
            # gRPC proto generation
            self.generateProto()
            self.writeProto()
    
    def generateProto(self):
        """ constructing the .proto file code content by
            iterating through 
            all commands in self.command_dic
            all properties in self.property_dic
            and all data types in self.data_type_dic.
        """
        #~ logging.debug("gen proto after parsing{}".format(1) )
        
        self.rpc_calls = ""
        self.messages = ""
        
        self.createDataTypes()
        self.createCommands()
        self.createProperties()
        self.assembleProtoFile() # finally assemble everything

    def createDataTypes(self):
        """Creating the DataType part of the proto file
           All data types that are defined in the FDL file should appear
          :param [param_name]: [description]"""
        
        logging.debug("------------- Datatypes ---------------")
        
        self.messages += "// ------ Data Type definitions -------\n\n"
        
        # iterating through all defined data types (esp. the derived Datatypes defined in the FDL file) 
        for dt_id, dt_attr in self.data_type_dic.items() :
            
            datatype_prefix = ".DataType_"
            
            # skipping over basic types - if desired
            if dt_attr['basic_type'] == True and not self.import_basic_types :
                logging.debug("basic type {}".format(1) )
                try:
                    parameters = "\t {proto_type} {param_id} = 1; ".format( param_id=dt_id,
                                                                            proto_type=dt_attr['basic_proto_type'] ) 
                    descr = dt_attr['description']
                    descr += "// constr-pattern: {}".format(dt_attr['constraints'])
                    descr = descr.replace('\n', '\n//')
                    self.messages += "// {description}\nmessage DataType_{dt_id} {{\n{param}}}\n\n".format(description=descr,
                                                                                                  dt_id=dt_id,
                                                                                                  param=parameters )
                except KeyError as err:
                    sys.stderr.write('ERROR: datatype KeyError {} failed'.format(err))
                    
            elif dt_attr['basic_type'] == False :  # complex = non basic data types
                try:
                    message_param = "\t {list_type} {sila2_type} {param_id} = 1; ".format( list_type=self.data_type_dic[dt_id]['proto_list_type'], 
                                                                                           sila2_type=self.data_type_dic[dt_id]['proto_namespace']
                                                                                        + "." + self.data_type_dic[dt_id]['basic_sila_type'],  # proto_type=self.data_type_dic[dtd.Identifier]['basic_proto_type'],
                                                                                        param_id=dt_id ) 
                    
                    descr = dt_attr['description']
                    constr_dict = dt_attr['constraints']
                    #~ logging.debug("*constr dic: {}".format(constr_dict) )
                    
                    descr += "\n// constr-pattern: {}".format(constr_dict['pattern'])
                    
                    self.messages += "// {description}\nmessage DataType_{dt_id} {{\n{param}}}\n\n".format(description=descr,
                                                                                                  dt_id=dt_id,
                                                                                                  param=message_param )
                except KeyError as err:
                        sys.stderr.write('ERROR: datatype KeyError {} failed'.format(err))
                        
    def createCommands(self):
        """Creating the Commands part of the proto file
           :param [param_name]: [description]"""
           
        self.messages += "// ---- Command Parameter and Response Data Type definitions ----\n\n"
         
        # iterating through all commands
        for cmd in self.sila_feature.Command :
            try:
                # handling command call
                cmd_id = cmd.Identifier
                logging.debug(cmd_id)
                
                # RP Call
                descr = " ".join(str(cmd.Description).split()).replace('\n', '\n//')
                # non-observable command
                if cmd.Observable == "No":
                    self.rpc_calls += ("\n\t// {description}\n\trpc {cmd_id}({param})"
                                    " returns ({response});\n").format(description=descr, cmd_id=cmd_id,
                                                                        param=self.proto_namespace + "." + cmd_id + "_Parameters",
                                                                        response=self.proto_namespace + "." + cmd_id +"_Responses" )
                else: # observable command
                    self.rpc_calls += ("\n\t// {description}\n\trpc {cmd_id}({param})"
                                       " returns ({response});\n").format(description=descr, cmd_id=cmd_id,
                                                                          param=self.proto_namespace + "." + cmd_id + "_Parameters",
                                                                          response=self.sila_framework_namespace + ".CommandConfirmation")
                    if hasattr(cmd, "IntermediateResponse"):
                        self.rpc_calls += ("\trpc {cmd_id}({param})"
                                           " returns (stream {response});\n").format(description=descr, cmd_id=cmd_id + "_Intermediate",
                                                                                     param=self.sila_framework_namespace + ".CommandExecutionUUID",
                                                                                     response=self.proto_namespace + "." + cmd_id + "_IntermediateResponses")

                    self.rpc_calls += ("\trpc {cmd_id}({param})"
                                       " returns (stream {response});\n").format(description=descr, cmd_id=cmd_id + "_Info",
                                                                                     param=self.sila_framework_namespace + "." + "CommandExecutionUUID",
                                                                                     response=self.sila_framework_namespace + ".ExecutionInfo")
                    self.rpc_calls += ("\trpc {cmd_id}({param})"
                                       " returns ({response});\n").format(description=descr, cmd_id=cmd_id + "_Result",
                                                                                     param=self.sila_framework_namespace + ".CommandExecutionUUID",
                                                                                     response=self.proto_namespace + "." + cmd_id + "_Responses")

                # proto command parameter message
                parameters = ""
                idx = 1
                for para_dic in self.cmd_parameter_dic[cmd]:
                    dt_dic = para_dic['datatype_dic']

                    if para_dic['id'] != 'Void':
                        parameters += "\n\t{namespace}{datatype_prefix}{proto_type} {param_id} = {field_idx};".format( namespace=dt_dic['proto_namespace'],
                                                            datatype_prefix = dt_dic['datatype_prefix'],
                                                            proto_type=dt_dic['sila_type'],
                                                            param_id=para_dic['id'],
                                                            field_idx=idx )  #self.data_type_dic[cmd.Parameter.DataType.DataTypeIdentifier]['basic_proto_type'], )
                        idx += 1

                descr = "Parameter Message: " + self.data_type_dic[dt_dic['sila_type']]['description']
                descr = descr.replace('\n', '\n//')
                self.messages += "// {description}\nmessage {msg_id} {{{param}\n}}\n\n".format(description=descr,
                                                                                                 msg_id=cmd_id + "_Parameters", #could also be fully qualified
                                                                                                 param=parameters )

                # proto intermediate response message
                if hasattr(cmd, "IntermediateResponse"):
                    parameters = ""
                    idx = 1
                    for intermed_para_dic in self.intermed_resp_parameter_dic[cmd]:
                        dt_dic = intermed_para_dic['datatype_dic']

                        if intermed_para_dic['id'] != 'Void':
                            parameters += "\n\t{namespace}{datatype_prefix}{proto_type} {param_id} = {field_idx};".format( namespace=dt_dic['proto_namespace'],
                                                                datatype_prefix = dt_dic['datatype_prefix'],
                                                                proto_type=dt_dic['sila_type'],
                                                                param_id=intermed_para_dic['id'],
                                                                field_idx=idx )  #self.data_type_dic[cmd.Parameter.DataType.DataTypeIdentifier]['basic_proto_type'], )
                            idx += 1
                        try:
                            datatype_prefix = ""
                            descr = "Intermediate Response Message: " + self.data_type_dic[dt_dic['sila_type']]['description']
                            self.messages += "// {description}\nmessage {datatype_prefix}{msg_id} {{{param}\n}}\n\n".format(description=descr,
                                                                                datatype_prefix=datatype_prefix,
                                                                                msg_id=cmd_id+"_IntermediateResponses",
                                                                                param=parameters )
                        except Exception as err:
                            sys.stderr.write('ERROR: reponse KeyError {} failed\n'.format(err))



                # proto response message - mind that some commands might have no respones
                try:
                    parameters = ""
                    idx = 1
                    for para_dic in self.resp_parameter_dic[cmd]:
                        dt_dic = para_dic['datatype_dic']

                        try:
                            if para_dic['id'] != 'Void':
                                parameters += "\n\t{namespace}{datatype_prefix}{proto_type} {param_id} = {field_idx};".format(namespace=dt_dic['proto_namespace'],
                                                                    datatype_prefix = dt_dic['datatype_prefix'],
                                                                    proto_type=dt_dic['sila_type'],
                                                                    param_id=para_dic['id'],
                                                                    field_idx= idx ) #self.data_type_dic[cmd.Response.DataType.DataTypeIdentifier]['basic_proto_type'] )
                                idx += 1

                        except Exception as err:
                            sys.stderr.write('ERROR: reponse KeyError {} failed\n'.format(err))

                        datatype_prefix = ""
                        descr = "Response Message: " + self.data_type_dic[dt_dic['sila_type']]['description']
                        logging.warning("handle command constraints{}".format(1) )
                        self.messages += "// {description}\nmessage {datatype_prefix}{msg_id} {{{param}\n}}\n\n".format(description=descr,
                                                                                                    datatype_prefix=datatype_prefix,
                                                                                                    msg_id=cmd_id+"_Responses",
                                                                                                    param=parameters )
                except KeyError as err:
                    logging.debug("Command [{0}] has no response - {1}".format(cmd_id, err) )
            except Exception as err:
                logging.exception("")
                #~ sys.stderr.write('ERROR (proto_gen): general cmd gen-Error {} failed\n'.format(err))

        
    def createProperties(self):
        """Creating the Properties part of the proto file
           :param [param_name]: [description]"""
        # iterating through all properties
        
        logging.debug("------------- Properties ---------------")
        
        self.messages += "// ---- Property Parameter and Response Data Type definitions ----\n\n"
        
        if not hasattr(self.sila_feature, 'Property') : return
             
        for prop in self.sila_feature.Property :
            prop_id = prop.Identifier
            if prop.Observable == "No": # non-observable property
                prop_prefix = "Get_"
                resp_type = "" # normal response
            else: # observable property
                prop_prefix = "Subscribe_"
                resp_type = "stream " # server-side streaming response
            
            logging.debug("...... prop id %s ......" % prop_id)
            
            parameters = ""
            idx = 1
            for para_dic in self.prop_resp_parameter_dic[prop]:
                dt_dic = para_dic['datatype_dic']

                descr = "remark: Property Parameters are always Empty"
                self.rpc_calls += "\n\t// {description}\n\trpc {prop_id}({param}) returns ({response});\n".format(description=descr,
                                                                                                    prop_id=prop_prefix + prop_id,
                                                                                                    param=self.proto_namespace + "." + prop_prefix + prop_id+"_Parameters",
                                                                                                    response=resp_type + self.proto_namespace + "." + prop_prefix + prop_id+"_Responses" )
                # parameters (= Empty)
                descr = str(prop.Description).replace('\n', '\n//')
                self.messages += "// {description}\nmessage {msg_id} {{ }}\n\n".format(description=descr,
                                                                                    msg_id=prop_prefix+prop_id+"_Parameters" )

                parameters += "\n\t{list_type} {namespace}{datatype_prefix}{param_type} {param_id} = {field_idx};".format( list_type=dt_dic['proto_list_type'],
                                                                namespace = dt_dic['proto_namespace'],
                                                                datatype_prefix=dt_dic['datatype_prefix'],
                                                                param_type=dt_dic['sila_type'],
                                                                param_id=prop_id,
                                                                field_idx=idx )
                idx += 1

                descr = prop_id + "_Response:\n"
                #~ descr +=  "// constraints: {constr}\n// maximal length={max_len}".format(constr=dt_dic['pattern'],
                                                                                #~ max_len=dt_dic['maxLen'] )
                descr += para_dic['description']
                descr = descr.replace('\n', '\n//')
                self.messages += "// {description}\nmessage {msg_id} {{{param}\n}}\n\n".format(description=descr,
                                                                                            msg_id=prop_prefix+prop_id+"_Responses",
                                                                                            param=parameters )

        logging.debug("-- Properties done  --")
                                                                                        
    def assembleProtoFile(self):
        """This puts everything together in one string
           :param [param_name]: [description]"""
           
        sila2framework_proto = ""
        if self.import_basic_types : sila2framework_proto = "import \"SiLAFramework.proto\";\n\n"
        
        self.proto_file_str = ("// This file is automatically generated by {generator}\n"
                               "// :generation date: {date}\n\n"
                               "// ---- PLEASE DO NOT MODIFY MANUALLY !! ---\n\n").format(generator=__name__+__version__, 
                                                                                          date=datetime.datetime.now())
        self.proto_file_str += ("syntax = \"proto3\";\n\n"
                                "{sila2framework_proto}"
                                "package {proto_namespace};\n\n"
                                "service {feature_id} {{ {rpc_calls}\n}}\n\n"
                                "{messages}").format(sila2framework_proto=sila2framework_proto,
                                                     proto_namespace=self.proto_namespace,
                                                     feature_id=self.sila_feature.Identifier,
                                                     rpc_calls=self.rpc_calls,
                                                     messages=self.messages)
    def writeProto(self):
        """ writes .proto to project dir
            :param -: -"""

        with open(self.proto_output_filename, 'w') as file:
            file.write(self.proto_file_str)
            
        logging.debug("proto file: {} written \n\n".format(self.proto_output_filename))
        
    def compileProtoFile(self, proto_filename="", proto_dir=None, output_dir=None, 
                         output_format="python"):
        """ compiles .proto files to python (stub) code,
            output dir is self.output_dir
            modified version of buildPackageProtos
            from grpcio_tools/grpc_tools/command.py
            :param proto_file: filename of .proto source file
            :param proto_dir: .proto source directory
            :param tartget_dir: target directory for generated python/javascript code
            :param output_format - string : ["python"|"javascript"] 
        """
        output_format.lower()
        
        if proto_filename == "":
            proto_filename = os.path.abspath(self.proto_output_filename)
            
        if proto_dir is None:
            curr_project_dir = os.path.abspath(self.project_dir)
    
        if output_dir == None:
            output_dir = os.path.abspath(self.output_dir)
        
        if output_format == "python":
            command = [ 'grpc_tools.protoc',
                        '--proto_path={}'.format(curr_project_dir),
                        '--python_out={}'.format(output_dir),
                        '--grpc_python_out={}'.format(output_dir),
                      ] + [proto_filename]
            if protoc.main(command) != 0:
                sys.stderr.write('ERROR: proto compiling for python [{}] failed\n\n'.format(command))
                exit()
            
        elif output_format == "javascript":
            logging.warning("JavaScript output only works with protoc > {} - tpye:\n\tprotoc --version \n to show the version".format("v3.6.1") )
            # protoc -I="protos" echo.proto --js_out=import_style=commonjs:generated --grpc-web_out=import_style=commonjs,mode=grpcwebtext:generated
            command = [ 'protoc',
                    '--I={}'.format(curr_project_dir),
                    '--js_out={}'.format("import_style=commonjs:generated"),
                    '--grpc-web_out={}'.format("import_style=commonjs,mode=grpcwebtext:generated"),
                  ] + [proto_filename]
        else:
            logging.error("Unknown output format {}".format(output_format) )
                        
