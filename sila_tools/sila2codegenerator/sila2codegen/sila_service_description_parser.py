"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SiLA service decription parser*

:details: SiLA Service description parser. This class is used to describe 
          a complete SiLA Service (server/client pair)

:file:    sila_service_description_parser.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180530
:date: (last modification) 20181230

.. note:: -
.. todo:: - change to XML / SDL (Service Descripton Language) format ?? 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

import logging
import json

class ServiceDecriptor():
    """ Class doc """
    def __init__ (self, JSON_filename="service_description.json"):
        """ This class is used to describe a complete SiLA2 service by the help of a JSON file"""
        self.JSON_filename = JSON_filename
        
        self.service_descr_dic = {}
        
        self.loadJSONfile()

    def loadJSONfile(self, filename=""):
        """ :param filename: name of the JSON file to be loaded"""
        
        if filename is not "": self.JSON_filename = filename
        
        #~ logging.debug( "opening JSON service descriptor file:{}".format(self.JSON_filename) )
        with open(self.JSON_filename, 'r' ) as dev_descr_fp:
            try:
                self.service_descr_dic = json.load(dev_descr_fp)
                #~ logging.info('JSON file structure of {} seems to be OK :)'.format(self.JSON_filename) )
                return self.service_descr_dic
                
            except ValueError as  err:
                logging.error('JSON service descriptor file {}, ValueError: {}'.format(self.JSON_filename, err) )
                exit()
            except Exception as err:
                logging.error('JSON service descriptor file {}, ExceptionError: {}'.format(self.JSON_filename, err) ) 
                exit()
