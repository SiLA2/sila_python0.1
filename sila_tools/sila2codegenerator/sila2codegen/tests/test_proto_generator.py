"""
________________________________________________________________________

:PROJECT: SiLA2_python

*unittest SiLA2 proto generator *

:details: SiLA2 proto generator.
          To run the tests type: 
            python -m unittest tests/test_proto_generator.py

:file:    test_fdl_parser.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20181003
:date: (last modification) 20181019

.. note:: - this is very ugly code, will be replaced by cleaner module
.. todo:: - preserv parameter and response order !!!
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.5"

import os
import unittest
import logging

import sila2lib.fdl_parser as fdl
import sila2codegen.proto_generator as pgen 

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

class TestProtoGenerator(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        
        self.test_fdl = os.path.join(TEST_DATA_DIR, "SiLAService.sila.xml")
        logging.debug("fdl;{}".format(self.test_fdl) )
        
        self.fdl_parser = fdl.FDLParser(self.test_fdl)
        logging.debug("fdl_parser version: {}".format(self.fdl_parser.version()) )
        self.proto_gen = pgen.ProtoBuilder(self.fdl_parser)
    
    def test_generateProto(self):
        """ generating a gRPC protobuf file
            :param [param_name]: [description]"""
        
        #~ self.assertEqual(self.proto_gen.proto_str, "proto")
        pass
        
        # test generation of Void type code !! (best any type )
        
        
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
