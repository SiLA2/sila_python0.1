"""
________________________________________________________________________

:PROJECT: SiLA2_python

*unittest SiLA2 serverclient generator *

:details: SiLA2 serverclient generator.
          To run the tests type: 
            python setup.py test or 
            python -m unittest tests/test_serverclient_generator.py

:file:    test_fdl_parser.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20181003
:date: (last modification) 20181019

.. note:: - this is very ugly code, will be replaced by cleaner module
.. todo:: - preserv parameter and response order !!!
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import unittest
import logging
import tempfile

import sila2lib.fdl_parser as fdl
import sila2codegen.proto_generator as pgen
from sila2codegen.sila_service_description_parser import ServiceDecriptor
from sila2codegen.serverclient_generator import ServerClientPrototypeGenerator

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

class TestServerClientGenerator(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        
        self.test_fdl = os.path.join(TEST_DATA_DIR, "SiLAService.sila.xml")
        logging.debug("fdl;{}".format(self.test_fdl) )
        
        self.fdl_parser = fdl.FDLParser(self.test_fdl)
        logging.debug("fdl_parser version: {}".format(self.fdl_parser.version()) )
        #~ self.proto_gen = pgen.ProtoBuilder(self.fdl_parser)
                
        self.sd = ServiceDecriptor(os.path.join(TEST_DATA_DIR, "service_description.json"))
        
        self.service_name = self.sd.service_descr_dic['service_name']
        logging.debug("Generating Service Name: {}".format(self.service_name) )
        
        self.sd.service_descr_dic['service_type'] = "TestServer"+__version__ 
        self.sd.service_descr_dic['command_calls'] = "" # init command section for all test client calls
        self.sd.service_descr_dic['property_calls'] = "" # init properties section for all test client calls   
        
        service_name = self.sd.service_descr_dic['service_name'] 
        
        self.tmp_dir_name = tempfile.gettempdir()
        logging.debug(" using temp dir: {}".format(self.tmp_dir_name) )
    
        
    def test_genServerPrototypeCode(self):
        """ generating a gRPC protobuf file
            :param [param_name]: [description]"""

        server_client_gen = ServerClientPrototypeGenerator( fdl_parser=self.fdl_parser, 
                                                                service_descr_dic=self.sd.service_descr_dic, 
                                                                output_dir=self.tmp_dir_name)
        
        server_client_gen.writeServicerCode(server_mode="simulation" )
        server_client_gen.writeServerImplementationPrototypeCode(server_mode="simulation" )
        server_client_gen.writeServerImplementationPrototypeCode(server_mode="real" )
        server_client_gen.writeServerErrorHandlerCode() 
        
         # finally writing the SiLA server entry code / user interface 
        if server_client_gen is not None : server_client_gen.writeServerCode()
        
        # writing a SiLA test client for the defined SiLA server  
        if server_client_gen is not None : server_client_gen.writeClientCode()
                                                            
        #~ self.assertEqual(self.proto_gen.proto_str, "proto")
        
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
