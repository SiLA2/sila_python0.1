"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SiLA command code generator*

:details: SiLA2 server_client code generator. : 

:file:    command_generator.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190105
:date: (last modification) 20190105

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.4"

import os
import logging

lower_first = lambda s: s[:1].lower() + s[1:] if s else '' # macro for lower first character of a string
newline_comment = lambda s: str(s).replace('\n', '\n#') if s else '' 

