#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*${service_name} *

:details: ${service_name}: ${service_description}. 
           
:file:    ${service_name}.py
:authors: ${authors}

:date: (creation)          ${creation_date}
:date: (last modification) ${creation_date}

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "${version}"

import logging
import argparse

import sila2lib.sila_server as slss

${import_service_grpc_classes}

${import_service_implementation_classes}

class ${service_name}Server(slss.SiLA2Server):
    """ Class doc """
    def __init__ (self, parsed_args):
        super().__init__(name=parsed_args.server_name,
                         description=parsed_args.description,
                         server_type=parsed_args.server_type,
                         version=__version__, 
                         vendor_URL="${vendor_url}")
        
        """ Class initialiser """
        # registering features
${service_registration}
            
        # starting and running the gRPC/SiLA2 server
        self.run()
        
    def switchToSimMode(self):
        """overwriting base class method"""
        self.simulation_mode = True
${switch_sim}
        success = True
        logging.debug("switched to sim mode {}".format(success) )

    def switchToRealMode(self):
        """overwriting base class method"""
        self.simulation_mode = False
${switch_real}
        success = True
        logging.debug("switched to real mode {}".format(success) )
        

def parseCommandLine():
    """ just looking for commandline arguments ...
       :param - : -"""
    help = "SiLA2 service: ${service_name}"
    
    parser = argparse.ArgumentParser(description="A SiLA2 service: ${service_name}")
    parser.add_argument('-s','--server-name', action='store',
                         default="${service_name}", help='start SiLA server with [server-name]' )
    parser.add_argument('-t','--server-type', action='store',
                         default="${service_type}", help='start SiLA server with [server-type]' )
    parser.add_argument('-d','--description', action='store',
                         default="${service_description}", help='SiLA server description' )
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    return parser.parse_args()
    
        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()
                
    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.server_name))
        
        # generate SiLAserver
        sila_server = ${service_name}Server(parsed_args=parsed_args )
        
