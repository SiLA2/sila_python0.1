"""
________________________________________________________________________

:PROJECT: SiLA2_python

*${module_name} *

:details: ${module_name}: ${module_description}. 
           
:file:    ${module_name}.py
:authors: ${authors}

:date: (creation)          ${creation_date}
:date: (last modification) ${creation_date}

.. note:: ${module_note}
           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "${version}"


${server_code}

${error_handler_code}
