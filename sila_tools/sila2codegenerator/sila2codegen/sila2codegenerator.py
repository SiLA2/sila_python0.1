#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SiLA2 code generator*

:details: SiLA2 code generator. This is similar to the SiLA2 java 
          code generator with some extra features: 
         - JSON output
         - SiLA2 server/client prototype classes (server/client generator)

:file:    sila2codegenerator.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180522
:date: (last modification) 20190816

.. note:: -
.. todo:: - simulation mode
          - dynamic properties
          - testing all command line parameters / calling options
          - service detection
          - dynamic properties with command ids ...
          - heart beat

          - tests
          - documentation
          - django app
          - QT5 app ???
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.10"


import os
import sys
from datetime import datetime

import logging
import argparse

from sila2lib.fdl_parser import FDLParser
from sila2codegen.proto_generator import ProtoBuilder
from sila2codegen.sila_service_description_parser import ServiceDecriptor
from sila2codegen.serverclient_generator import ServerClientPrototypeGenerator

def main(parsed_args=None):
    """The main sila2codegenerator routine."""
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    #~ if args is None:
        #~ args = sys.argv[1:]
    
    fdl_parser=None
    
    if parsed_args is None:
        parsed_args = parseCommandLine()
    
    if parsed_args.proto :
        logging.debug("pure proto buf file genration {}".format(parsed_args.proto) )
        filename_base, fn_ext = os.path.splitext(parsed_args.proto[0])
        logging.info("pos arg - generating proto from FDL file: {fdl_file}".format(fdl_file=parsed_args.proto))
        
        fdl_parser = FDLParser(parsed_args.proto[0], fdl_schemaname=parsed_args.fdl_schemaname)
        ProtoBuilder(fdl_parser, filename_base + ".proto")
        
    elif parsed_args.fdl_filename :
        filename_base, fn_ext = os.path.splitext(parsed_args.fdl_filename)
        logging.info("generating proto from sila.XML/FDL file: {fdl_file}".format(fdl_file=parsed_args.fdl_filename))
            
        if parsed_args.verify_xml:
            FDLParser.validateFDLFile(None,fdl_filename=parsed_args.fdl_filename, 
                                           fdl_schemaname=parsed_args.fdl_schemaname)
                                           
        else :
            fdl_parser = FDLParser(parsed_args.fdl_filename, fdl_schemaname=parsed_args.fdl_schemaname)
            
            proto_gen = ProtoBuilder(fdl_parser, filename_base + ".proto")
            
            if parsed_args.compile_proto :
                proto_gen.compileProtoFile()
                
    elif parsed_args.compile_proto :
        logging.debug("Compiling proto file: {}".format(parsed_args.compile_proto) )
        proto_gen = ProtoBuilder(proto_filename=parsed_args.compile_proto)
        proto_gen = proto_gen.compileProtoFile()
                    
    if parsed_args.project_dir :
        logging.info("generating server and client code from project directory: [{}]".format(parsed_args.project_dir))
        
        service_descr_filename = "service_description.json" # default description file name, should be definable
        sd = ServiceDecriptor(os.path.join(parsed_args.project_dir, service_descr_filename  ))
        
        # setting to default time
        if sd.service_descr_dic['creation_date'] == '':
            sd.service_descr_dic['creation_date'] = datetime.utcnow().isoformat()
        
        sd.service_descr_dic['service_type'] = "TestServer" # default service_type, should be set in dev_descr_file
            
        service_name = sd.service_descr_dic['service_name']

        server_client_output_dir = os.path.join(parsed_args.output_dir, service_name + "_build")
        
        # make build output dir, if not exists ...
        if not os.path.exists(server_client_output_dir):
            os.mkdir(server_client_output_dir)
                    
        # generate a proto file for each .xml/FDL file
        fdl_parser_dic = {}
        sd.service_descr_dic['command_calls'] = "" # init command section for all test client calls
        sd.service_descr_dic['property_calls'] = "" # init properties section for all test client calls
        
        server_client_gen = None
        # iterate through all features in the list
        for sila_feature_id in sd.service_descr_dic['SiLA_feature_list']:
            # finding corresponding .xml file to feature
            fdl_file = os.path.join(parsed_args.project_dir, sila_feature_id + ".sila.xml")
            logging.debug("\n---- parsing FDL file: {} -----------\n".format(fdl_file) )
            
            try:
                # step 1: building proto files
                fdl_parser_dic[sila_feature_id] = FDLParser(fdl_filename=fdl_file,
                                                            fdl_schemaname=parsed_args.fdl_schemaname)
                fdl_parser_dic[sila_feature_id].pickleAllDicts(server_client_output_dir)
            
                proto_gen = ProtoBuilder( fdl_parser=fdl_parser_dic[sila_feature_id], 
                                proto_filename=sila_feature_id + ".proto", 
                                service_descr_dic=sd.service_descr_dic, 
                                project_dir=parsed_args.project_dir )
                
                # step 2: compiling the proto file 
                logging.debug("\n---- compiling proto for {} -----------\n".format(fdl_file) )                
                proto_gen.compileProtoFile()
                
                # fix generated _pb2 and _pb2_grpc files to use Framework files from library
                # quite ugly - should be fixed by better solution - any suggestions ?
                def fix_pb2_files(pb2_suffix):
                    pb2_filename = os.path.join(server_client_output_dir, sila_feature_id + pb2_suffix )
                    with open(pb2_filename, "r") as fin:
                        corr_text=fin.read().replace('SiLAFramework_pb2', 'sila2lib.SiLAFramework_pb2')
                    with open(pb2_filename, "w") as fout:
                        fout.write(corr_text)

                fix_pb2_files("_pb2.py")
                fix_pb2_files("_pb2_grpc.py")

                logging.debug("\n---- building server/client prototype code {}  ------\n".format(fdl_file) )
                
                # nicer solution, but not working yet:
                #~ with open(pb2_filename, "r+t") as fin:
                    #~ for line in fin:
                        #~ fin.write(line.replace('SiLAFramework_pb2', 'sila2lib.SiLAFramework_pb2'))
        
                # step 3: building the service prototype code
                server_client_gen = ServerClientPrototypeGenerator(fdl_parser=fdl_parser_dic[sila_feature_id], 
                                              service_descr_dic=sd.service_descr_dic, 
                                              output_dir=parsed_args.output_dir)
                
                # writing servicer and prototype code for implementations
                server_client_gen.writeServicerCode(server_mode="simulation" )
                server_client_gen.writeServerImplementationPrototypeCode(server_mode="simulation" )
                server_client_gen.writeServerImplementationPrototypeCode(server_mode="real" )
                
                server_client_gen.writeServerErrorHandlerCode() 
                
            except Exception as err:
                logging.exception('')
                             
        # iterating first through SiLA standard feature list - to generate also this code ....
        #~ for sila_std_feature_id in sd.service_descr_dic['SiLA_standard_feature_list']:
            #~ logging.warning("to be implemented: adding sila standard{}".format(sila_std_feature_id) )
        
        # finally writing the SiLA server entry code / user interface 
        if server_client_gen is not None : server_client_gen.writeServerCode()
        
        # writing a SiLA test client for the defined SiLA server  
        if server_client_gen is not None : server_client_gen.writeClientCode()
        
        #  client (= orchestrator)
        #~ if parsed_args.client_filename :
            #~ server_client_gen.writeClientCode(parsed_args.client_filename+"_client.py")
        #~ else : server_client_gen.writeClientCode(parsed_args.server_client_filename+"_client.py")
        
def parseCommandLine():
    """ just looking for commandline arguments ...
       :param - : -
       
       """
       
    help = 'Generate code for SiLA2 server / client pair.'
    
    # just the command line parser
    parser = argparse.ArgumentParser(description="A SiLA2 code generator")
    
    # optional commandline arguments
    parser.add_argument('-f','--fdl_filename', action='store', help='.proto file generation' )
    
    # new option: -p --project-file : user can specify an arbitrary project (JSON) file and directory of project file is used as default dir
    parser.add_argument('-d','--project_dir', action='store', help='server/client project directory' ) # depricated, use -p instead
    
    parser.add_argument('-o','--output_dir', action='store', default='.', help='server/client output directory' )
    parser.add_argument('-p','--proto_filename', action='store', help='.proto output filename' )
    parser.add_argument('-x','--verify-xml', action='store_true', help='XML/FDL file validation' )
    parser.add_argument('-s','--schema', action='store', dest='fdl_schemaname', default=None, help='XML/FDL validation schema' )
    parser.add_argument('-c','--compile-proto', action='store', help='compile .proto file to python output' )
    parser.add_argument('-j','--javascript', action='store_true', help='compile .proto file to javascript output' )
    parser.add_argument('-t','--client', action='store', dest='client_filename', help='client code generation' )
    
    parser.add_argument('-g','--generic-grpc', action='store_true', help='(Not yet Implemented !!)uses grpc data types, not SiLA datatypes')
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    
    # positional commandline arguments
    parser.add_argument('proto', action='store', nargs="*", help='.proto file generation' )

    return parser.parse_args()    
        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()
    
    main(parsed_args)
