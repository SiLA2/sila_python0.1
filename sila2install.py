#!/usr/bin/env python3
"""_____________________________________________________________________

:PROJECT: SiLA2_python

*SiLA2 sila2_python installer skript*

:details: SiLA2 sila2_python installer.
          Installing core SiLA2 python library
          and optional tools interactivly.
          
:authors: mark doerr (mark@uni-greifswald.de)
          Timm Severin (timm.severin@tum.de)
          Robert Giessmann (robert.giessmann@tu-berlin.de)

:date: (creation)          20180610
:date: (last modification) 20190713
________________________________________________________________________
"""


import sys
import os
from pathlib import Path

import pip
import subprocess

from distutils.util import strtobool

# checking python version
if sys.version_info.major < 3:
    raise Exception("Your python version is outdated, please use Python > 3.5")
elif sys.version_info.minor < 5:
    raise Exception("Your python version is outdated, please use Python > 3.5")

HOME_DIR = str(Path.home())

REPO_DIR = os.path.dirname(os.path.realpath(__file__))  # current directory of this repository


# --------------- installation helper functions, please do not modify -----------------------------

def query_yes_no(question, default_answer="yes", help=""):
    """Ask user at stdin a yes or no question

    :param question: question text to user
    :param default_answer: should be "yes" or "no"
    :param help: help text string
    :return:  :type: bool
    """
    if default_answer == "yes":
        prompt_txt = "{question} [Y/n] ".format(question=question)
    elif default_answer == "no":  # explicit no
        prompt_txt = "{question} [y/N] ".format(question=question)
    else:
        raise ValueError("default_answer must be 'yes' or 'no'!")

    while True:
        try:
            answer = input(prompt_txt)
            if answer:
                if answer == "?":
                    print(help)
                    continue
                else:
                    return strtobool(answer)
            else:
                return strtobool(default_answer)
        except ValueError:
            sys.stderr.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
        except KeyboardInterrupt:
            sys.stderr.write("Query interrupted by user, exiting now ...")
            exit(0)


def query(question, default_answer="", help=""):
    """Ask user a question

    :param question: question text to user
    :param default_answer: any default answering text string
    :param help:  help text string
    :return: stripped answer string
    """
    prompt_txt = "{question} [{default_answer}] ".format(question=question, default_answer=default_answer)

    while True:
        answer = input(prompt_txt).strip()

        if answer:
            if answer == "?":
                print(help)
                continue
            else:
                return answer
        else:
            return default_answer


def call(command=""):
    ''' Convenient command call: it splits the command string into tokens (default separator: space)

    :param command: the command to be executed by the system
    '''
    try:
        cmd_lst = command.split()

        print("cmd lst {}".format(cmd_lst))

        subprocess.run(cmd_lst, check=True)
    except subprocess.CalledProcessError as err:
        sys.stderr.write('CalledProcessERROR:{}'.format(err))


def run(command="", parameters=[]):
    '''This version is closer to the subprocess version

    :param command: the command to be executed by the system
    :param parameters: parameters of the this command
    '''
    try:
        subprocess.run([command] + parameters, check=True, shell=True)
    except subprocess.CalledProcessError as err:
        sys.stderr.write('ERROR:', err)


def runSetup(src_dir="", lib_dir=""):
    """running a setup.py file within a pyhton script
       it requires a lot of things set...
       :param src_dir: directory containing the setup.py file
       :param lib_dir: directory containing the target lib directory
    """
    # all path settings seem to be required by run_setup and setup.py
    os.environ["PYTHONPATH"] = os.path.join(lib_dir, 'lib', 'python3.5', 'site-packages')
    sys.path.append(lib_dir)
    os.chdir(src_dir)
    setup_file = os.path.join(src_dir, 'setup.py')
    # this no longer works:
    #~ run_setup(setup_file,  script_args=['install', '--prefix', lib_dir ])
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', '.'])


def installAndImport(package):
    """ This imports a package and
        if not available, installs it first
    """
    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        import pip
        #~ pip.main(['install', package])
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', package])
    finally:
        globals()[package] = importlib.import_module(package)


# --------------- actual installation functions -----------------------------

# Display Welcome Text
installer_welcome_txt = ("______________________________________________________\n\n"
                         " ___  ____  __      __    ___  \n"
                         "/ __)(_  _)(  )    /__\  (__ \ \n"
                         "\__ \ _)(_  )(__  /(__)\  / _/ \n"
                         "(___/(____)(____)(__)(__)(____)\n\n"
                         "This is the SiLA2 Python3 installer\n"
                         "It will guide you through the complete installation \n"
                         "of the core parts of SiLA2-python ...\n"
                         "[type: ?    - for further information\n"
                         "       help - to list all input options    ]\n"
                         "______________________________________________________\n")

print(installer_welcome_txt)

# Install virtual Python3 environment ?
inst_venv = query_yes_no("Install a virtual Python environment (highly recommended) ?",
                         help="HELP: This is the recommended installation mode for testing SiLA2-Python")
if inst_venv:
    venv_dir = query("Please specify a directory for your virtual python3 environment",
                     default_answer=os.path.join(HOME_DIR, "python3", "sila2_venv"),
                     help="HELP: specify the target directory for the virtual python3 environment")

    create_venv_anyway = True

    if os.path.exists(venv_dir):
        create_venv_anyway = query_yes_no("\nWARNING !! Virtual environment exists: [{}], shall I create it anyway ?".
                                          format(venv_dir), default_answer='no',
                                          help="HELP: create the python 3 virtual environment anyway ?")

    if create_venv_anyway:
        # try/except should be replaced by importAndInstall, when this is working
        #     installAndImport('venv') # should be replaced by importAndInstall, when this is working
        try:
            import venv
        except ImportError:
            call('pip install venv')
            import venv

        print("\t...creating virtual environment in dir [{}]".format(venv_dir))
        if venv is not None:
            venv.create(venv_dir, system_site_packages=False, clear=False, symlinks=False, with_pip=True)

    print("* Activating virtual environment [{}]".format(
        venv_dir))  # this is done by prepending python3 path to system path
    os.environ['PATH'] = os.pathsep.join([os.path.join(venv_dir, 'bin'), os.environ['PATH']])

    if os.name == 'nt':
        print("* Activating virtual environment on windows [{}]".format(
            venv_dir))  # this is done by prepending python3 path to sytem path
        activate_cmd = os.path.join(venv_dir, "Scripts", "activate.bat")
        print("* Activating virtual environment on windows with: [{}]".format(
            activate_cmd))  # this is done by prepending python3 path to system path
        call(activate_cmd)

# Install dependencies ? pip3 is faster than dependencies from setup.py
if query_yes_no("Install library dependencies (recommended) ?",
                help="HELP: This will install all required packages"):
    print("Installing dependencies ...")

    requirements = os.path.join(REPO_DIR, 'sila_library', 'requirements_base.txt')

    call('pip3 install -r ' + requirements)

    # also install com requirements ... ask user here:

# Install SiLA2 libraries ?
if query_yes_no("Install SiLA2 libraries (recommended) ?",
                help="HELP: This will install core SiLA2 libraries"):
    print("Installing libraries ...")

    src_dir = os.path.join(REPO_DIR, 'sila_library')
    os.chdir(src_dir)
    call('pip3 install .')

else:
    sys.stdout.write("Well, what a pitty ... - you should really consider installing the SiLA libraries \n")

# Install SiLA tools ?
if query_yes_no("Install SiLA2 tools, like the codegenerator (recommended) ?",
                help="HELP: This will install core SiLA2 tools, like codegenerator"):
    print("Installing tools ...")

    requirements = os.path.join(REPO_DIR, 'sila_tools', 'requirements.txt')

    call('pip3 install -r ' + requirements)

    src_dir = os.path.join(REPO_DIR, 'sila_tools', 'sila2codegenerator')
    os.chdir(src_dir)
    call('pip3 install .')

# Generate server key
#   s. https://bbengfort.github.io/programmer/2017/03/03/secure-grpc.html
#     openssl genrsa -out sila_server.key 2048 #  generate a 2048 bit RSA key
# The second command will generate the certificate
#     openssl req -new -x509 -sha256 -key sila_server.key -out sila_server.crt -days 3650
# Finally, to generate a certificate signing request (.csr) using
#     openssl req -new -sha256 -key server.key -out server.csr
#     openssl x509 -req -sha256 -in server.csr -signkey server.key -out server.crt -days 3650

# Generate documentation ?
if query_yes_no("Generate documentation (recommended) ?",
                help="HELP: This will generate the documentation"):
    print("Generating documentation ...")

# Need to create a .config directory for sila ...
os.makedirs(os.path.abspath(os.path.join(HOME_DIR, ".config", "sila2")), exist_ok=True)

print("Enjoy the SiLA2 with Python3 :)")

if inst_venv:
    print(" ")
    print("--------------------------------------------------------------------")
    print("ATTENTION: Please do not forget to activate the virtual environment by calling: \n"
          "source {}/bin/activate \n"
          "- to deactivate the venv, simply type:\n"
          "deactivate \n".format(venv_dir))
    print("--------------------------------------------------------------------")
    print(" ")
