"""
________________________________________________________________________

:PROJECT: SiLA2_python

* sila_server *

:details: SiLA2 error handling.

:file:    sila_error_handling.py
:authors: mark doerr
          Florian Meinicke

:date: (creation)          20181223
:date: (last modification) 20190712

.. note:: - 0.0.6
.. todo:: -
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.6"

import logging

from enum import Enum
import grpc
import sila2lib.SiLAFramework_pb2 as sfwpb


class FrameworkErrorType(Enum):
    """ Possible error types for SiLA2 FrameworkErrors """
    COMMAND_EXECUTION_NOT_ACCEPTED = 0
    INVALID_COMMAND_EXECUTION_UUID = 1
    COMMAND_EXECUTION_NOT_FINISHED = 2
    INVALID_METADATA = 3
    NO_METADATA_ALLOWED = 4
    BINARY_UPLOAD_IMPOSSIBLE = 5


def raiseRPCError(context: grpc.ServicerContext, error: sfwpb.SiLAError):
    """Raises the given error using the underlying gRPC framework.
        :param context: gRPC context
        :param error: The SiLAError that should be serialized into a gRPC error message.
    """

    message = error.SerializeToString()
    context.set_details(message)
    context.set_code(grpc.StatusCode.ABORTED)


def getValidationError(parameter: str, message: str):
    """Validation Error is an error that occurs during validation of
       parameters before execution
        :param parameter: The parameter that failed the validation
        :param message: Information why the parameter was invalid and how to
                        resolve the error
    """

    return sfwpb.SiLAError(validationError=sfwpb.ValidationError(
        parameter=parameter, message=message
    ))


def getDefinedExecutionError(errorIdentifier: str, message: str):
    """Defined Execution Error is an Execution Error that has been defined by
       the Feature Designer as part of the Feature. The nature of the errors as
       well as possible recovery procedures are known in better detail.
        :param errorIdentifier: A valid identifier of a DefinedExecutionError
        :param message: Information about the error and how to resolve the error
    """

    return sfwpb.SiLAError(definedExecutionError=sfwpb.DefinedExecutionError(
            errorIdentifier=errorIdentifier, message=message
    ))


def getUndefinedExecutionError(message: str):
    """Any ExecutionError that is not a DefinedExecutionError is an
       UndefinedExecutionError. These errors cannot be foreseen by the Feature
       Designer and hence cannot be specified as part of the Feature.
        :param message: Information about the error and how to resolve the error
    """

    return sfwpb.SiLAError(undefinedExecutionError=sfwpb.UndefinedExecutionError(
            message=message
    ))


def getFrameworkError(errorType: SiLAFrameworkErrorType, message: str):
    """Framework Error is an error which occurs when a SiLA Client accesses
       a SiLA Server in a way that violates the SiLA2 specification.
        :param errorType: The exact type of the error
        :param message: Information about the error and how to resolve the error
    """

    return sfwpb.SiLAError(frameworkError=sfwpb.FrameworkError(
            errorType=errorType, message=message
    ))
