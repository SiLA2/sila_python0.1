"""
________________________________________________________________________

:PROJECT: SiLA2_python

* sila_client *

:details: SiLA2 client. 
           
:file:    sila_client.py
:authors: mark doerr

:date: (creation)          20190105
:date: (last modification) 20190105

.. note::
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.4"

# import general packages required
import os
import sys
import logging
from configparser import ConfigParser, NoSectionError, NoOptionError
import uuid
import grpc

# import SiLA2 library packages
import sila2lib.std_features.SiLAService_pb2_grpc as sspbgrpc
import sila2lib.std_features.SimulationController_pb2_grpc as simctrlgrpc

# from concurrent.futures import ThreadPoolExecutor
# import sila2lib.std_features.SimulationController as simctrl
# import sila2lib.std_features.SiLAService as siserv
# from sila2lib.sila_service_detection import SiLA2ServiceDetection

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class SiLA2Client:

    def __init__(self, name=None,
                 service_name=None,
                 description="Description: This is a SiLA2 client",
                 UUID=None,
                 version="0.0",
                 sila_hostname="localhost",
                 vendor_URL="lara.uni-greifswald.de",
                 ip='127.0.0.1', port=50051,
                 key='sila_server.key', cert='sila_server.crt',
                 simulation_mode=False):
        """
        Initialising and starting the gRPC server, which is the core of the SiLA server

        :param name="SiLA2TestServer": name of the Server
        :param description="A short, human readable description": description of the Server
        :param vendor_URL="testdomain.tld": vendor URL of the Server
        :param port=55001:  port of Server
        :param key="sila_server.key": filename of private server key
        :param cert="sila_server.crt": filename of server certificate
        :param simulation_mode=False: Determines whether the simulation mode is active at initialisation or not
        """ 
        
        self.name = name
        self.service_name = service_name
        
        self.simulation_mode = True
        
        self.sila_hostname = sila_hostname
        self.description = description
        self.server_UUID = UUID
        self.server_version = version 
        self.vendor_URL = vendor_URL
        
        self.server_ip = ip
        self.server_port = port
        self.server_key = key
        self.server_cert = cert

        # set the default value for the simulation mode
        self.simulation_mode = simulation_mode
    
        self.encryption = False
        
        self.readConfigFile()

        # decide whether to us hostname or IP, we prefer IP, since it is unique
        if self.server_ip is not None and self.server_ip != '':
            if self.sila_hostname is not None and self.sila_hostname != '':
                logging.info(
                    (
                        'Overwriting given hostname "{sila_hostname}" '
                        'with IP address "{ip}" since IP takes precedence'
                    ).format(sila_hostname=self.sila_hostname, ip=self.server_ip)
                )
            self.sila_hostname = self.server_ip

        # TODO: Handle encryption
        # # reading keys/certs for encryption
        # try:
        #     with open(self.server_key, 'rb') as f:
        #         self.private_key = f.read()
        #     with open(server_cert, 'rb') as f:
        #         self.certificate_chain = f.read()
        #
        #     self.encryption = True
        # except FileNotFoundError as err:
        #     logging.error("Encryption key file {} not found, please generate keys!".format(err))
        #
        # # auto service detection
        # if self.server_name is not None:
        #     # automatic connection
        #     connect_param = SiLA2ServiceDetection.findServiceByName(service_name)
        #
        #     sila_hostname = connect_param['server_name']
        #     port = connect_param['port']
            
        if self.sila_hostname != "" and port != 0:
            # manual connection
            if cert is not None:
                # encryption
                # grpc.ssl_channel_credentials(root_certificates=trusted_certs)
                credentials = grpc.ssl_channel_credentials(open(cert, 'rb').read())
                self.channel = grpc.secure_channel(':'.join((self.sila_hostname, str(port))), credentials)
            else:
                self.channel = grpc.insecure_channel(':'.join((self.sila_hostname, str(port))))
        else:
            # TODO: Implement auto detection
            logging.error('Auto-detection of server not implemented. Use IP/hostname and port for a connection')
            raise NotImplementedError
                
        # this is required to access the SiLAService functionality
        self.SiLAService_serv_stub = sspbgrpc.SiLAServiceStub(self.channel)
        self.simulation_ctrl_stub = simctrlgrpc.SimulationControllerStub(self.channel)

    def run(self):
        """ main gRPC client routine, please override!"""
        logging.warning("abstract class client run - please override this method !{}".format("run"))
        raise NotImplementedError

    def stop(self):
        """ stop SiLA client routine"""
        logging.info("remember terminating all observable commands / dynamic properties {}".format("stopping now..."))
        # self.grpc_server.stop(0)

        raise NotImplementedError
   
    def switchToSimMode(self):
        """ switching to simulation mode
            please overwrite this method in your server !
            # stopping gRPC server
               self.stop()
            # re-assign services
            # restart gRPC server
              self.run()"""
        self.simulation_mode = True
        logging.debug('Switching to simulation mode.')

        # must be implemented by derived class
        raise NotImplementedError

    def switchToRealMode(self):
        """please overwrite this method in your server !
            # stopping gRPC server
               self.stop()
            # re-assign services
            # restart gRPC server
              self.run()"""
        self.simulation_mode = False
        logging.debug('Switching to real mode.')

        # must be implemented by derived class
        raise NotImplementedError

    def toggleSimMode(self):
        """Toggle between simulation and real mode"""

        logging.debug('Toggle of simulation/real mode requested:')
        if self.simulation_mode:
            return self.switchToRealMode()
        else:
            return self.switchToSimMode()

    def simulationModeStatus(self):
        """Returns the simulation state of the client"""

        return self.simulation_mode
        
    def encryptionModeStatus(self):
        """Returns the encryption status of the connection"""

        return self.encryption

    def readConfigFile(self):
        """Read the config file for the client"""
        
        # reading config file

        # the config files are either taken from APPDATA or <HOME>/.config/sila2
        config_dir = os.path.join(os.environ.get('APPDATA')
                                  or os.path.join(os.environ['HOME'], '.config', 'sila2'), self.name)

        # generate the config filename based on the server name
        config_filename = os.path.join(config_dir, self.name + '.conf')

        logging.info('Trying to load config file "{config_filename}"'.format(
            config_filename=config_filename
        ))

        # ensure that the path for the config file exists
        if not os.path.exists(config_dir):
            os.makedirs(config_dir, exist_ok=True)
            logging.info("Config dir [{config_dir}] created".format(config_dir=config_dir))
                        
        try: 
            sila2_config = ConfigParser()
            sila2_config.read(config_filename)
        except Exception as err:
            logging.exception(
                (
                    'Error while creating config parser: {err}' '\n'
                    'Cannot permanently store UUID, generating a temporary one'
                ).format(err=err))
            self.client_UUID = uuid.uuid4()
            return
                
        try:
            self.client_UUID = sila2_config['client']['UUID']
            uuid_missing = False
        except NoSectionError as err:
            sys.stderr.write("ERROR: Please add section in your SiLA2 config file {}".format(err))
            uuid_missing = True
        except NoOptionError as err:
            sys.stderr.write("ERROR: Cannot read config file option in {}".format(err))
            uuid_missing = True
        except KeyError as err:
            sys.stderr.write("ERROR: Cannot read config file option in {}".format(err))
            uuid_missing = True

        # if no UUID is given, generate your own UUID and store it in the file
        if uuid_missing:
            self.client_UUID = uuid.uuid4()
            sila2_config['client'] = {}
            sila2_config['client']['UUID'] = str(self.client_UUID)

            logging.debug('No UUID found, automatically auto-generated an ID, this will be written to the config file')
            with open(config_filename, 'w') as file_config:
                sila2_config.write(file_config)
                
        logging.debug("Client UUID: {UUID}".format(UUID=self.client_UUID))
