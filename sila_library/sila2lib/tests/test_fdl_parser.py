"""
________________________________________________________________________

:PROJECT: SiLA2_python

*unittest SiLA2 FDL/XML parser*

:details: SiLA2 FDL/XML parser unittest.
          To run the tests type:
            python setup.py test 
            python -m unittest tests/test_fdl_parser.py 

:file:    test_fdl_parser.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20181003
:date: (last modification) 20190305

.. note:: - this is very ugly code, will be replaced by cleaner module
.. todo:: - preserv parameter and response order !!!
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.2.0"

import os
import unittest
import logging

import sila2lib.fdl_parser as fdl 

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

class TestFDLParser(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)

        self.test_fdl = os.path.join(TEST_DATA_DIR, "SiLAService.sila.xml")
        
        self.fdl_parser = fdl.FDLParser(self.test_fdl)
    
    @unittest.skip("Validation only -  disabled- validation is also tested in all other mothods...")    
    def test_FDLvalidation(self):
        """ validate FDL document
            :param [param_name]: [description]"""

        test_fdl_schema = os.path.join(TEST_DATA_DIR, "FeatureDefinition.xsd")
        
        print("\n\n======= ********* ===== {} ******** =====\n".format(test_fdl_schema) )
        
        self.assertTrue(fdl.FDLValidator(fdl_filename=self.test_fdl, fdl_schemaname=test_fdl_schema) )
        
    def test_treeBuilding(self):
        """ test tree building
            :param [param_name]: [description]
            .. to-do:: should be improved """
        
        self.assertEqual(self.fdl_parser.root.tag, "{http://www.sila-standard.org}Feature")
        
    def test_basicDataTypes(self):
        """ test basic data type creation
        """
        binary_type = self.fdl_parser.data_type_dic["Binary"]
        
        logging.debug("testing an arbitrary member of the data_type dict id: {}".format(binary_type['basic_sila_type']) )
        
        self.assertEqual(binary_type['basic_proto_type'], 'bytes')
        
    def test_parameterParsing(self):
        """ :param [param_name]: [description]"""
        
        #~ logging.debug("\n\n======= ********* ===== {} ******** =====\n".format("para parsing") )

        cmd_param_list = [cmd_para.Identifier for cmd_para in self.fdl_parser.cmd_parameter_dic.keys() ]
        #~ logging.debug("\ncmd parameter id list: {}".format(cmd_param_list) )
        #~ logging.debug("cmd parameter : {}".format(self.fdl_parser.cmd_parameter_dic) )
        
        resp_param_list = [resp.Identifier for resp in self.fdl_parser.resp_parameter_dic.keys() ]
        #~ logging.debug("\ncmd resp parameter id list: {}".format(resp_param_list) )
        #~ logging.debug("cmd resp parameter : {}".format(self.fdl_parser.resp_parameter_dic) )

        prop_param_list = [prop.Identifier for prop in self.fdl_parser.prop_resp_parameter_dic.keys() ]
        
        #~ logging.debug("\n*****************prop parameter id list: {}".format(prop_param_list) )
        #~ logging.debug("prop parameter : {}".format(self.fdl_parser.prop_resp_parameter_dic) )
        
        self.assertTrue('ImplementedFeatures' in prop_param_list)
        
        self.assertTrue( len(prop_param_list) == 7)
        
            
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    unittest.main()


