<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="0.1" FeatureVersion="1.0" MaturityLevel="Verified" Originator="org.silastandard" Category="core"
        xmlns="http://www.sila-standard.org" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>SiLAService</Identifier>
    <DisplayName>SiLA Service</DisplayName>
    <Description>
        The Feature each SiLA Server MUST implement. It is the entry point to a SiLA Server and helps to discover the Features it implements.
    </Description>

    <!-- Commands -->
    <Command>
        <Identifier>GetFeatureDefinition</Identifier>
        <DisplayName>Get Feature Definition</DisplayName>
        <Description>Get all details on one Feature through the qualified Feature id.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>QualifiedFeatureIdentifier</Identifier>
            <DisplayName>Qualified Feature Identifier</DisplayName>
            <Description>The qualified Feature identifier for which the Feature description should be retrieved.</Description>
            <DataType>
                <DataTypeIdentifier>FeatureIdentifier</DataTypeIdentifier>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>FeatureDefinition</Identifier>
            <DisplayName>Feature Definition</DisplayName>
            <Description>The Feature Definition in XML format.</Description>
            <DataType>
                <DataTypeIdentifier>FeatureDefinition</DataTypeIdentifier>
            </DataType>
        </Response>
        <StandardExecutionErrors>
            <StandardExecutionErrorIdentifier>UnimplementedFeature</StandardExecutionErrorIdentifier>
        </StandardExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetServerName</Identifier>
        <DisplayName>Set Server Name</DisplayName>
        <Description>Sets a human readable name to the Server Name property</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>ServerName</Identifier>
            <DisplayName>Server Name</DisplayName>
            <Description>The human readable name of to assign to the SiLA Server</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Parameter>
    </Command>
    <StandardExecutionError>
        <Identifier>UnimplementedFeature</Identifier>
        <DisplayName>Unimplemented Feature</DisplayName>
        <Description>The feature specified by the given feature identifier is not implemented by the server.</Description>
    </StandardExecutionError>
    <!-- Properties -->
    <Property>
        <Identifier>ServerName</Identifier>
        <DisplayName>Server Name</DisplayName>
        <Description>Human readable name of the SiLA Server.</Description>
        <DataType>
            <Basic>String</Basic>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ServerType</Identifier>
        <DisplayName>Server Type</DisplayName>
        <Description>The type of Server this is. Is specified by the implementer of the server and not unique.</Description>
        <DataType>
            <Basic>String</Basic>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ServerUUID</Identifier>
        <DisplayName>Server UUID</DisplayName>
        <Description>Globally unique identifier that identifies a SiLA Server. The Server UUID MUST
            be generated once and always remain the same.</Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Length>36</Length>
                    <Pattern>[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}</Pattern>
                </Constraints>
            </Constrained>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ServerDescription</Identifier>
        <DisplayName>Server Description</DisplayName>
        <Description>Description of the SiLA Server.</Description>
        <DataType>
            <Basic>String</Basic>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ServerVersion</Identifier>
        <DisplayName>Server Version</DisplayName>
        <Description>Returns the version of the SiLA Server. A "Major" and a "Minor" version number (e.g. 1.0) MUST be provided,
            a Patch version number MAY be provided. Optionally, an arbitrary text, separated by an underscore MAY be appended, e.g. “3.19.373_mighty_lab_devices”
        </Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Pattern>(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*))?([_a-zA-Z0-9]+)?</Pattern>
                </Constraints>
            </Constrained>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ServerVendorURL</Identifier>
        <DisplayName>Server Vendor URL</DisplayName>
        <Description>Returns the URL to the website of the vendor or the website 
            of the product of this SiLA Server.</Description>
        <DataType>
            <DataTypeIdentifier>URL</DataTypeIdentifier>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <Property>
        <Identifier>ImplementedFeatures</Identifier>
        <DisplayName>Implemented Features</DisplayName>
        <Description>Returns a list of qualified Feature identifiers of all 
            implemented Features of this SiLA Server.</Description>
        <DataType>
            <List>
                <DataType>
                    <DataTypeIdentifier>FeatureIdentifier</DataTypeIdentifier>
                </DataType>
            </List>
        </DataType>
        <Observable>No</Observable>
    </Property>
    <DataTypeDefinition>
        <Identifier>FeatureIdentifier</Identifier>
        <DisplayName>Feature Identifier</DisplayName>
        <Description>Qualified Feature Identifier as provided by the 
            ImplementedFeatures property.</Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <MaximalLength>255</MaximalLength>
                    <FullyQualifiedIdentifier>FeatureIdentifier</FullyQualifiedIdentifier>
                </Constraints>
            </Constrained>
        </DataType>
    </DataTypeDefinition>
    <DataTypeDefinition>
        <Identifier>FeatureDefinition</Identifier>
        <DisplayName>Feature Definition</DisplayName>
        <Description>The content of a Feature Definition conforming 
            with the XML Schema.</Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Schema>
                        <Type>Xml</Type>
                        <Url>https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd</Url>
                    </Schema>
                </Constraints>
            </Constrained>
        </DataType>
    </DataTypeDefinition>
    <DataTypeDefinition>
        <Identifier>URL</Identifier>
        <DisplayName>URL</DisplayName>
        <Description>Uniform Resource Locator as defined in RFC 1738.</Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Pattern>https?://.+</Pattern>
                </Constraints>
            </Constrained>
        </DataType>
    </DataTypeDefinition>
</Feature>
