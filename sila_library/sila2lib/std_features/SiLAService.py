"""
________________________________________________________________________

:PROJECT: SiLA2_python

*silaservice_server_simulation *

:details: silaservice_server_simulation: 
        The Feature each SiLA Server MUST implement. It is the entry point to a SiLA Server and helps to discover the Features it implements.
    . 
           
:file:    silaservice_server_simulation.py
:authors: mark doerr

:date: (creation)          2019-02-02T19:20:43.734709
:date: (last modification) 20190308

.. note:: Code generated automatically by SiLA2codegenerator v0.1.6!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import sila2lib.sila_error_handling as serh

import sila2lib.std_features.SiLAService_pb2 as pb2
import sila2lib.std_features.SiLAService_pb2_grpc as pb2_grpc

class SiLAService(pb2_grpc.SiLAServiceServicer):
    """ SiLAService - 
#        The Feature each SiLA Server MUST implement. It is the entry point to a SiLA Server and helps to discover the Features it implements.
#     """
    def __init__ (self, server_name="Unknown Server Name",
                        server_description="Server Description",
                        server_version="0.0",
                        server_type="TestServer",
                        server_UUID="00000000-0000-0000-0000-000000000000",
                        vendor_URL = "noname.nodomain"):
        """ SiLAService class initialiser """
        logging.debug("init class: SiLAService ")
        
        self.server_name = server_name
        self.server_description = server_description
        self.server_version = server_version
        self.server_type = server_type
        self.server_UUID = server_UUID
        self.vendor_URL = vendor_URL
        
        self.qual_feature_id = "SiLAService"
        
        logging.warning("feature ID should be fully qualified !! : {}".format(self.qual_feature_id) )
        
        self.implemented_features = {} # dictionary of all implemented and registered features
        
        # loading SiLAService FDL from library dir
        self.FDL_filename = os.path.join( os.path.dirname(__file__), "SiLAService.sila.xml" )
        
        # reading FDL file from current dir = dir of server
        # should be retrieved from processed file/pickle or database ??
        with open(self.FDL_filename, 'r',  encoding="utf8") as fdl_file:
            sila_service_fdl = fdl_file.read()
            self.registerFeature(self.qual_feature_id, sila_service_fdl)


    def GetFeatureDefinition(self, request, context):
        """Get all details on one Feature through the qualified Feature id.
            :param request: gRPC request
            :param context: gRPC context
            :param request.QualifiedFeatureIdentifier: The qualified Feature identifier for which the Feature description should be retrieved.

        """
        feature_id = request.QualifiedFeatureIdentifier.FeatureIdentifier.value
        
        logging.debug("ident: {}".format(feature_id) )
                
        try:
            return pb2.GetFeatureDefinition_Responses(
                       FeatureDefinition=pb2.DataType_FeatureDefinition(
                           FeatureDefinition=fwpb2.String(value=self.implemented_features[feature_id])
                       ) )
        except KeyError as err:
            err_msg = '*Feature not found -a1 * '
            serh.validationError(context=context, 
                                 parameter="QualifiedFeatureIdentifier.Identifier", 
                                 cause="Feature [{}] not available".format(feature_id), 
                                 action="Please provide a valid Qualified Feature Identifier")
                                 
            return pb2.GetFeatureDefinition_Responses()
            
    def SetServerName(self, request, context):
        """Sets a human readable name to the Server Name property
            :param request: gRPC request
            :param context: gRPC context
            :param request.ServerName: The human readable name of to assign to the SiLA Server

        """
        self.server_name = request.ServerName.value
        
        logging.debug("new server name: {}".format(self.server_name) )
        
        return pb2.SetServerName_Responses(  )

    def Get_ServerName(self, request, context):
        """Human readable name of the SiLA Server.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerName: Human readable name of the SiLA Server.

        """
        logging.debug("server name: {}".format(self.server_name) )
       
        return pb2.ServerName_Responses( ServerName=fwpb2.String(value=self.server_name) )

    def Get_ServerType(self, request, context):
        """The type of Server this is. Is specified by the implementer of the server and not unique.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerType: The type of Server this is. Is specified by the implementer of the server and not unique.

        """
        logging.debug("Get_ServerType: {}".format(self.server_type) )

        return pb2.ServerType_Responses( ServerType=fwpb2.String(value=self.server_type) )

    def Get_ServerUUID(self, request, context):
        """Globally unique identifier that identifies a SiLA Server. The Server UUID MUST
            be generated once and always remain the same.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerUUID: Globally unique identifier that identifies a SiLA Server. The Server UUID MUST
            be generated once and always remain the same.

        """
        logging.debug("Get_ServerUUID: {}".format(self.server_UUID) )
        
        return pb2.ServerUUID_Responses( ServerUUID=fwpb2.String(value=self.server_UUID) )

    def Get_ServerDescription(self, request, context):
        """Description of the SiLA Server.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerDescription: Description of the SiLA Server.

        """
        logging.debug("Get_ServerDescription: {}".format(self.server_description) )

        return pb2.ServerDescription_Responses( ServerDescription=fwpb2.String(value=self.server_description) )

    def Get_ServerVersion(self, request, context):
        """Returns the version of the SiLA Server. A "Major" and a "Minor" version number (e.g. 1.0) MUST be provided,
            a Patch version number MAY be provided. Optionally, an arbitrary text, separated by an underscore MAY be appended, e.g. “3.19.373_mighty_lab_devices”
        
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerVersion: Returns the version of the SiLA Server. A "Major" and a "Minor" version number (e.g. 1.0) MUST be provided,
            a Patch version number MAY be provided. Optionally, an arbitrary text, separated by an underscore MAY be appended, e.g. “3.19.373_mighty_lab_devices”
        

        """
        logging.debug("Get_ServerVersion: {}".format(self.server_version) )
        
        return pb2.ServerVersion_Responses( ServerVersion=fwpb2.String(value=self.server_version) )

    def Get_ServerVendorURL(self, request, context):
        """Returns the URL to the website of the vendor or the website 
            of the product of this SiLA Server.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ServerVendorURL: Returns the URL to the website of the vendor or the website 
            of the product of this SiLA Server.

        """
        logging.debug("Get_ServerVendorURL: {}".format(self.vendor_URL) )
        
        return pb2.ServerVendorURL_Responses( ServerVendorURL=pb2.DataType_ServerVendorURL(URL=fwpb2.String(value=self.vendor_URL)) )

    def Get_ImplementedFeatures(self, request, context):
        """Returns a list of qualified Feature identifiers of all 
            implemented Features of this SiLA Server.
            :param request: gRPC request
            :param context: gRPC context
            :param response.ImplementedFeatures: Returns a list of qualified Feature identifiers of all 
            implemented Features of this SiLA Server.

        """
        
        feature_list = [pb2.DataType_FeatureIdentifier(FeatureIdentifier=fwpb2.String(value=feature_id) ) for feature_id in self.implemented_features]
        
        logging.debug("Get_ImplementedFeatures {}".format(feature_list)  )
        
        return pb2.ImplementedFeatures_Responses( ImplementedFeatures=feature_list ) 

    def registerFeature(self, feature_id="", fdl=""):
            """ :param feature_id: feature identifier 
                :param fdl: Feature Definition Language XML data string
            """
            
            self.implemented_features[feature_id] = fdl
