"""
________________________________________________________________________

:PROJECT: SiLA2_python

*simulationcontroller_server_simulation *

:details: simulationcontroller_server_simulation: 
        This Feature provides control over the simulation behaviour of a SiLA Server.

        A SiLA Server can run in two modes:
        (a) a real mode - with real activities, e.g. addressing or controlling real hardware,
        writing to real databeses, moving real plates etc.
        (b) a simulation mode - where every command is only simulated and responses are just example returns.

        Note that certain commands and properties might not be affected by this feature if they
        do not interact with the real world.
    . 
           
:file:    simulationcontroller_server_simulation.py
:authors: mark doerr

:date: (creation)          2019-02-02T19:20:43.734709
:date: (last modification) 2019-02-02T19:20:43.734709

.. note:: Code generated automatically by SiLA2codegenerator v0.1.6!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.1"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import sila2lib.std_features.SimulationController_pb2 as pb2
import sila2lib.std_features.SimulationController_pb2_grpc as pb2_grpc


class SimulationController(pb2_grpc.SimulationControllerServicer):
    """ SimulationController - 
#        This Feature provides control over the simulation behaviour of a SiLA Server.
#
#        A SiLA Server can run in two modes:
#        (a) a real mode - with real activities, e.g. addressing or controlling real hardware,
#        writing to real databeses, moving real plates etc.
#        (b) a simulation mode - where every command is only simulated and responses are just example returns.
#
#        Note that certain commands and properties might not be affected by this feature if they
#        do not interact with the real world.
#     """
    def __init__ (self, SiLA2Server=None):
        """ SimulationController class initialiser """
        logging.debug("init class: SimulationController ")
        
        self.sila_server = SiLA2Server

    def StartSimulationMode(self, request, context):
        """
            Starting server into simulation mode with the following procedure, if the server was in real mode:
            - Terminate all running  commands that alter real world data or matter.
            - Cancel all dynamic property subscriptions that use real world data.
        
        empty parameter
        """
        
        self.sila_server.switchToSimMode()
        success = True
        logging.debug("swith sim mode:{}".format(success) )

        return pb2.StartSimulationMode_Responses(  )

    def StartRealMode(self, request, context):
        """
            Starting server into real mode with the following procedure:
            - Terminate all simulated commands and properties
            - If real world is ready (e.g. databases or hardware): Accept new commands and property
            retrievals.
        
        empty parameter
        """
        logging.debug("StartRealMode - Mode: real ")
        
        self.sila_server.switchToRealMode()
        success = True
        logging.debug("swith real mode:{}".format(success) )

        return pb2.StartRealMode_Responses( )

    def Get_SimulationMode(self, request, context):
        """Indication whether SiLA Server is in Simulation Mode or not.
            :param request: gRPC request
            :param context: gRPC context
            :param response.SimulationMode: Indication whether SiLA Server is in Simulation Mode or not.

        """
        logging.debug("Get_SimulationMode - Mode: simulation ")

        return pb2.SimulationMode_Responses( SimulationMode=fwpb2.Boolean(value=self.sila_server.simulation_mode) )




