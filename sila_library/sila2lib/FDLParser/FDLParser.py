
# import general packages
from lxml import etree, objectify
import os
import logging
import pickle

# import meta packages
from typing import Any, Dict

# load modules from this package
from .FDLSiLAReader import FDLSiLAReader
from .Command import Command
from .Property import Property
from .Metadata import Metadata
from .DataTypeDefinition import DataTypeDefinition
from .StandardErrors import StandardExecutionError, StandardReadError, DefinedExecutionError


class FDLParser(FDLSiLAReader):
    """This class builds a tree of dictionaries that represent the feature defined in an FDL/XML input file"""

    # Basic access elements for the feature definition
    #: The filename for the feature that has been read
    fdl_filename: str

    #: The root of the XML input file
    root: Any

    # General information about this feature
    #: Identifier of the command
    identifier: str

    #: Display name property of the command
    name: str

    #: Command description
    description: str

    # Attributes of the feature definition
    #: The version of the SiLA2 protocol used in this feature definition file
    SiLA2_version: str

    #: Category of this feature (optional)
    category: str

    #: Maturity level (optional, default: Draft)
    maturity_level: str

    #: Language (optional, default: en-US)
    locale: str

    #: Originator, e.g. 'org.silastandard'
    originator: str

    #: The features version
    feature_version: str

    #: Major feature version for easy comparison
    feature_version_minor: int

    #: Minor feature version for easy comparison
    feature_version_major: int

    # Storage dictionaries for everything defined in this feature
    #: Dictionary of defined commands, the commands identifier is used as key
    commands: Dict[str, Command]

    #: Dictionary of defined properties, the properties identifier is used as a key
    properties: Dict[str, Property]

    #: Dictionary of defined standard read errors, the errors identifier is used as a key (SiLA v0.1)
    standard_read_errors: Dict[str, StandardReadError]

    #: Dictionary of defined standard execution errors, the errors identifier is used as a key (SiLA v0.1)
    standard_execution_errors: Dict[str, StandardExecutionError]

    #: Dictionary of all defined execution errors, the errors identifier is used as a key (SiLA v0.2)
    defined_execution_errors: Dict[str, DefinedExecutionError]

    #: Dictionary of data types defined, the data types identifier is used as a key
    data_type_definitions: Dict[str, DataTypeDefinition]

    def __init__(self, fdl_filename, fdl_schema_filename: str = None):
        """
        FDLParser class initialiser.

        :param fdl_filename: filename of FDL file to read.
        :param fdl_schema_filename: filename of the FDL scheme. If None, it will be set the the FDL scheme stored
                                    in this package.
        """
        super().__init__(fdl_schema_filename=fdl_schema_filename)

        self.fdl_filename = fdl_filename

        # create a parser and generate python object(s) from XML structure
        schema = etree.XMLSchema(file=self.fdl_schema_filename)
        parser = objectify.makeparser(schema=schema)
        tree = objectify.parse(self.fdl_filename, parser)

        # get access to the root element
        self.root = tree.getroot()

        # extract basic attributes
        #   mandatory elements
        self.SiLA2_version = str(self.root.get('SiLA2Version'))
        self.feature_version = str(self.root.get('FeatureVersion'))
        self.feature_version_major = int(self.feature_version.split('.')[0])
        self.feature_version_minor = int(self.feature_version.split('.')[1])
        self.originator = str(self.root.get('Originator'))
        # optional elements
        self.locale = str(self.root.get('Locale')) if 'Locale' in self.root.attrib else 'en-us'
        self.maturity_level = str(self.root.get('MaturityLevel')) if 'MaturityLevel' in self.root.attrib else 'Draft'
        self.category = str(self.root.get('Category')) if 'Category' in self.root.attrib else None

        # extract feature details
        self.identifier = self.root.Identifier.text
        self.name = self.root.DisplayName.text
        self.description = self.root.Description.text

        # Import definitions first, so they are (in theory) available later on
        #   Data type definitions
        self.data_type_definitions = {}
        if hasattr(self.root, 'DataTypeDefinition'):
            for data_type_definition in self.root.DataTypeDefinition:
                obj = DataTypeDefinition(xml_tree_element=data_type_definition)
                self.data_type_definitions[obj.identifier] = obj
        # Standard Execution Errors
        self.standard_execution_errors = {}
        if hasattr(self.root, 'StandardExecutionError'):
            for standard_execution_error in self.root.StandardExecutionError:
                obj = StandardExecutionError(xml_tree_element=standard_execution_error)
                self.standard_execution_errors[obj.identifier] = obj
        # Standard Read Errors
        self.standard_read_errors = {}
        if hasattr(self.root, 'StandardReadError'):
            for standard_read_error in self.root.StandardReadError:
                obj = StandardReadError(xml_tree_element=standard_read_error)
                self.standard_read_errors[obj.identifier] = obj
        # Defined Execution Errors
        self.defined_execution_errors = {}
        if hasattr(self.root, 'DefinedExecutionError'):
            for defined_execution_error in self.root.DefinedExecutionError:
                obj = DefinedExecutionError(xml_tree_element=defined_execution_error)
                self.defined_execution_errors[obj.identifier] = obj
        # Import Server Commands, Properties, and Metadata
        self.commands = {}
        if hasattr(self.root, 'Command'):
            for xml_command in self.root.Command:
                obj = Command(xml_tree_element=xml_command)
                self.commands[obj.identifier] = obj
        self.properties = {}
        if hasattr(self.root, 'Property'):
            for xml_property in self.root.Property:
                obj = Property(xml_tree_element=xml_property)
                self.properties[obj.identifier] = obj
        self.metadata = {}
        if hasattr(self.root, 'Metadata'):
            for xml_metadata in self.root.Metadata:
                obj = Metadata(xml_tree_element=xml_metadata)
                self.metadata[obj.identifier] = obj

    def pickle_fdl_file(self, output_dir: str):
        """
        Pickling custom data types and FDL tree

           :param output_dir: pickle target dir
        """

        pickle_filename = os.path.join(output_dir, self.identifier + ".pickle")

        logging.debug("Writing pickle file to: {pickle_filename}".format(pickle_filename=pickle_filename))

        # TODO: Add standard errors, properties, commands
        #   why do we need this?
        list_to_pickle = [self.data_type_definitions, self.root]

        with open(pickle_filename, 'wb') as file:
            pickle.dump(list_to_pickle, file)
