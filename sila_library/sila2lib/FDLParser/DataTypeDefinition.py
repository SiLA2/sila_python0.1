
# import library packages
from .DataType import DataType
from .BasicType import BasicType
from .ListType import ListType
from .ConstrainedType import ConstrainedType
from .StructureType import StructureType
from .DataTypeIdentifier import DataTypeIdentifier


class DataTypeDefinition(DataType):
    """
    Class for data types that are defined directly inside the FDL file.
        This class stores information on data types that are defined in the FDL file so they can be reused in parameter
        or response definitions.

    .. seealso:: DataTypeDefinitions are read in the :class:`~.Command` class.
    """

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <DataTypeDefinition>-xml element that contains this identifier.

        :raises TypeError: No known sub-type found.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        self.is_sila_element = True

        # Every data type definition **must** have a DataType sub-element, so we create a sub-element from that
        if hasattr(xml_tree_element.DataType, 'Basic'):
            self.sub_type = BasicType(xml_tree_element=xml_tree_element.DataType.Basic)
        elif hasattr(xml_tree_element.DataType, 'List'):
            self.sub_type = ListType(xml_tree_element=xml_tree_element.DataType.List)
        elif hasattr(xml_tree_element.DataType, 'Structure'):
            self.sub_type = StructureType(xml_tree_element=xml_tree_element.DataType.Structure)
        elif hasattr(xml_tree_element.DataType, 'Constrained'):
            self.sub_type = ConstrainedType(xml_tree_element=xml_tree_element.DataType.Constrained)
        elif hasattr(xml_tree_element.DataType, 'DataTypeIdentifier'):
            self.sub_type = DataTypeIdentifier(xml_tree_element=xml_tree_element.DataType.DataTypeIdentifier)
        else:
            # invalid type
            raise TypeError
