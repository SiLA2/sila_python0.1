
# import library packages
from .ParameterDataType import ParameterDataType


class IntermediateDataType(ParameterDataType):
    """
    The class for intermediate responses.
        This is essentially identical to a :class:`~.ParameterDataType`, however can be handled differently in the final
        application and thus exists as its own class/object.

    .. note:: When checking whether an object is an intermediate response or a parameter, note that
              :func:`isinstance(obj, ParameterDataType)` will also return true if the object is a
              :class:`IntermediateDataType`, since they are derived from each other. Use
              ``type(obj) is ParameterDataType`` for a precise check.
    """

    pass
