
# import meta packages
from typing import Any


class StandardError:
    """Base class for all standard errors that are defined."""

    #: Identifier of the standard error
    identifier: str

    #: Display name property of the standard error
    name: str

    #: Standard error description
    description: str

    #: Reference to the xml-tree of the property
    _tree: Any

    def __init__(self, xml_tree_element):
        """
        Class initialiser

        :param xml_tree_element: The contents of the <Property>-xml node
        """
        self._tree = xml_tree_element

        self.identifier = xml_tree_element.Identifier.text
        self.name = xml_tree_element.DisplayName
        self.description = xml_tree_element.Description


class StandardExecutionError(StandardError):
    """
    Storage class for standard execution errors.
        This class can be used to store standard errors that occur while executing a command and are usually read in
        :class:`~.Command`. Since these are have no special properties, this class is effectively the identical
        implementation to :class:`StandardError`

    .. note:: This type of error was used in SiLA v0.1 and is not available in higher version sof the SiLA protocol.
              It is however still supported for legacy reasons.
    """
    pass


class StandardReadError(StandardError):
    """
    Storage class for standard read errors.
        This class can be used to store standard errors that occur while reading a property and are usually read in
        :class:`~.Property`. Since these are have no special properties, this class is effectively the identical
        implementation to :class:`StandardError`

    .. note:: This type of error was used in SiLA v0.1 and is not available in higher version sof the SiLA protocol.
              It is however still supported for legacy reasons.
    """
    pass


class DefinedExecutionError(StandardError):
    """
    Storage class for defined execution errors.
        This class can be used to store all standard execution errors that occur while executing a command or reading a
        property. Consequently, these objects are usually created by the :class:`~.Command` and :class:`~.Property` #
        classes. Since this class has no special properties, it is effectively the identical implementation to any
        :class:`StandardError`

    .. note:: This class supersedes the :class:`StandardReadError` and :class:`StandardExecutionError` classes that were
              used in the version 0.1 of the SiLA standard.
    """
    pass
