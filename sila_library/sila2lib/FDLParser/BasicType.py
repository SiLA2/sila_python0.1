
# import general packages
import datetime

# import meta packages
from typing import Any

# import package related packages
from .DataType import DataType


class BasicType(DataType):
    """
    Class for the basic data type as defined in the SiLA standards.
    """

    #: Default value, only available for BasicTypes
    default_value: Any

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <Basic>-xml element that contains this basic type.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        # Set specific data
        self.is_basic = True
        self.sub_type = xml_tree_element.text

        # The name is simply equal to the underlying data type
        self.name = self.sub_type

        # and now distinguish between the named types
        if self.sub_type == 'Boolean':
            self.description = "Basic logic/boolean type"
            self.default_value = False
        elif self.sub_type == 'Integer':
            self.description = ''
            self.default_value = 0
        elif self.sub_type == 'Real':
            self.description = 'Basic float/double type'
            self.default_value = 0.0
        elif self.sub_type == 'String':
            self.description = 'Basic string type'
            self.default_value = "''"
        elif self.sub_type == 'Binary':
            self.description = 'Basic byte type'
            self.default_value = b""
        elif self.sub_type == 'Void':
            self.description = 'Void/empty type'
            self.default_value = b""
        elif self.sub_type == 'Date':
            # TODO: Check if the implementation is correct, I have so far no clue how 'Date' is represented in the FDL
            self.description = 'Basic date type'
            self.default_value = datetime.datetime.utcnow().strftime('%Y-%m-%d +0000')
        elif self.sub_type == 'Time':
            # TODO: Check if the implementation is correct, I have so far no clue how 'Time' is represented in the FDL
            self.description = 'Basic time type'
            self.default_value = datetime.datetime.utcnow().strftime('%H:%M:%S +0000')
        elif self.sub_type == 'Timestamp':
            # TODO: Check if the implementation is correct, I have so far no clue how 'Timestamp' is represented in the
            #  FDL
            self.description = 'Basic timestamp type'
            self.default_value = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S +0000')
        else:
            raise TypeError
