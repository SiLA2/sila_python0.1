
# import general Packages
import unittest
import os

# import package related modules and classes
from ..FDLParser import FDLParser
from ..Command import Command
from ..Property import Property
from ..DataTypeDefinition import DataTypeDefinition
from ..StandardErrors import StandardReadError, StandardExecutionError


class TestFDLParser(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        Create the basic path in which the input xml files are stored.
        """
        self.base_path = os.path.join(os.path.dirname(__file__), "fdl")

    def test_feature(self):
        obj = FDLParser(os.path.join(self.base_path, "Simple.sila.xml"))

        self.assertEqual(obj.root.tag, '{http://www.sila-standard.org}Feature')

    def test_attributes(self):
        """
        Test of all attributes are read correctly
            For this test it is assumed that no optional attributes are present and only default values are found.
        """
        obj = FDLParser(os.path.join(self.base_path, "Simple.sila.xml"))

        # start with mandatory attributes
        self.assertEqual(obj.feature_version, '1.3')
        self.assertEqual(obj.feature_version_major, 1)
        self.assertEqual(obj.feature_version_minor, 3)
        self.assertEqual(obj.originator, 'org.silastandard')
        self.assertEqual(obj.SiLA2_version, '0.1')
        # optional arguments and defaults
        self.assertEqual(obj.maturity_level, 'Draft')
        self.assertIsNone(obj.category)
        self.assertEqual(obj.locale, 'en-us')

    def test_attributes_optional(self):
        """
        Tests if optional attributes are read correctly if not set.
            For this test all optional attributes must be set.
        """
        obj = FDLParser(os.path.join(self.base_path, "Simple_AttributesOptional.sila.xml"))

        self.assertEqual(obj.category, 'example')
        self.assertEqual(obj.locale, 'en-GB')
        self.assertEqual(obj.maturity_level, 'Verified')

    def test_elements_base(self):
        """Tests if the base elements of a feature are read correctly."""
        obj = FDLParser(os.path.join(self.base_path, "Simple.sila.xml"))

        self.assertEqual(obj.identifier, 'SimpleFeature')
        self.assertEqual(obj.name, 'Simple Feature')
        self.assertEqual(
            obj.description,
            'Minimal feature definition, nothing is required. Can be used to check (default) attributes.'
        )

    def test_elements_complete(self):
        """Tests if all elements (one of each) are read correctly."""
        obj = FDLParser(os.path.join(self.base_path, "Complete.sila.xml"))

        self.assertEqual(len(obj.commands), 1)
        self.assertIn('CommandIdentifier', obj.commands)
        self.assertIs(type(obj.commands['CommandIdentifier']), Command)

        self.assertEqual(len(obj.properties), 1)
        self.assertIn('PropertyIdentifier', obj.properties)
        self.assertIs(type(obj.properties['PropertyIdentifier']), Property)

        self.assertEqual(len(obj.data_type_definitions), 1)
        self.assertIn('DataTypeDefinitionIdentifier', obj.data_type_definitions)
        self.assertIs(type(obj.data_type_definitions['DataTypeDefinitionIdentifier']), DataTypeDefinition)

        self.assertEqual(len(obj.standard_execution_errors), 1)
        self.assertIn('StandardExecutionErrorIdentifier', obj.standard_execution_errors)
        self.assertIs(type(obj.standard_execution_errors['StandardExecutionErrorIdentifier']), StandardExecutionError)

        self.assertEqual(len(obj.standard_read_errors), 1)
        self.assertIn('StandardReadErrorIdentifier', obj.standard_read_errors)
        self.assertIs(type(obj.standard_read_errors['StandardReadErrorIdentifier']), StandardReadError)
