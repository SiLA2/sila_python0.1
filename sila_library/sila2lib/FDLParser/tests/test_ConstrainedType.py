
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..ConstrainedType import ConstrainedType
from ..ListType import ListType
from ..BasicType import BasicType
from ..DataTypeIdentifier import DataTypeIdentifier

# import custom exceptions
from ..Exceptions.Exceptions import SubDataTypeError


class TestConstrainedType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.
            Loads all constraint strings so they can be used more easily later
        """
        from ._data_ConstrainedType import data_constraint_length, data_constraint_length_minimal, \
            data_constraint_length_maximal, data_constraint_enumeration, data_constraint_pattern, \
            data_constraint_value_maximal_exclusive, data_constraint_value_maximal_inclusive, \
            data_constraint_value_minimal_exclusive, data_constraint_value_minimal_inclusive, \
            data_constraint_elements_minimal, data_constraint_elements_maximal, data_constraint_unit, \
            data_constraint_content_type, data_constraint_identifier, data_constraint_schema
        self.constraint_strings = {
            'Length':                   data_constraint_length,
            'MinimalLength':            data_constraint_length_minimal,
            'MaximalLength':            data_constraint_length_maximal,
            'Enumeration':              data_constraint_enumeration,
            'Pattern':                  data_constraint_pattern,
            'MaximalExclusive':         data_constraint_value_maximal_exclusive,
            'MaximalInclusive':         data_constraint_value_maximal_inclusive,
            'MinimalExclusive':         data_constraint_value_minimal_exclusive,
            'MinimalInclusive':         data_constraint_value_minimal_inclusive,
            'MinimalElementCount':      data_constraint_elements_minimal,
            'MaximalElementCount':      data_constraint_elements_maximal,
            'Unit':                     data_constraint_unit,
            'ContentType':              data_constraint_content_type,
            'FullyQualifiedIdentifier': data_constraint_identifier,
            'Schema':                   data_constraint_schema
        }

    def test_basic(self):
        from ._data_ConstrainedType import data_basic

        obj = ConstrainedType(objectify.fromstring(data_basic).Constrained)
        self.assertIs(type(obj.sub_type), BasicType)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_constrained)

    def test_list(self):
        from ._data_ConstrainedType import data_list

        obj = ConstrainedType(objectify.fromstring(data_list).Constrained)
        self.assertIs(type(obj.sub_type), ListType)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_constrained)

    def test_invalid_subtype_constrained(self):
        from ._data_ConstrainedType import data_constrained

        with self.assertRaises(SubDataTypeError):
            _ = ConstrainedType(objectify.fromstring(data_constrained).Constrained)

    def test_invalid_subtype_structure(self):
        from ._data_ConstrainedType import data_structure

        with self.assertRaises(SubDataTypeError):
            _ = ConstrainedType(objectify.fromstring(data_structure).Constrained)

    @unittest.skip("Undefined behaviour, constraints have no identifier.")
    def test_data_type_identifier(self):
        from ._data_ConstrainedType import data_data_type_identifier

        obj = ConstrainedType(objectify.fromstring(data_data_type_identifier).Constrained)
        self.assertIs(type(obj.sub_type), DataTypeIdentifier)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_constrained)

    def test_basic_constraints(self):
        # for this test we want to ignore all warnings
        import warnings
        warnings.simplefilter("ignore")

        from ._data_ConstrainedType import data_basic_template

        for constraint in self.constraint_strings:
            # some features are not yet implemented, we ignore those
            #   TODO: Remove when implemented
            try:
                input_xml = data_basic_template.format(constraint=self.constraint_strings[constraint])
                obj = ConstrainedType(objectify.fromstring(input_xml).Constrained)

                with self.subTest(constraint=constraint):
                    self.assertIsNotNone(obj[constraint])
            except NotImplementedError:
                continue

    def test_list_constraints(self):
        # for this test we want to ignore all warnings
        import warnings
        warnings.simplefilter("ignore")

        from ._data_ConstrainedType import data_list_template

        for constraint in self.constraint_strings:
            # some features are not yet implemented, we ignore those
            #   TODO: Remove when implemented
            try:
                input_xml = data_list_template.format(constraint=self.constraint_strings[constraint])
                obj = ConstrainedType(objectify.fromstring(input_xml).Constrained)

                with self.subTest(constraint=constraint):
                    self.assertIsNotNone(obj[constraint])
            except NotImplementedError:
                continue

    # TODO: Check constraints
    #  * Test if constraints have been read correctly
    #  * Test if warnings are raised as expected, if a constrained is applied to an invalid type
    #  * Multiple constraints at once
    #  * Setting constraints via dictionary access
    # TODO: Sub-types
    #  * Sub type DataTypeIdentifier, currently raises NotImplementedError (test skipped above)
