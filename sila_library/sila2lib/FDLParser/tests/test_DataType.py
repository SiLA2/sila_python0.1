
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..BasicType import BasicType


class TestDataType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_tree(self):
        from ._data_BasicType import data_boolean
        obj = BasicType(objectify.fromstring(data_boolean).Basic)

        self.assertEqual(objectify.fromstring(data_boolean).Basic, obj._tree)
