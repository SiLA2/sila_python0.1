
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..ParameterDataType import ParameterDataType
from ..BasicType import BasicType
from ..ListType import ListType
from ..StructureType import StructureType
from ..ConstrainedType import ConstrainedType
from ..DataTypeIdentifier import DataTypeIdentifier


class TestParameterDataType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_basic(self):
        from ._data_ParameterDataType import data_basic

        obj = ParameterDataType(objectify.fromstring(data_basic).Parameter)
        self.assertIs(type(obj.sub_type), BasicType)
        self.assertEqual(
            obj.description,
            "This parameter defines a basic parameter."
        )
        self.assertEqual(
            obj.name,
            "Basic Parameter"
        )
        self.assertEqual(
            obj.identifier,
            "BasicParameter"
        )

    def test_list(self):
        from ._data_ParameterDataType import data_list

        obj = ParameterDataType(objectify.fromstring(data_list).Parameter)
        self.assertIs(type(obj.sub_type), ListType)
        self.assertEqual(
            obj.description,
            "This parameter defines a list parameter."
        )
        self.assertEqual(
            obj.name,
            "List Parameter"
        )
        self.assertEqual(
            obj.identifier,
            "ListParameter"
        )

    def test_structure(self):
        from ._data_ParameterDataType import data_structure

        obj = ParameterDataType(objectify.fromstring(data_structure).Parameter)
        self.assertIs(type(obj.sub_type), StructureType)
        self.assertEqual(
            obj.description,
            "This parameter defines a structure parameter."
        )
        self.assertEqual(
            obj.name,
            "Structure Parameter"
        )
        self.assertEqual(
            obj.identifier,
            "StructureParameter"
        )

    def test_constrained(self):
        from ._data_ParameterDataType import data_constrained

        obj = ParameterDataType(objectify.fromstring(data_constrained).Parameter)
        self.assertIs(type(obj.sub_type), ConstrainedType)
        self.assertEqual(
            obj.description,
            "This parameter defines a constrained parameter."
        )
        self.assertEqual(
            obj.name,
            "Constrained Parameter"
        )
        self.assertEqual(
            obj.identifier,
            "ConstrainedParameter"
        )

    def test_data_type_identifier(self):
        from ._data_ParameterDataType import data_data_type_identifier

        obj = ParameterDataType(objectify.fromstring(data_data_type_identifier).Parameter)
        self.assertIs(type(obj.sub_type), DataTypeIdentifier)
        self.assertEqual(
            obj.description,
            "This parameter defines a parameter that has been defined previously."
        )
        self.assertEqual(
            obj.name,
            "DefinedDataType Parameter"
        )
        self.assertEqual(
            obj.identifier,
            "DefinedDataType"
        )
