
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..StandardErrors import StandardError, StandardExecutionError, StandardReadError, DefinedExecutionError


class TestStandardError(unittest.TestCase):
    """
    This unittest tests the :class:`~..StandardError` class.
        Theoretically we would need to test the classes :class:`~..StandardExecutionError` and
        :class:`..StandardReadError`, too, as well as the :class: `DefinedExecutionError` added in SiLA v0.2, however,
        since they are simply derived from the base class without any changes we simply go for the base class and only
        once construct the derived types to have them covered.
    """

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        # define the patterns which we expect to read for the different errors
        self.read_error_identifier = "ReadError{count}"
        self.read_error_display_name = "Read Error {count}"
        self.read_error_description = "{count}. defined read error that can occur."
        self.execute_error_identifier = "ExecutionError{count}"
        self.execute_error_display_name = "Execution Error {count}"
        self.execute_error_description = "{count}. error that can occur when executing a command."
        self.defined_error_identifier = "ExecutionError{count}"
        self.defined_error_display_name = "Execution Error {count}"
        self.defined_error_description = "{count}. error that can occur when executing a command or reading a property."

    def test_tree(self):
        from ._data_StandardErrors import data_standard_execution_error, data_standard_read_error, \
            data_defined_execution_error

        with self.subTest(error_type="StandardReadError"):
            xml_input = objectify.fromstring(data_standard_read_error)
            for standard_error in xml_input.StandardReadError:
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(standard_error, obj._tree)

        with self.subTest(error_type="StandardExecutionError"):
            xml_input = objectify.fromstring(data_standard_execution_error)
            for standard_error in xml_input.StandardExecutionError:
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(standard_error, obj._tree)

        with self.subTest(error_type="DefinedExecutionError"):
            xml_input = objectify.fromstring(data_defined_execution_error)
            for standard_error in xml_input.DefinedExecutionError:
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(standard_error, obj._tree)

    def test_attributes(self):
        from ._data_StandardErrors import data_standard_execution_error, data_standard_read_error, \
            data_defined_execution_error

        with self.subTest(error_type="StandardReadError"):
            xml_input = objectify.fromstring(data_standard_read_error)
            for counter, standard_error in enumerate(xml_input.StandardReadError, start=1):
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(self.read_error_identifier.format(count=counter), obj.identifier)
                self.assertEqual(self.read_error_display_name.format(count=counter), obj.name)
                self.assertEqual(self.read_error_description.format(count=counter), obj.description)

        with self.subTest(error_type="StandardExecutionError"):
            xml_input = objectify.fromstring(data_standard_execution_error)
            for counter, standard_error in enumerate(xml_input.StandardExecutionError, start=1):
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(self.execute_error_identifier.format(count=counter), obj.identifier)
                self.assertEqual(self.execute_error_display_name.format(count=counter), obj.name)
                self.assertEqual(self.execute_error_description.format(count=counter), obj.description)

        with self.subTest(error_type="DefinedExecutionError"):
            xml_input = objectify.fromstring(data_defined_execution_error)
            for counter, standard_error in enumerate(xml_input.DefinedExecutionError, start=1):
                obj = StandardError(xml_tree_element=standard_error)

                self.assertEqual(self.defined_error_identifier.format(count=counter), obj.identifier)
                self.assertEqual(self.defined_error_display_name.format(count=counter), obj.name)
                self.assertEqual(self.defined_error_description.format(count=counter), obj.description)

    def test_StandardExecutionError(self):
        from ._data_StandardErrors import data_standard_execution_error

        xml_input = objectify.fromstring(data_standard_execution_error)
        obj = StandardExecutionError(xml_tree_element=xml_input.StandardExecutionError[0])

        self.assertIsInstance(obj, StandardError)
        self.assertIsInstance(obj, StandardExecutionError)
        self.assertNotIsInstance(obj, StandardReadError)

    def test_StandardReadError(self):
        from ._data_StandardErrors import data_standard_read_error

        xml_input = objectify.fromstring(data_standard_read_error)
        obj = StandardReadError(xml_tree_element=xml_input.StandardReadError[0])

        self.assertIsInstance(obj, StandardError)
        self.assertIsInstance(obj, StandardReadError)
        self.assertNotIsInstance(obj, StandardExecutionError)

    def test_DefinedExecutionError(self):
        from ._data_StandardErrors import data_defined_execution_error

        xml_input = objectify.fromstring(data_defined_execution_error)
        obj = DefinedExecutionError(xml_tree_element=xml_input.DefinedExecutionError[0])

        self.assertIsInstance(obj, StandardError)
        self.assertIsInstance(obj, DefinedExecutionError)
