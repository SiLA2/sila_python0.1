
#: Define a list of Basic data types
data_basic = """
<root><List>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
</List></root>
"""

#: Define a nested list xml-tree. This is actually not allowed in the standard
data_list = """
<root><List>
    <DataType>
        <List>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </List>
    </DataType>
</List></root>
"""

#: Define a list of structures
data_structure = """
<root><List>
    <DataType>
        <Structure>
            <Element>
                <Identifier>ListStructureElement</Identifier>
                <DisplayName>List-Structure Element</DisplayName>
                <Description>This parameter defines an element of a structure that is inside a list.</Description>
                <DataType>
                    <Basic>Boolean</Basic>
                </DataType>
            </Element>
        </Structure>
    </DataType>
</List></root>
"""

#: Define a list of constrained elements
data_constrained = """
<root><List>
    <DataType>
        <Constrained>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
            <Constraints>
                <!-- we do not define any constraints here -->
            </Constraints>
        </Constrained>
    </DataType>
</List></root>
"""

#: Define a list of (previously) defined data types
data_data_type_identifier = """
<root><List>
    <DataType>
        <DataTypeIdentifier>TestDataType</DataTypeIdentifier>
    </DataType>
</List></root>
"""