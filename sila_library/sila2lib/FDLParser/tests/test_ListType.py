
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..ListType import ListType
from ..BasicType import BasicType
from ..StructureType import StructureType
from ..ConstrainedType import ConstrainedType
from ..DataTypeIdentifier import DataTypeIdentifier

# import custom exceptions
from ..Exceptions.Exceptions import SubDataTypeError


class TestListType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_basic(self):
        from ._data_ListType import data_basic

        obj = ListType(objectify.fromstring(data_basic).List)
        self.assertIs(type(obj.sub_type), BasicType)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_list)

    def test_list(self):
        from ._data_ListType import data_list

        # List of lists are not allowed
        with self.assertRaises(SubDataTypeError):
            _ = ListType(objectify.fromstring(data_list).List)

    def test_structure(self):
        from ._data_ListType import data_structure

        obj = ListType(objectify.fromstring(data_structure).List)
        self.assertIs(type(obj.sub_type), StructureType)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_list)

    def test_constrained(self):
        from ._data_ListType import data_constrained

        obj = ListType(objectify.fromstring(data_constrained).List)
        self.assertIs(type(obj.sub_type), ConstrainedType)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_list)

    def test_data_type_identifier(self):
        from ._data_ListType import data_data_type_identifier

        obj = ListType(objectify.fromstring(data_data_type_identifier).List)
        self.assertIs(type(obj.sub_type), DataTypeIdentifier)
        self.assertEqual(obj.name, '')
        self.assertEqual(obj.description, '')
        self.assertEqual(obj.identifier, '')
        self.assertTrue(obj.is_list)
