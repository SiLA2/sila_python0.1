"""This module defines xml-data that can be used to test Constrained data type"""

#: Define a template of a constrained on a basic data type
data_basic_template = """
<root><Constrained>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
    <Constraints>
        {constraint}
    </Constraints>
</Constrained></root>"""

#: Define a template of a constrained on a list data type
data_list_template = """
<root><Constrained>
    <DataType>
        <List>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </List>
    </DataType>
    <Constraints>
        {constraint}
    </Constraints>
</Constrained></root>"""

#: Define a template for a constrained constrained (not allowed -> Exception SubDataTypeError)
data_constrained_template = """
<root><Constrained>
    <DataType>
        <Constrained>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
            <Constraints>
                <!-- we do not define any constraints here -->
            </Constraints>
        </Constrained>
    </DataType>
    <Constraints>
        {constraint}
    </Constraints>
</Constrained></root>"""

#: Define a template for a constrained structure (not allowed -> Exception SubDataTypeError)
data_structure_template = """
<root><Constrained>
    <DataType>
        <Structure>
            <Element>
                <Identifier>BasicElement</Identifier>
                <DisplayName>Basic Element</DisplayName>
                <Description>This parameter defines a basic element.</Description>             
                <DataType>
                    <Basic>Boolean</Basic>
                </DataType>
            </Element>
        </Structure>
    </DataType>
    <Constraints>
        {constraint}
    </Constraints>
</Constrained></root>"""

#: Define a template for a constrained data type identifier (is this allowed?)
data_data_type_identifier_template = """
<root><Constrained>
    <DataType>
        <DataTypeIdentifier>TestDataType</DataTypeIdentifier>
    </DataType>
    <Constraints>
        {constraint}
    </Constraints>
</Constrained></root>"""

#: Simple basic constrained type without any constraints
data_basic = data_basic_template.format(constraint="<!-- we do not define any constraints here -->")

#: Simple list constrained type without any constraints
data_list = data_list_template.format(constraint="<!-- we do not define any constraints here -->")

#: Simple constrained constrained type without any constraints (invalid)
data_constrained = data_constrained_template.format(constraint="<!-- we do not define any constraints here -->")

#: Simple constrained structure type without any constraints (invalid)
data_structure = data_structure_template.format(constraint="<!-- we do not define any constraints here -->")

#: Simple constrained structure type without any constraints (invalid)
data_data_type_identifier = data_data_type_identifier_template.format(
    constraint="<!-- we do not define any constraints here -->"
)

#: Length constraint
data_constraint_length = """
<Length>42</Length>
"""

#: Minimal length constraint
data_constraint_length_minimal = """
<MinimalLength>42</MinimalLength>
"""

#: Maximal length constraint
data_constraint_length_maximal = """
<MaximalLength>42</MaximalLength>
"""

#: Enumeration constraint
data_constraint_enumeration = """
<Enumeration>
    <Value>A</Value>
    <Value>B</Value>
    <Value>C</Value>
</Enumeration>
"""

#: Pattern constraint
data_constraint_pattern = """
<Pattern>[a-zA-Z0-9_]</Pattern>
"""

#: Maximal value constraint (exclusive)
data_constraint_value_maximal_exclusive = """
<MaximalExclusive>42</MaximalExclusive>
"""

#: Maximal value constraint (inclusive)
data_constraint_value_maximal_inclusive = """
<MaximalInclusive>42</MaximalInclusive>
"""

#: Minimal value constraint (exclusive)
data_constraint_value_minimal_exclusive = """
<MinimalExclusive>42</MinimalExclusive>
"""

#: Minimal value constraint (inclusive)
data_constraint_value_minimal_inclusive = """
<MinimalInclusive>42</MinimalInclusive>
"""

#: Unit constraint
data_constraint_unit = """
<Unit>
    <Label>Unit-Label</Label>
    <UnitComponent>
        <SIUnit>Dimensionless</SIUnit>
    </UnitComponent>
    <Exponent>1</Exponent>
    <Factor>1</Factor>
    <Offset>0</Offset>
</Unit>
"""

#: Content type constraint
data_constraint_content_type = """
<ContentType>
    <Type>Type</Type>
    <Subtype>Subtype</Subtype>
    <Parameters>
        <Parameter>
            <Attribute>Attribute_1</Attribute>
            <Value>Value_1</Value>
        </Parameter>
        <Parameter>
            <Attribute>Attribute_2</Attribute>
            <Value>Value_2</Value>
        </Parameter>
        <Parameter>
            <Attribute>Attribute_3</Attribute>
            <Value>Value_3</Value>
        </Parameter>
    </Parameters>
</ContentType>
"""

#: Minimal number of elements constraint
data_constraint_elements_minimal = """
<MinimalElementCount>42</MinimalElementCount>
"""

#: Maximal number of elements constraint
data_constraint_elements_maximal = """
<MaximalElementCount>42</MaximalElementCount>
"""

#: Identifier constraint
data_constraint_identifier = """
<FullyQualifiedIdentifier>
    <FeatureIdentifier>FeatureIdentifier</FeatureIdentifier>
</FullyQualifiedIdentifier>
"""

#: Schema constraint
data_constraint_schema = """
<Schema>
    <Type>Xml</Type>
    <Url>http://www.w3.org/2001/XMLSchema</Url>
</Schema>
"""
