
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..ParameterDataType import ParameterDataType
from ..ResponseDataType import ResponseDataType


class TestResponseDataType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_response_data_type_object(self):
        """
        Check whether we can identify a ResponseDataType.
            A ResponseDataType object is effectively identical to a ParameterResponseType object. So we have only one
            way to distinguish them, which we test here and now.
        """
        from ._data_ParameterDataType import data_basic
        obj = ResponseDataType(objectify.fromstring(data_basic).Parameter)

        self.assertIs(type(obj), ResponseDataType)
        self.assertIsNot(type(obj), ParameterDataType)
