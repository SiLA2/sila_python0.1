
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..Command import Command
from ..ParameterDataType import ParameterDataType
from ..ResponseDataType import ResponseDataType
from ..IntermediateDataType import IntermediateDataType
from ..BasicType import BasicType


class TestCommand(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_tree(self):
        from ._data_Command import data_simple

        xml_input = objectify.fromstring(data_simple).Command
        obj = Command(xml_tree_element=xml_input)

        self.assertEqual(xml_input, obj._tree)

    def test_attributes(self):
        from ._data_Command import data_simple

        obj = Command(xml_tree_element=objectify.fromstring(data_simple).Command)
        # check simple attributes
        self.assertEqual(
            obj.identifier,
            "CommandIdentifier"
        )
        self.assertEqual(
            obj.name,
            "Command Name"
        )
        self.assertEqual(
            obj.description,
            "Simple, unobservable command without any complex elements"
        )
        self.assertFalse(obj.observable)
        # check parameter and responses
        self.assertEqual(len(obj.parameters), 1)
        self.assertEqual(len(obj.responses), 1)
        self.assertIn('ParameterIdentifier', obj.parameters)
        self.assertIn('ResponseIdentifier', obj.responses)
        self.assertIs(type(obj.parameters['ParameterIdentifier']), ParameterDataType)
        self.assertIs(type(obj.responses['ResponseIdentifier']), ResponseDataType)

    def test_observable(self):
        from ._data_Command import data_observable

        obj = Command(xml_tree_element=objectify.fromstring(data_observable).Command)
        self.assertTrue(obj.observable)

    def test_parameter(self):
        from ._data_Command import data_empty_parameter, data_empty_both, data_multiple_parameter

        with self.subTest(parameter="empty_parameter"):
            obj = Command(xml_tree_element=objectify.fromstring(data_empty_parameter).Command)

            self.assertEqual(len(obj.parameters), 1)
            self.assertEqual(len(obj.responses), 1)
            self.assertIn('EmptyParameter', obj.parameters)
            self.assertIs(type(obj.parameters['EmptyParameter']), ParameterDataType)
            self.assertIs(type(obj.parameters['EmptyParameter'].sub_type), BasicType)
            self.assertEqual(
                obj.parameters['EmptyParameter'].sub_type.sub_type,
                'Void'
            )
        with self.subTest(parameter="empty_both"):
            obj = Command(xml_tree_element=objectify.fromstring(data_empty_both).Command)

            self.assertEqual(len(obj.parameters), 1)
            self.assertEqual(len(obj.responses), 1)
            self.assertIn('EmptyParameter', obj.parameters)
            self.assertIs(type(obj.parameters['EmptyParameter']), ParameterDataType)
            self.assertIs(type(obj.parameters['EmptyParameter'].sub_type), BasicType)
            self.assertEqual(
                obj.parameters['EmptyParameter'].sub_type.sub_type,
                'Void'
            )
        with self.subTest(parameter="multiple"):
            obj = Command(xml_tree_element=objectify.fromstring(data_multiple_parameter).Command)

            self.assertEqual(len(obj.parameters), 3)
            self.assertEqual(len(obj.responses), 1)
            for index in range(1, 3):
                identifier = 'ParameterIdentifier{index}'.format(index=index)
                self.assertIn(identifier, obj.parameters)
                self.assertIs(type(obj.parameters[identifier]), ParameterDataType)

    def test_responses(self):
        from ._data_Command import data_empty_response, data_empty_both, data_multiple_responses

        with self.subTest(parameter="empty_response"):
            obj = Command(xml_tree_element=objectify.fromstring(data_empty_response).Command)

            self.assertEqual(len(obj.parameters), 1)
            self.assertEqual(len(obj.responses), 1)
            self.assertIn('EmptyResponse', obj.responses)
            self.assertIs(type(obj.responses['EmptyResponse']), ResponseDataType)
            self.assertIs(type(obj.responses['EmptyResponse'].sub_type), BasicType)
            self.assertEqual(
                obj.responses['EmptyResponse'].sub_type.sub_type,
                'Void'
            )
        with self.subTest(parameter="empty_both"):
            obj = Command(xml_tree_element=objectify.fromstring(data_empty_both).Command)

            self.assertEqual(len(obj.parameters), 1)
            self.assertEqual(len(obj.responses), 1)
            self.assertIn('EmptyResponse', obj.responses)
            self.assertIs(type(obj.responses['EmptyResponse']), ResponseDataType)
            self.assertIs(type(obj.responses['EmptyResponse'].sub_type), BasicType)
            self.assertEqual(
                obj.responses['EmptyResponse'].sub_type.sub_type,
                'Void'
            )
        with self.subTest(parameter="multiple"):
            obj = Command(xml_tree_element=objectify.fromstring(data_multiple_responses).Command)

            self.assertEqual(len(obj.parameters), 1)
            self.assertEqual(len(obj.responses), 3)
            for index in range(1, 3):
                identifier = 'ResponseIdentifier{index}'.format(index=index)
                self.assertIn(identifier, obj.responses)
                self.assertIs(type(obj.responses[identifier]), ResponseDataType)

    def test_intermediate_responses(self):
        from ._data_Command import data_observable, data_observable_intermediate, data_unobservable_intermediate

        with self.subTest(observeable="Yes", intermediate=False):
            obj = Command(xml_tree_element=objectify.fromstring(data_observable).Command)

            self.assertEqual(len(obj.intermediates), 0)

        with self.subTest(observeable="Yes", intermediate=True):
            obj = Command(xml_tree_element=objectify.fromstring(data_observable_intermediate).Command)

            self.assertEqual(len(obj.intermediates), 1)
            self.assertIn('IntermediateIdentifier', obj.intermediates)
            self.assertIs(type(obj.intermediates['IntermediateIdentifier']), IntermediateDataType)
            self.assertEqual(
                obj.intermediates['IntermediateIdentifier'].identifier,
                'IntermediateIdentifier'
            )
            self.assertEqual(
                obj.intermediates['IntermediateIdentifier'].description,
                'A random intermediate response definition.'
            )
            self.assertEqual(
                obj.intermediates['IntermediateIdentifier'].name,
                'Intermediate Identifier'
            )

        with self.subTest(observeable="No", intermediate=True):
            obj = Command(xml_tree_element=objectify.fromstring(data_unobservable_intermediate).Command)

            self.assertEqual(len(obj.intermediates), 0)

    def test_standard_execution_errors(self):
        from ._data_Command import data_standard_execution_errors

        obj = Command(xml_tree_element=objectify.fromstring(data_standard_execution_errors).Command)

        self.assertEqual(len(obj.standard_execution_errors), 3)
        for index in range(1, 3):
            identifier = "ExecutionError{index}".format(index=index)
            self.assertIn(identifier, obj.standard_execution_errors)

    def test_defined_execution_errors(self):
        from ._data_Command import data_defined_execution_errors

        obj = Command(xml_tree_element=objectify.fromstring(data_defined_execution_errors).Command)

        self.assertEqual(len(obj.defined_execution_errors), 3)
        for index in range(1, 3):
            identifier = "ExecutionError{index}".format(index=index)
            self.assertIn(identifier, obj.defined_execution_errors)


if __name__ == '__main__':
    unittest.main()
