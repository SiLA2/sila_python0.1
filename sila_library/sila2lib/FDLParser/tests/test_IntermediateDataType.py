
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..ParameterDataType import ParameterDataType
from ..IntermediateDataType import IntermediateDataType


class TestResponseDataType(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_intermediate_data_type_object(self):
        """
        Check whether we can identify a IntermediateDataType.
            A nIntermediateDataType object is effectively identical to a ParameterResponseType object. So we have only
            one way to distinguish them, which we test here and now.
        """
        from ._data_ParameterDataType import data_basic
        obj = IntermediateDataType(objectify.fromstring(data_basic).Parameter)

        self.assertIs(type(obj), IntermediateDataType)
        self.assertIsNot(type(obj), ParameterDataType)
