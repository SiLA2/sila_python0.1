"""This module defines xml-data that can be used to test the Command class"""

data_simple = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command without any complex elements</Description>
    <Observable>No</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
</Command></root>"""

data_empty_parameter = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command without any parameters</Description>
    <Observable>No</Observable>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
</Command></root>"""

data_empty_response = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command without any responses</Description>
    <Observable>No</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
</Command></root>"""

data_empty_both = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command without parameters and responses</Description>
    <Observable>No</Observable>
</Command></root>"""

data_observable = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, observable command without any complex elements</Description>
    <Observable>Yes</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
</Command></root>"""

#: Data to check on intermediate responses
data_observable_intermediate = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, observable command without any complex elements</Description>
    <Description>Simple, observable command without any complex elements</Description>
    <Observable>Yes</Observable>
    <IntermediateResponse>
        <Identifier>IntermediateIdentifier</Identifier>
        <DisplayName>Intermediate Identifier</DisplayName>
        <Description>A random intermediate response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </IntermediateResponse>
</Command></root>"""

#: A command containing an intermediate response while being not observable, this is not allowed
data_unobservable_intermediate = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, observable command without any complex elements</Description>
    <Observable>No</Observable>
    <IntermediateResponse>
        <Identifier>IntermediateIdentifier</Identifier>
        <DisplayName>Intermediate Identifier</DisplayName>
        <Description>A random intermediate response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </IntermediateResponse>
</Command></root>"""

#: Defines an input for a command that contains three parameter
data_multiple_parameter = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command with multiple parameters</Description>
    <Observable>No</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier1</Identifier>
        <DisplayName>1. Parameter Identifier</DisplayName>
        <Description>A random parameter definition (Parameter 1).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Parameter>
        <Identifier>ParameterIdentifier2</Identifier>
        <DisplayName>2. Parameter Identifier</DisplayName>
        <Description>A random parameter definition (Parameter 2).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Parameter>
        <Identifier>ParameterIdentifier3</Identifier>
        <DisplayName>3. Parameter Identifier</DisplayName>
        <Description>A random parameter definition (Parameter 3).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
</Command></root>"""

#: Defines an input for a command that contains three responses
data_multiple_responses = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command with multiple responses</Description>
    <Observable>Yes</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier1</Identifier>
        <DisplayName>1. Response Identifier</DisplayName>
        <Description>A random response definition (Response 1).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
    <Response>
        <Identifier>ResponseIdentifier2</Identifier>
        <DisplayName>2. Response Identifier</DisplayName>
        <Description>A random response definition (Response 2).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
    <Response>
        <Identifier>ResponseIdentifier3</Identifier>
        <DisplayName>3. Response Identifier</DisplayName>
        <Description>A random response definition (Response 3).</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
</Command></root>"""

data_standard_execution_errors = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command with standard errors defined</Description>
    <Observable>No</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
    <StandardExecutionErrors>
        <StandardExecutionErrorIdentifier>ExecutionError1</StandardExecutionErrorIdentifier>
        <StandardExecutionErrorIdentifier>ExecutionError2</StandardExecutionErrorIdentifier>
        <StandardExecutionErrorIdentifier>ExecutionError3</StandardExecutionErrorIdentifier>
    </StandardExecutionErrors>
</Command></root>"""

data_defined_execution_errors = """
<root><Command>
    <Identifier>CommandIdentifier</Identifier>
    <DisplayName>Command Name</DisplayName>
    <Description>Simple, unobservable command with defined execution errors defined</Description>
    <Observable>No</Observable>
    <Parameter>
        <Identifier>ParameterIdentifier</Identifier>
        <DisplayName>Parameter Identifier</DisplayName>
        <Description>A random parameter definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Parameter>
    <Response>
        <Identifier>ResponseIdentifier</Identifier>
        <DisplayName>Response Identifier</DisplayName>
        <Description>A random response definition.</Description>
        <DataType>
            <Basic>Boolean</Basic>
        </DataType>
    </Response>
    <DefinedExecutionErrors>
        <Identifier>ExecutionError1</Identifier>
        <Identifier>ExecutionError2</Identifier>
        <Identifier>ExecutionError3</Identifier>
    </DefinedExecutionErrors>
</Command></root>"""