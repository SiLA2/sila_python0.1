"""This module defines xml-data that can be used to test the Property class"""

data_simple = """
<root><Property>
    <Identifier>PropertyIdentifier</Identifier>
    <DisplayName>Property Name</DisplayName>
    <Description>Simple, unobservable property</Description>
    <Observable>No</Observable>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
</Property></root>"""

data_observable = """
<root><Property>
    <Identifier>PropertyIdentifier</Identifier>
    <DisplayName>Property Name</DisplayName>
    <Description>Simple, observable property</Description>
    <Observable>Yes</Observable>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
</Property></root>"""

data_standard_read_errors = """
<root><Property>
    <Identifier>PropertyIdentifier</Identifier>
    <DisplayName>Property Name</DisplayName>
    <Description>Simple, unobservable property with standard errors defined</Description>
    <Observable>No</Observable>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
    <StandardReadErrors>
        <StandardReadErrorIdentifier>ReadError1</StandardReadErrorIdentifier>
        <StandardReadErrorIdentifier>ReadError2</StandardReadErrorIdentifier>
        <StandardReadErrorIdentifier>ReadError3</StandardReadErrorIdentifier>
    </StandardReadErrors>
</Property></root>"""

data_defined_execution_errors = """
<root><Property>
    <Identifier>PropertyIdentifier</Identifier>
    <DisplayName>Property Name</DisplayName>
    <Description>Simple, unobservable property with defined execution errors defined</Description>
    <Observable>No</Observable>
    <DataType>
        <Basic>Boolean</Basic>
    </DataType>
    <DefinedExecutionErrors>
        <Identifier>ReadError1</Identifier>
        <Identifier>ReadError2</Identifier>
        <Identifier>ReadError3</Identifier>
    </DefinedExecutionErrors>
</Property></root>"""