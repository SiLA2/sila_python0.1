
data_standard_read_error = """
<root>
    <StandardReadError>
        <Identifier>ReadError1</Identifier>
        <DisplayName>Read Error 1</DisplayName>
        <Description>1. defined read error that can occur.</Description>
    </StandardReadError>
    <StandardReadError>
        <Identifier>ReadError2</Identifier>
        <DisplayName>Read Error 2</DisplayName>
        <Description>2. defined read error that can occur.</Description>
    </StandardReadError>
    <StandardReadError>
        <Identifier>ReadError3</Identifier>
        <DisplayName>Read Error 3</DisplayName>
        <Description>3. defined read error that can occur.</Description>
    </StandardReadError>
</root>
"""

data_standard_execution_error = """
<root>
    <StandardExecutionError>
        <Identifier>ExecutionError1</Identifier>
        <DisplayName>Execution Error 1</DisplayName>
        <Description>1. error that can occur when executing a command.</Description>
    </StandardExecutionError>
    <StandardExecutionError>
        <Identifier>ExecutionError2</Identifier>
        <DisplayName>Execution Error 2</DisplayName>
        <Description>2. error that can occur when executing a command.</Description>
    </StandardExecutionError>
    <StandardExecutionError>
        <Identifier>ExecutionError3</Identifier>
        <DisplayName>Execution Error 3</DisplayName>
        <Description>3. error that can occur when executing a command.</Description>
    </StandardExecutionError>
</root>
"""

data_defined_execution_error = """
<root>
    <DefinedExecutionError>
        <Identifier>ExecutionError1</Identifier>
        <DisplayName>Execution Error 1</DisplayName>
        <Description>1. error that can occur when executing a command or reading a property.</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>ExecutionError2</Identifier>
        <DisplayName>Execution Error 2</DisplayName>
        <Description>2. error that can occur when executing a command or reading a property.</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>ExecutionError3</Identifier>
        <DisplayName>Execution Error 3</DisplayName>
        <Description>3. error that can occur when executing a command or reading a property.</Description>
    </DefinedExecutionError>
</root>
"""
