
# import general Packages
import unittest
from lxml import objectify

# import package related modules and classes
from ..DataTypeDefinition import DataTypeDefinition
from ..BasicType import BasicType
from ..ListType import ListType
from ..StructureType import StructureType
from ..ConstrainedType import ConstrainedType
from ..DataTypeIdentifier import DataTypeIdentifier


class TestDataTypeDefinition(unittest.TestCase):

    def setUp(self):
        """
        Sets up basic attributes for the unit tests run in this class.

        *Nothing to set up.*
        """
        pass

    def test_basic(self):
        from ._data_DataTypeDefinition import data_basic

        obj = DataTypeDefinition(xml_tree_element=objectify.fromstring(data_basic).DataTypeDefinition)
        self.assertIs(type(obj.sub_type), BasicType)
        self.assertEqual(
            obj.description,
            "Definition of an identifier for a basic data type"
        )
        self.assertEqual(
            obj.name,
            "Basic Identifier"
        )
        self.assertEqual(
            obj.identifier,
            "BasicIdentifier"
        )

    def test_list(self):
        from ._data_DataTypeDefinition import data_list

        obj = DataTypeDefinition(xml_tree_element=objectify.fromstring(data_list).DataTypeDefinition)
        self.assertIs(type(obj.sub_type), ListType)
        self.assertEqual(
            obj.description,
            "Definition of an identifier for a list data type"
        )
        self.assertEqual(
            obj.name,
            "List Identifier"
        )
        self.assertEqual(
            obj.identifier,
            "ListIdentifier"
        )

    def test_structure(self):
        from ._data_DataTypeDefinition import data_structure

        obj = DataTypeDefinition(xml_tree_element=objectify.fromstring(data_structure).DataTypeDefinition)
        self.assertIs(type(obj.sub_type), StructureType)
        self.assertEqual(
            obj.description,
            "Definition of an identifier for a structure data type"
        )
        self.assertEqual(
            obj.name,
            "Structure Identifier"
        )
        self.assertEqual(
            obj.identifier,
            "StructureIdentifier"
        )

    def test_constrained(self):
        from ._data_DataTypeDefinition import data_constrained

        obj = DataTypeDefinition(xml_tree_element=objectify.fromstring(data_constrained).DataTypeDefinition)
        self.assertIs(type(obj.sub_type), ConstrainedType)
        self.assertEqual(
            obj.description,
            "Definition of an identifier for a constrained data type"
        )
        self.assertEqual(
            obj.name,
            "Constrained Identifier"
        )
        self.assertEqual(
            obj.identifier,
            "ConstrainedIdentifier"
        )

    def test_data_type_identifier(self):
        from ._data_DataTypeDefinition import data_data_type_identifier

        obj = DataTypeDefinition(xml_tree_element=objectify.fromstring(data_data_type_identifier).DataTypeDefinition)
        self.assertIs(type(obj.sub_type), DataTypeIdentifier)
        self.assertEqual(
            obj.description,
            "Definition of an identifier for a defined data type"
        )
        self.assertEqual(
            obj.name,
            "Definition Identifier"
        )
        self.assertEqual(
            obj.identifier,
            "DefinitionIdentifier"
        )
