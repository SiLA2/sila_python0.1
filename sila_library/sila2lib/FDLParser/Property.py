
# import meta packages
from typing import Any
from typing import List

# import library packages
from .ResponseDataType import ResponseDataType


class Property:
    """
    This class stores all information corresponding to **one** property defined in the feature definition.
    """
    #: Identifier of the property
    identifier: str

    #: Display name property of the property
    name: str

    #: Property description
    description: str

    #: Is this an observable property
    observable: bool

    #: Reference to the xml-tree of the property
    _tree: Any

    #: The response defined for this property
    response: ResponseDataType

    #: List of all standard (read) errors that can occur during the call to this property (SiLA v0.1)
    standard_read_errors: List[str]

    #: List of all defined execution errors (identifiers) that can occur during reading of this property (SiLA v0.2)
    defined_execution_errors: List[str]

    def __init__(self, xml_tree_element):
        """
        Class initialiser

        :param xml_tree_element: The contents of the <Property>-xml node
        """
        self._tree = xml_tree_element

        self.identifier = xml_tree_element.Identifier.text
        self.name = xml_tree_element.DisplayName
        self.description = xml_tree_element.Description

        self.observable = True if xml_tree_element.Observable.text == 'Yes' else False

        self.response = ResponseDataType(xml_tree_element=xml_tree_element)

        # get all standard errors
        #   SiLA v0.1: Standard Read Errors
        self.standard_read_errors = []
        if hasattr(xml_tree_element, 'StandardReadErrors'):
            for standard_error in xml_tree_element.StandardReadErrors.StandardReadErrorIdentifier:
                self.standard_read_errors.append(standard_error.text)
        # SilA v0.2: Defined Execution Errors
        self.defined_execution_errors = []
        if hasattr(xml_tree_element, 'DefinedExecutionErrors'):
            for defined_error in xml_tree_element.DefinedExecutionErrors.Identifier:
                self.defined_execution_errors.append(defined_error.text)


