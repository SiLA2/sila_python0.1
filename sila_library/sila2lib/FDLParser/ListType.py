
# import library packages
from .DataType import DataType
from . import BasicType as BType
from . import StructureType as SType
from . import ConstrainedType as CType
from . import DataTypeIdentifier as DTType

# import custom exceptions
from .Exceptions.Exceptions import SubDataTypeError


class ListType(DataType):
    """
    Class to store the SiLA derived data type list.
    """

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <List>-xml element that contains this list type.

        :raises TypeError: Unknown sub-type found.
        :raises: SubDataTypeError: An invalid sub-type has been used.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        # Set specific data
        self.is_list = True

        # Every list **must** have a DataType sub-element, so we create a sub-element from that. However, this must not
        #   be a List itself
        if hasattr(xml_tree_element.DataType, 'Basic'):
            self.sub_type = BType.BasicType(xml_tree_element=xml_tree_element.DataType.Basic)
        elif hasattr(xml_tree_element.DataType, 'List'):
            # Nested lists are not supported
            raise SubDataTypeError
        elif hasattr(xml_tree_element.DataType, 'Structure'):
            self.sub_type = SType.StructureType(xml_tree_element=xml_tree_element.DataType.Structure)
        elif hasattr(xml_tree_element.DataType, 'Constrained'):
            self.sub_type = CType.ConstrainedType(xml_tree_element=xml_tree_element.DataType.Constrained)
        elif hasattr(xml_tree_element.DataType, 'DataTypeIdentifier'):
            self.sub_type = DTType.DataTypeIdentifier(xml_tree_element=xml_tree_element.DataType.DataTypeIdentifier)
        else:
            # invalid type
            #   TODO: Extended error message
            raise TypeError
