
# import library packages
from .DataType import DataType
from . import BasicType as BType
from . import ListType as LType
from . import ConstrainedType as CType
from . import DataTypeIdentifier as DTType

from typing import List


class StructureType(DataType):
    """
    Class for the derived SiLA data type that implements a structure.
        A structure can contain many elements, which is why to access those multiple levels must be stepped through. The
        sub-type of this DataType is only a List that refers to all :class:`StructureElementType` Elements. These again
        contain a sub-type that represents the actual element.
    """

    #: The underlying type here is always a list of :class:`StructureElementType` objects.
    sub_type: List['StructureElementType']

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <Structure>-xml element that contains this structure type.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        # Set specific data
        self.is_structure = True

        # Structures can have an arbitrary number of elements, let's figure them out
        self.sub_type = []
        for element_index in range(0, len(xml_tree_element.Element)):
            self.sub_type.append(
                StructureElementType(xml_tree_element=xml_tree_element.Element[element_index])
            )


class StructureElementType(DataType):
    """
    Class that represents a single element of a structure.
    """

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <Structure><Element>-xml element that contains this elements info.

        :raises TypeError: Unknown sub-type found.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        self.is_sila_element = True

        # Every element **must** have a DataType sub-element, so we create a sub-element from that
        if hasattr(xml_tree_element.DataType, 'Basic'):
            self.sub_type = BType.BasicType(xml_tree_element=xml_tree_element.DataType.Basic)
        elif hasattr(xml_tree_element.DataType, 'List'):
            self.sub_type = LType.ListType(xml_tree_element=xml_tree_element.DataType.List)
        elif hasattr(xml_tree_element.DataType, 'Structure'):
            self.sub_type = StructureType(xml_tree_element=xml_tree_element.DataType.Structure)
        elif hasattr(xml_tree_element.DataType, 'Constrained'):
            self.sub_type = CType.ConstrainedType(xml_tree_element=xml_tree_element.DataType.Constrained)
        elif hasattr(xml_tree_element.DataType, 'DataTypeIdentifier'):
            self.sub_type = DTType.DataTypeIdentifier(xml_tree_element=xml_tree_element.DataType.DataTypeIdentifier)
        else:
            # invalid type
            raise TypeError

