
# import general packages used here
import warnings

# import meta packages
from typing import Any
from typing import List

# import custom exceptions
from .Exceptions.Exceptions import SubDataTypeError, BasicTypeWarning, ConstraintTypeWarning

# import library packages
from .DataType import DataType

from . import BasicType as BType
from . import ListType as LType


class ConstrainedType(DataType):
    """
    Class for the derived SiLA data type that implements constraints.
    """

    #: The length constraint
    constraint_length: int = None

    #: The minimal length constraint
    constraint_length_minimal: int = None

    #: The maximal allowed length
    constraint_length_maximal: int = None

    #: The element must be part of an enumeration
    constraint_enumeration: List[str] = None

    #: The element is constrained by a RegExp pattern
    constraint_pattern: str = None

    #: The constraint for a maximal (exclusive) value
    constraint_value_maximal_exclusive: str = None

    #: The constraint for a maximal (inclusive) value
    constraint_value_maximal_inclusive: str = None

    #: The constraint for a minimal (exclusive) value
    constraint_value_minimal_exclusive: str = None

    #: The constraint for a minimal (inclusive) value
    constraint_value_minimal_inclusive: str = None

    #: A unit constraint
    constraint_unit: Any = None

    #: Content type constraint
    constraint_content_type: Any = None

    #: Constraint for the minimal number of elements in a list
    constraint_elements_minimal: int = None

    #: Constraint for the maximal number of elements in a list
    constraint_elements_maximal: int = None

    #: Constraint to require a fully qualified identifier
    constraint_identifier: Any = None

    #: Constrain the content to follow a schema
    constraint_schema: Any = None

    def __init__(self, xml_tree_element):
        """
        Class initialiser.

        :param xml_tree_element: The content of this <List>-xml element that contains this list type.

        :raises SubDataTypeError: Invalid sub-type found when trying to create the underlying DataType object.
        :raises SubDataTypeWarning: A warning that a sub-type is invalid, however the program can somehow continue.
                                    Some special case handling might however fail.

        .. note:: For remaining parameters see :meth:`~.DataType.__init__`.
        """
        super().__init__(xml_tree_element=xml_tree_element)

        # Set specific data
        self.is_constrained = True

        # Every constrained **must** have a DataType sub-element, so we create a sub-element from that
        if hasattr(xml_tree_element.DataType, 'Basic'):
            self.sub_type = BType.BasicType(
                xml_tree_element=xml_tree_element.DataType.Basic
            )
        elif hasattr(xml_tree_element.DataType, 'List'):
            self.sub_type = LType.ListType(
                xml_tree_element=xml_tree_element.DataType.List
            )
        elif hasattr(xml_tree_element.DataType, 'DataTypeIdentifier'):
            # I'm not sure if this is possible? Anyway, we cannot check whether the sub-type can be used for constraints
            #   without having access to the DataType storage in the main FDLParser.
            raise NotImplementedError
        else:
            # invalid type
            raise SubDataTypeError

        # Let us try to find out the basic, underlying type. This expects the sub-type to be either a basic element, or
        #   a list of Basic elements. If this is not the case, we do not really know how to handle it.
        if isinstance(self.sub_type, BType.BasicType):
            # Assume: Basic / org.silastandard.<type>
            type_underlying = self.sub_type.sub_type
        elif isinstance(self.sub_type, LType.ListType):
            # Assume: List / Basic / org.silastandard.<type>
            type_underlying = self.sub_type.sub_type.sub_type
        else:
            warnings.warn('Invalid sub-type of the constrained type found. So far I am quite unaware of how to handle '
                          'the constraints.', BasicTypeWarning)
            type_underlying = None

        # if underlying type found is not a string we haven't found the basic type yet. However, if it is a list we can
        #   handle it for the element count constraints, so allow that. Everything else just warn, go on and hope for
        #   the best.
        if not isinstance(type_underlying, str) and not isinstance(self.sub_type, LType.ListType):
            warnings.warn('Could not determine basic, underlying data type. So far I am quite unaware of how to handle '
                          'the constraints.', BasicTypeWarning)
            type_underlying = None

        # we read the constraints after the sub-type has been read, so we can validate whether it is possible to apply
        #   the given constraints to the underlying data-type
        if hasattr(xml_tree_element.Constraints, 'Length'):
            # Applicable to: String
            self.constraint_length = xml_tree_element.Constraints.Length.text
            if type_underlying not in ["String"]:
                warnings.warn('Length constraint applied to a non-string type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MinimalLength'):
            # Applicable to: String, SmallBinary
            self.constraint_length_minimal = xml_tree_element.Constraints.MinimalLength.text
            if type_underlying not in ["String", "SmallBinary"]:
                warnings.warn('MinimalLength constraint applied to a non-string type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MaximalLength'):
            # Applicable to: String, SmallBinary
            self.constraint_length_maximal = xml_tree_element.Constraints.MaximalLength.text
            if type_underlying not in ["String", "SmallBinary"]:
                warnings.warn('MaximalLength constraint applied to a non-string type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'Enumeration'):
            self.constraint_enumeration = []
            for enum_value_index in range(0, len(xml_tree_element.Constraints.Enumeration.Value)-1):
                self.constraint_enumeration.append(xml_tree_element.Constraints.Enumeration.Value[enum_value_index])
            # Applicable to: String
            if type_underlying not in ["String"]:
                warnings.warn('Enumeration constraint applied to a non-string type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'Pattern'):
            self.constraint_pattern = xml_tree_element.Constraints.Pattern.text
            # Applicable to: String
            if type_underlying not in ["String"]:
                warnings.warn('Pattern constraint applied to a non-string type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MaximalExclusive'):
            self.constraint_value_maximal_exclusive = xml_tree_element.Constraints.MaximalExclusive.text
            # Applicable to: Integer, Real, Date, Time, Timestamp
            if type_underlying not in ["Integer", "Real", "Date", "Time", "Timestamp"]:
                warnings.warn('MaximalExclusive constraint applied to a non-numeric or time type.',
                              ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MaximalInclusive'):
            self.constraint_value_maximal_inclusive = xml_tree_element.Constraints.MaximalInclusive.text
            # Applicable to: Integer, Real, Date, Time, Timestamp
            if type_underlying not in ["Integer", "Real", "Date", "Time", "Timestamp"]:
                warnings.warn('MaximalInclusive constraint applied to a non-numeric or time type.',
                              ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MinimalExclusive'):
            self.constraint_value_minimal_exclusive = xml_tree_element.Constraints.MinimalExclusive.text
            # Applicable to: Integer, Real, Date, Time, Timestamp
            if type_underlying not in ["Integer", "Real", "Date", "Time", "Timestamp"]:
                warnings.warn('MinimalExclusive constraint applied to a non-numeric or time type.',
                              ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MinimalInclusive'):
            self.constraint_value_minimal_inclusive = xml_tree_element.Constraints.MinimalInclusive.text
            # Applicable to: Integer, Real, Date, Time, Timestamp
            if type_underlying not in ["Integer", "Real", "Date", "Time", "Timestamp"]:
                warnings.warn('MinimalInclusive constraint applied to a non-numeric or time type.',
                              ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'Unit'):
            # Applicable to: Integer, Real
            #   TODO: Implement
            warnings.warn('Unit constraint not yet implemented', Warning)
        if hasattr(xml_tree_element.Constraints, 'ContentType'):
            # Applicable to: String, SmallBinary
            #   TODO: Implement
            warnings.warn('ContentType constraint not yet implemented', Warning)
        if hasattr(xml_tree_element.Constraints, 'MinimalElementCount'):
            self.constraint_elements_minimal = xml_tree_element.Constraints.MinimalElementCount.text
            # Applicable to: List
            if not isinstance(self.sub_type, LType.ListType):
                warnings.warn('MinimalElementCount constraint applied to a non-List type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'MaximalElementCount'):
            self.constraint_elements_maximal = xml_tree_element.Constraints.MaximalElementCount.text
            # Applicable to: List
            if not isinstance(self.sub_type, LType.ListType):
                warnings.warn('MaximalElementCount constraint applied to a non-List type.', ConstraintTypeWarning)
        if hasattr(xml_tree_element.Constraints, 'FullyQualifiedIdentifier'):
            # Applicable to: String
            #   TODO: Implement
            warnings.warn('FullyQualifiedIdentifier constraint not yet implemented', Warning)
        if hasattr(xml_tree_element.Constraints, 'Schema'):
            # Applicable to: String
            #   TODO: Implement
            warnings.warn('Schema constraint not yet implemented', Warning)

    def __getitem__(self, key: str) -> Any:
        """
        Make the constraints available using a key (dictionary-like) call.

        :param key: The constraint property to read.

        :raises TypeError: Given `item` is not a valid string identifier.
        :raises KeyError: The given key is unknown.
        """

        # check the input type
        if not isinstance(key, str):
            raise TypeError

        if key == 'Length':
            return self.constraint_length
        elif key == 'MinimalLength':
            return self.constraint_length_minimal
        elif key == 'MaximalLength':
            return self.constraint_length_maximal
        elif key == 'Enumeration':
            return self.constraint_enumeration
        elif key == 'Pattern':
            return self.constraint_pattern
        elif key == 'MaximalExclusive':
            return self.constraint_value_maximal_exclusive
        elif key == 'MaximalInclusive':
            return self.constraint_value_maximal_inclusive
        elif key == 'MinimalExclusive':
            return self.constraint_value_minimal_exclusive
        elif key == 'MinimalInclusive':
            return self.constraint_value_minimal_inclusive
        elif key == 'MinimalElementCount':
            return self.constraint_elements_minimal
        elif key == 'MaximalElementCount':
            return self.constraint_elements_maximal
        elif key in ['Unit', 'ContentType', 'FullyQualifiedIdentifier', 'Schema']:
            # TODO: Implement
            raise NotImplementedError
        else:
            raise KeyError

    def __setitem__(self, key: str, value: Any):
        """
        Make the constraints available using a key (dictionary-like) assignment operation.

        :param key: The constraint property to read.
        :param value: The value to set. This **will** automatically be converted to the required data type if necessary
                      (e.g. int or str). If this is not supported by the value given, an error might occur!

        :raises TypeError: Given `item` is not a valid string identifier.
        :raises KeyError: The given key is unknown.
        """

        # check the input type
        if not isinstance(key, str):
            raise TypeError

        # store the input and explicitly convert it to the required datatype.
        if key == 'Length':
            self.constraint_length = int(value)
        elif key == 'MinimalLength':
            self.constraint_length_minimal = int(value)
        elif key == 'MaximalLength':
            self.constraint_length_maximal = int(value)
        elif key == 'Enumeration':
            self.constraint_enumeration = [str(item) for item in value]
        elif key == 'Pattern':
            self.constraint_pattern = str(value)
        elif key == 'MaximalExclusive':
            self.constraint_value_maximal_exclusive = str(value)
        elif key == 'MaximalInclusive':
            self.constraint_value_maximal_inclusive = str(value)
        elif key == 'MinimalExclusive':
            self.constraint_value_minimal_exclusive = str(value)
        elif key == 'MinimalInclusive':
            self.constraint_value_minimal_inclusive = str(value)
        elif key == 'MinimalElementCount':
            self.constraint_elements_minimal = int(value)
        elif key == 'MaximalElementCount':
            self.constraint_elements_maximal = int(value)
        elif key in ['Unit', 'ContentType', 'FullyQualifiedIdentifier', 'Schema']:
            # TODO: Implement
            raise NotImplementedError
        else:
            raise KeyError
