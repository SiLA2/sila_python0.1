"""
________________________________________________________________________

:PROJECT: SiLA2_python

*SiLA2 FDL/XML parser*

:details: SiLA2 Feature Definition Language (FDL/XML) parser - lxml.objectify based.
          - building a tree of dictionaries from an XML/FDL file
          - pickling this dictionary tree

:file:    fdl_parser.py
:authors: mark doerr (mark@uni-greifswald.de)
          Florian Meinecke
          Timm Severin

:date: (creation)          20180523
:date: (last modification) 20190816

.. note:: - 
.. todo:: - check, if namespaces are handled correctly
            preserve parameter and response order !!!
          - error parsing !! - might be moved to server client gen.
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.2.2"

# Import general packages
import os
import sys
import logging
import pickle

# Import XML packages
from lxml import etree
from lxml import objectify


class FDLSiLAReader:
    """Base class to read SiLA FDL/XML files"""

    def __init__(self, fdl_schema_filename=None):
        """
        Class initialiser

            :param fdl_schema_filename: The filename to the schema file which is used when parsing or validating an FDL
                                        file. If this is None, the default schema file from this library is loaded.
        """

        if fdl_schema_filename is None:
            self.fdl_schema_filename = os.path.join(os.path.dirname(__file__), "schema", "FeatureDefinition.xsd")
            logging.debug('Set schema file for FDL/XML parsing to {schema_filename}'.format(
                schema_filename=self.fdl_schema_filename)
            )


class FDLValidator(FDLSiLAReader):
    """Validator a FDL/XML input file"""

    def __init__(self, fdl_schema_file=""):
        """
        Class initialiser

            :param fdl_schema_file: Path to the FDL schema file
        """
        super().__init__(fdl_schema_filename=fdl_schema_file)

    def validate(self, input_file):
        """
        Validating Feature Definition FDL / XML according to SiLA2 schema

            :param input_file: File to validate with the given schema

            :returns: Whether the input file is valid based on the FDL schema file
        """
        logging.info("Validating FDL file {fdl_filename}".format(fdl_filename=input_file))
        try:
            schema = etree.XMLSchema(file=self.fdl_schema_filename)
            parser = objectify.makeparser(schema=schema)
            _ = objectify.parse(input_file, parser)
            is_valid = True

        except Exception as err:
            sys.stderr.write("ERROR: XML/FDL validation:\n{}\n".format(err))
            is_valid = False

        return is_valid


class FDLParser(FDLSiLAReader):
    """ This class builds a tree of dictionaries collecting all information from the XML/FDL file"""

    def __init__(self, fdl_filename, fdl_schemaname=None):
        """ FDLParser Class initialiser

            :param fdl_filename: filename of FDL file
            :param fdl_schemaname: filename of the FDL scheme. If None, it will be set the the FDL scheme stored in this
                                   package
        """
        super().__init__(fdl_schema_filename=fdl_schemaname)
        
        self.sila2_namespace = {"sila2": "http://www.sila-standard.org"}  # this is used for schema checking
        
        self.fdl_filename = fdl_filename

        # TODO: Use OrderedDict to keep the order of the elements
        self.cmd_parameter_dic = dict()
        self.resp_parameter_dic = dict()
        self.prop_resp_parameter_dic = dict()

        # prepare class variables
        self.tree = None
        # self.root = None
        self.sila_feature = None
        self.data_type_dic = None
        
        self.generateTreeModel()
        
        feature_major_ver = self.sila_feature.get("FeatureVersion").split('.')[0]
        self.proto_base_namespace = ".".join(("sila2", self.sila_feature.get("Originator")))
        self.proto_namespace = ".".join((self.proto_base_namespace, self.sila_feature.get("Category"),
                                         str(self.sila_feature.Identifier).lower(), "v" + feature_major_ver))
        self.sila_framework_namespace = ".".join(("sila2", self.sila_feature.get("Originator")))
        
        self.addDataTypes()
        self.parseCommandParameters()
        self.parsePropertyResponses()
        
    def generateTreeModel(self):
        """Generating FDL/XML object tree model using lxml.objectify"""
        try:
            # validate and parse XML / FDL, converting them into python objects with objectify
            logging.debug("Schema name for XML validation: {}".format(self.fdl_schema_filename))

            # create a parser and generate python object(s) from XML structure
            schema = etree.XMLSchema(file=self.fdl_schema_filename)
            parser = objectify.makeparser(schema=schema)
            self.tree = objectify.parse(self.fdl_filename, parser)
            
            
            self.sila_feature = self.tree.find(".", namespaces=self.sila2_namespace)
        except Exception as err:
            logging.exception("")
            sys.stderr.write("ERROR (treeModelGeneration): XML/FDL validation/parsing:\n{}\n".format(err))
            exit()
            
    def addDataTypes(self):
        """
        Generates a dictionary for all data types defined in the SiLA Standard and additional, complex data types
        defined in the FDL input file.

            Default data types should be covered by SilaFramework.proto.

            The method creates a dictionary `self.data_type_dic` with entries of the following structure:
            "<key>": {
                "basic_sila_type":          (String) The name of the SiLA type
                "basic_proto_type":         (String) The corresponding name of the data type in .proto files
                "basic_type":               (Boolean) Whether this is a basic type
                "constrained":              (Boolean) Are there any constraints on this data type
                "constraints":              (Dictionary) Dictionary of the constraints (see below)
                "is_list":                  (Boolean) Whether this data type is a list
                "proto_list_type":          TODO: Figure out what the this is
                "proto_namespace":          TODO: Figure out what this namespace defines
                "datatype_prefix":          TODO: ...
                "description":              (String): Description of this data type
                "def_value":                (Mixed) Default value of this data type
            }

            The constraints dictionary in the data_type_dic has the following structure.
            constraints: {
                "pattern":                  (String) Regular expression pattern that is allowed for a string
                "maxLen":                   (Integer) Maximum number of characters/bytes allowed
            }
            The description is only a short summary, for details please refer to the document
                "SiLA 2 Part (A) - Overview, Concepts and Core Specification",
                Chapter SiLA Data Types > SiLA Derived Types > SiLA Constrained Type
                (as of 2019-06-20)
        """

        # First add all basic data types as defined in the specifications
        #  TODO: Move from Binary -> SmallBinary,
        #        Add LargeBinary, Date, Time, Timestamp

        # define the default dictionary for all basic types.
        _basic_type_dict = {
            "basic_type": True,
            "constrained": False, "constraints": {},
            "is_list": False, "proto_list_type": "",
            "proto_namespace": self.sila_framework_namespace,
            "datatype_prefix": ".",
            # entries that must be overwritten by the following definitions
            "basic_sila_type": None, "basic_proto_type": None,
            "description": "",
            "def_val": None
        }

        # Define all data types. Therefore we are going to merge the above default dictionary with the details for each
        #   data type.
        #   *Note:* The merge used in this implementation strictly requires python >= 3.5. If this is not desired,
        #   an additional function for merging dictionaries could be defined here
        #   *Note:* This is a **shallow** merge, any sub-dictionaries will be copied as reference, not by value.
        self.data_type_dic = dict(
            Boolean={**_basic_type_dict, **{
                "basic_sila_type": "Boolean", "basic_proto_type": "bool",
                "description": "basic logic/boolean type",
                "def_val": False
            }},
            Integer={**_basic_type_dict, **{
                "basic_sila_type": "Integer", "basic_proto_type": "int64",
                "description": "basic integer type (64bit width)",
                "def_val": 0
            }},
            Real={**_basic_type_dict, **{
                "basic_sila_type": "Real", "basic_proto_type": "double",
                "description": "basic float/double type",
                "def_val": 0.0
            }},
            String={**_basic_type_dict, **{
                "basic_sila_type": "String", "basic_proto_type": "string",
                "description": "basic string type",
                "def_val": "'DEFAULTString'"
            }},
            Binary={**_basic_type_dict, **{
                "basic_sila_type": "Binary", "basic_proto_type": "bytes",
                "description": "basic byte type",
                "def_val": b"DEFAULTBinary"
            }},
            Void={**_basic_type_dict, **{
                "basic_sila_type": "Void", "basic_proto_type": "empty",
                "description": "void / empty type",
                "def_val": b""
            }}
        )

        # Are there any additional, complex data types defined? If yes, handle those
        if not hasattr(self.sila_feature, 'DataTypeDefinition'):
            return

        # loop over all defined data types
        for data_type in self.sila_feature.DataTypeDefinition:
            # set the default dictionary for all complex types. Individual entries can/should be overwritten
            _complex_type_dict = {
                "basic_type": False,
                "constrained": False, "constraints":
                    {
                        'pattern': "",
                        'maxLen': 0
                    },
                "is_list": False, "proto_list_type": "",
                "proto_namespace": self.proto_namespace,
                "datatype_prefix": ".DataType_",
                # entries that **must** be overwritten by the following definitions
                "basic_sila_type": None, "basic_proto_type": None,
                "description": "",
                "def_val": None
            }

            # TODO: Add derived type Structure type
            if hasattr(data_type.DataType, 'Constrained'):
                logging.warning('Constrained data types not supported yet!')
                _complex_type_dict['constrained'] = True
                _complex_type_dict['basic_sila_type'] = data_type.DataType.Constrained.DataType.Basic

                # these exist, but there is no special handling (yet?)
                if hasattr(data_type.DataType.Constrained, 'DataTypeIdentifier'):
                    pass
                elif hasattr(data_type.DataType.Constrained.DataType, 'Basic'):
                    pass

                # load the defined constraints and store them
                if hasattr(data_type.DataType.Constrained, 'Constraint'):
                    if hasattr(data_type.DataType.Constrained.Constraint, 'Pattern'):
                        _complex_type_dict['constraints']['pattern'] = \
                            data_type.DataType.Constrained.Constraints.Pattern
                    if hasattr(data_type.DataType.Constrained.Constraint, 'MaximalLength'):
                        _complex_type_dict['constraints']['maxLen'] = \
                            data_type.DataType.Constrained.Constraint.MaximalLength
                    # TODO: Add constraints:
                    #   Length, MinimalLength, Enumeration, MaximalExclusive, MaximalInclusive, MinimalExclusive,
                    #   MinimalInclusive, Unit, FullyQualifiedIdentifier
                    #   And for SiLA 0.2: ContentType, Schema
            elif hasattr(data_type.DataType, 'List'):
                _complex_type_dict['is_list'] = True
                _complex_type_dict['proto_list_type'] = "repeated"

                if hasattr(data_type.DataType.List.DataType, 'DataTypeIdentifier'):
                    _complex_type_dict['basic_sila_type'] = \
                        data_type.DataType.List.DataType.DataTypeIdentifier
                elif hasattr(data_type.DataType.List.DataType, 'Basic'):
                    _complex_type_dict['basic_sila_type'] = \
                        data_type.DataType.List.DataType.Basic
                    _complex_type_dict['proto_namespace'] = self.sila_framework_namespace
                    _complex_type_dict['datatype_prefix'] = "."
                else:
                    # TODO: Can this case occur? Handle or raise an error
                    raise NotImplementedError
            else:  # no constraints
                _complex_type_dict['basic_sila_type'] = data_type.DataType.Basic
                
                if hasattr(data_type.DataType, 'DataTypeIdentifier'):
                    _complex_type_dict['proto_namespace'] = self.proto_namespace
                elif hasattr(data_type.DataType, 'Basic'):
                    _complex_type_dict['proto_namespace'] = self.sila_framework_namespace
            
            _complex_type_dict['basic_proto_type'] = \
                self.data_type_dic[_complex_type_dict['basic_sila_type']]['basic_proto_type']

            _complex_type_dict['description'] = str(data_type.Description).replace('\n', '\n//')

            # assign the dictionary.
            #   Note that this copies a reference, which is okay, since we create a new dictionary object each iteration
            #   of the for loop. Otherwise we will have to resolve to copy.deepcopy()
            self.data_type_dic[data_type.Identifier] = _complex_type_dict
    
    def parseCommandParameters(self):
        for cmd in self.sila_feature.Command:
            try:
                self.parseParameter(cmd, cmd.Parameter)
            except AttributeError as err:
                logging.debug((
                    "cmd [{command}]: No Command Parameter provided: " +
                    "{error} - using empty parameter"
                ).format(
                    command=cmd.Identifier,
                    error=err)
                )
                self.handleEmptyCommandParameter(cmd)
            try:
                self.parseParameter(cmd, cmd.Response)
            except AttributeError as err:
                logging.debug((
                    "cmd-resp [{command}]: No Command Response provided: " +
                    "{error} - using empty response "
                ).format(command=cmd.Identifier, error=err))
                self.handleEmptyCommandResponseParameter(cmd)
            
    def parsePropertyResponses(self):
        try:
            for prop in self.sila_feature.Property:
                self.parseParameter(prop, prop)
        except AttributeError as err:
            logging.debug("{}".format(err))
            
    def parseParameter(self, param_class, param):
        """generic method for parsing SiLA parameters - for description generation
          :param  param_class : tree item to be parsed (either command or property)
          :param  param: actual parameters """
          
        command_tag = "{{{0}}}Command".format(self.sila2_namespace['sila2'])
        parameter_tag = "{{{0}}}Parameter".format( self.sila2_namespace['sila2'] )
        response_tag = "{{{0}}}Response".format( self.sila2_namespace['sila2'] )
        intermediate_response_tag = "{{{0}}}IntermediateResponse".format( self.sila2_namespace['sila2'] )
        property_tag = "{{{0}}}Property".format( self.sila2_namespace['sila2'] )
        
        if param_class.tag == command_tag :
            # Working with a parameter of a command
            object_name = "request"
        else:
            # Working with a parameter of a property
            object_name = "response"
        
        # this is used in source descriptions
        param_description = " " * 4 + ":param request: gRPC request\n" \
                            + " " * 12 + ":param context: gRPC context\n"
        param_description += " " * 12 + ":param {object_name}.{id}: {descr}\n".format(id=param.Identifier,
                                                                                      object_name=object_name,
                                                                                      descr=param.Description)
        dt_dic = self.parseDataType(param)

        # storing the results in according dictionary only
        # !! TODO: testing of the new 'class not in parameter_dic' 
        if param.tag == parameter_tag   :
            cmd_parameter_dic = dict(id=param.Identifier, description=param_description,
                                           sila_type=dt_dic['sila_type'], datatype_dic=dt_dic)
            if param_class not in self.cmd_parameter_dic:
                self.cmd_parameter_dic[param_class] = [cmd_parameter_dic]
            else:
                self.cmd_parameter_dic[param_class].append(cmd_parameter_dic)
        elif  param.tag == response_tag:
            resp_parameter_dic = dict(id=param.Identifier, description=param_description,
                                           sila_type=dt_dic['sila_type'], datatype_dic=dt_dic)
            if param_class not in self.resp_parameter_dic:
                self.resp_parameter_dic[param_class] = [resp_parameter_dic]
            else:
                self.resp_parameter_dic[param_class].append(resp_parameter_dic)
        elif  param.tag == intermediate_response_tag :
            intermed_resp_parameter_dic = dict(id=param.Identifier, description=param_description,
                                           sila_type=dt_dic['sila_type'], datatype_dic=dt_dic)
            if param_class not in self.intermed_resp_parameter_dic:
                self.intermed_resp_parameter_dic[param_class] = [intermed_resp_parameter_dic]
            else:
                self.intermed_resp_parameter_dic[param_class].append(intermed_resp_parameter_dic)
        elif  param.tag == property_tag:
            prop_resp_parameter_dic = dict(id=param.Identifier, description=param_description,
                                           sila_type=dt_dic['sila_type'], datatype_dic=dt_dic)
            if param_class not in self.prop_resp_parameter_dic:
                self.prop_resp_parameter_dic[param_class] = [prop_resp_parameter_dic]
            else:
                self.prop_resp_parameter_dic[param_class].append(prop_resp_parameter_dic)
        # this is used in source descriptions of constraints            
        param_description += (" " * 12 + "{id} - constraints: {constraint}\n"
                              + " " * 12 + "{id} - max length: {max_len}").format(id=param.Identifier,
                                                                                  constraint=dt_dic['pattern'],
                                                                                  max_len=dt_dic['max_len'])
                                           
    def parseDataType(self, item):
        """ helper method for parsing the datatype of a command parameter or response of a property 
         :param item: command parameter, response or property"""
        pattern = ""
        max_len = -1
        is_basic_sila_type = False
        item_constrained = False
        is_list = False
        proto_list_type = ""
        item_sila_type = "String"
        
        proto_namespace = self.proto_namespace  # default namespace
        datatype_prefix = ".DataType_"
        
        if hasattr(item.DataType, 'Constrained'):
            item_constrained = True
            if hasattr(item.DataType.Constrained, 'DataTypeIdentifier'):
                # could be Constrained.DataType.DataTypeIdentifier ??
                item_sila_type = item.DataType.Constrained.DataTypeIdentifier
            elif hasattr(item.DataType.Constrained.DataType, 'Basic'):
                item_sila_type = item.DataType.Constrained.DataType.Basic
                is_basic_sila_type = True
                proto_namespace = self.proto_base_namespace
                datatype_prefix = "."
            if hasattr(item.DataType.Constrained.Constraints, 'Pattern'):
                pattern = item.DataType.Constrained.Constraints.Pattern
            else: 
                pattern = ""
            if hasattr(item.DataType.Constrained.Constraints, 'MaximalLength'):
                max_len = item.DataType.Constrained.Constraints.MaximalLength
            else: 
                max_len = 0
        else:  # no constraints
            if hasattr(item.DataType, 'DataTypeIdentifier'):
                item_sila_type = item.DataType.DataTypeIdentifier
            elif hasattr(item.DataType, 'Basic'):
                item_sila_type = item.DataType.Basic
                is_basic_sila_type = True
                proto_namespace = self.proto_base_namespace
                datatype_prefix = "."
        # List processing        
        if hasattr(item.DataType, 'List'):
            is_list = True
            proto_list_type = "repeated"
            if hasattr(item.DataType.List.DataType, 'DataTypeIdentifier'):
                item_sila_type = item.DataType.List.DataType.DataTypeIdentifier
            elif hasattr(item.DataType.List.DataType, 'Basic'):
                item_sila_type = item.DataType.List.DataType.Basic
                proto_namespace = self.proto_base_namespace
                datatype_prefix = "."
                     
        return dict(sila_type=item_sila_type, is_basic_sila_type=is_basic_sila_type, 
                    proto_namespace=proto_namespace,
                    datatype_prefix=datatype_prefix,
                    constrained=item_constrained, pattern=pattern, max_len=max_len,
                    is_list=is_list, proto_list_type=proto_list_type)

    def handleEmptyCommandParameter(self, cmd=None):
        """ :param cmd: TODO"""
        
        dt_dic = dict(sila_type='Void', is_basic_sila_type=True, proto_namespace="",
                      datatype_prefix=".",
                      constrained=False, pattern="", max_len=0,
                      is_list=False, proto_list_type=False)
                    
        self.cmd_parameter_dic[cmd] = dict(id='Void', description="empty parameter", 
                                           sila_type='Void', datatype_dic=dt_dic)
    
    def handleEmptyCommandResponseParameter(self, cmd=None):
        """ :param cmd: TODO"""
        
        dt_dic = dict(sila_type='Void', is_basic_sila_type=True, proto_namespace="",
                      datatype_prefix=".",
                      constrained=False, pattern="", max_len=0,
                      is_list=False, proto_list_type=False)
                    
        self.resp_parameter_dic[cmd] = dict(id='Void', description="empty parameter",
                                            sila_type='Void', datatype_dic=dt_dic)
        
    def pickleAllDicts(self, output_dir="."):
        """
        Pickling data types and FDL tree

           :param output_dir: pickle target dir
        """

        pickle_filename = os.path.join(output_dir, self.sila_feature.Identifier + ".pickle")
        
        logging.debug("Writing pickle file to: {pickle_filename}".format(pickle_filename=pickle_filename))
        
        # put all dictionaries into one list !
        # TODO: Add standard errors (self.std_err_dic,)
        list_to_pickle = [self.data_type_dic, self.tree]
        
        with open(pickle_filename, 'wb') as file:
            pickle.dump(list_to_pickle, file)

    @staticmethod
    def version():
        """
        Return the version of the FDLParser

            :returns: The version string of this file
        """
        
        return __version__
        
