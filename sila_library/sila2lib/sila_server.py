"""
________________________________________________________________________

:PROJECT: SiLA2_python

* sila_server *

:details: SiLA2 server. 
           
:file:    sila_server.py
:authors: mark doerr

:date: (creation)          20181128
:date: (last modification) 20190813

.. note:: 
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.6"

import os
import sys

import time
import logging
from configparser import ConfigParser, NoSectionError, NoOptionError
import uuid
import pickle
from lxml import etree

import grpc
from grpc import server as GRPCServer
from concurrent.futures import ThreadPoolExecutor

import sila2lib.std_features.SiLAService as siserv
import sila2lib.std_features.SiLAService_pb2_grpc as sspbgrpc

import sila2lib.std_features.SimulationController as simctrl
import sila2lib.std_features.SimulationController_pb2_grpc as simctrlgrpc

from sila2lib.sila_service_detection import SiLA2ServiceDetection

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class SiLA2Server:
    """Base class of a SiLA2 Server"""

    def __init__(self, name="SiLA2TestServer",
                 description="Description: This is a test server",
                 server_type="SiLA2TestServer",
                 UUID=None,
                 version="0.0",
                 vendor_URL="lara.uni-greifswald.de",
                 ip='127.0.0.1', port=50051,
                 key='sila_server.key', cert='sila_server.crt'):
        """
        Intialising and starting the gRPC server, which is the core of the SiLA server

        :param name="SiLA2TestServer": name of the Server
        :param description="A short, human readable description": description of the Server
        :param vendor_URL="testdomain.tld": vendor URL of the Server
        :param port=55001:  port of Server
        :param key="sila_server.key": filename of private server key
        :param cert="sila_server.crt": filename of server certificate
        """ 
        
        self.simulation_mode = True
        
        self.server_name = name
        self.description = description
        self.server_type = server_type
        self.server_UUID = UUID
        self.server_version = version 
        self.vendor_URL = vendor_URL
        
        self.server_ip = ip
        self.server_port = port
        self.server_key = key
        self.server_cert = cert
        
        self.simulation_mode = True
        
        self.registered_features = {}
    
        self.encryption = False
        
        self.readConfigFile()
        
        # reading keys/certs for encryption
        try:
            with open(self.server_key, 'rb') as f:
                self.private_key = f.read()
            with open(self.server_cert, 'rb') as f:
                self.certificate_chain = f.read()
                
            self.encryption = True
        except FileNotFoundError as err:
            logging.error("Encryption key file {} not found, please generate keys!".format(err))
        
        # creating the gRPC server object from the gRPC framework
        self.grpc_server = GRPCServer(ThreadPoolExecutor(max_workers=10))
        
        # adding encryption ??
        if self.encryption:
            server_credentials = grpc.ssl_server_credentials(
                (
                    (self.private_key, self.certificate_chain,),
                )
            )
            
            # ':'.join((dns_server_name, port)
            self.grpc_server.add_secure_port('[::]:{}'.format(self.server_port), server_credentials)
        else:
            self.grpc_server.add_insecure_port('[::]:{}'.format(self.server_port))
        
        # adding SiLA2 Service
        self.SiLAService_feature = siserv.SiLAService(server_name=self.server_name,
                                                      server_description=self.description,
                                                      server_version=self.server_version,
                                                      server_UUID=self.server_UUID,
                                                      vendor_URL=self.vendor_URL)
        sspbgrpc.add_SiLAServiceServicer_to_server(self.SiLAService_feature, self.grpc_server)
        
        # adding Simulation Controller
        self.SimulationController_feature = simctrl.SimulationController(SiLA2Server=self)
        simctrlgrpc.add_SimulationControllerServicer_to_server(self.SimulationController_feature, self.grpc_server)
        self.addFeature("SimulationController", os.path.dirname(simctrl.__file__))

        #~ self.serve()
        
    def run(self, block=True):
        """ main gRPC server routine
            :param block: If set to True, this function will block the execution
            of the main thread until CTRL-C is pressed to shutdown the gRPC server.
            Otherwise this function won't block the main thread and return immediately.
            Note that you are then responsible to shutdown the gRPC server yourself
            by calling `~stop_grpc_server()`.
        """
        self.grpc_server.start()

        # enabling zeroconf/Bonjour Service detection
        ssd = SiLA2ServiceDetection()
        ssd.registerService(service_name=self.server_name,
                            IP=self.server_ip,
                            port=self.server_port,
                            server_uuid=self.server_UUID)

        print("{} server started, please press ctrl-c to stop...".format(self.server_name) )

        if block:
            try:
                while True:
                    time.sleep(_ONE_DAY_IN_SECONDS)
            except KeyboardInterrupt:
                #~ ssd.close()
                self.grpc_server.stop(0)

    def stop_grpc_server(self):
        """
        Immediately stops the grpc server
        """
        logging.debug("stopping {} server...".format(self.server_name) )
        self.grpc_server.stop(0)

    def stop(self):
        """Stop SiLA Server routine"""

        logging.info("remember terminating all observable commands / dynamic properties {}".format("stopping now..."))
        # self.grpc_server.stop(0)

    def addFeature(self, feature_id, pickle_path=None):
        """
        Add a feature to this server

        :param feature_id:
        :param pickle_path:
        """
        
        if pickle_path is not None:
            # loading pickle
            fdl_pickle_filename = os.path.join(pickle_path, feature_id + ".pickle")
            logging.debug("loading pickle:{}".format(fdl_pickle_filename))
            
            with open(fdl_pickle_filename, 'rb') as file:
                fdl_list = pickle.load(file)
                # extracting FDL/XML file from pickled list
                self.SiLAService_feature.registerFeature(feature_id=feature_id, fdl=etree.tostring(fdl_list[1]))
                
                self.registered_features[feature_id] = dict(data_types=fdl_list[0], fdl_tree=fdl_list[1])
        else:
            self.SiLAService_feature.registerFeature(feature_id=feature_id,
                                                     fdl="<xml>...feature tree missing...</xml>")

    def switchToSimMode(self):
        """ switching to simulation mode
            please overwrite this method in your server !
            # stopping gRPC server
               self.stop()
            # re-assign services
            # restart gRPC server
              self.run()
        """
        self.simulation_mode = True
        logging.debug('Switching to simulation mode.')

        # must be implemented by derived class
        raise NotImplementedError

    def switchToRealMode(self):
        """please overwrite this method in your server !
            # stopping gRPC server
               self.stop()
            # re-assign services
            # restart gRPC server
              self.run() 
        """
        self.simulation_mode = False
        logging.debug('Switching to real mode.')

        # must be implemented by derived class
        raise NotImplementedError

    def toggleSimMode(self):
        """Toggle between simulation and real mode"""

        logging.debug('Toggle of simulation/real mode requested:')
        if self.simulation_mode:
            return self.switchToRealMode()
        else:
            return self.switchToSimMode()

    def simulationModeStatus(self):
        """Returns the simulation state of the server"""

        return self.simulation_mode
        
    def encryptionModeStatus(self):
        """Returns the encryption status of the connection"""

        return self.encryption

    def renameServer(self, new_server_name):
        """
        Change the name of the server

        :param new_server_name: New server name
        """

        # shutdown, rename, restart
        
        logging.debug("Renaming SiLA server from [{old_name}] to [{new_name}]".format(
            old_name=self.server_name,
            new_name=new_server_name
        ))
        
        self.server_name = new_server_name
        
    def readConfigFile(self):
        """Read a configuration file for the server"""
        
        # reading config file
        config_dir = os.path.join(
            os.environ.get('APPDATA') or os.path.join(
                os.environ['HOME'],
                '.config',
                'sila2'
            ),
            self.server_name
        )

        config_filename = os.path.join(config_dir, self.server_name + '.conf') 
        logging.debug("Reading config from: {config_file}".format(config_file=config_filename))
        
        if not os.path.exists(config_dir):
            os.makedirs(config_dir, exist_ok=True)
            logging.info("config dir [{}] created".format(config_dir))

        # Create the config parser to read the current config
        try: 
            sila2_config = ConfigParser()
            sila2_config.read(config_filename)
        except Exception as err:
            logging.exception(
                (
                    'Error while creating config parser: {err}' '\n'
                    'Cannot permanently store UUID, generating a temporary one'
                ).format(err=err))
            self.server_UUID = uuid.uuid4()
            return

        # read the server UUID from the config file
        try:
            self.server_UUID = "{}".format(sila2_config['server']['UUID'])
            uuid_missing = False
        except NoSectionError as err:
            sys.stderr.write("ERROR: Please add section in your SiLA2 config file {}".format(err))
            uuid_missing = True
        except NoOptionError as err:
            sys.stderr.write("ERROR: Cannot read config file option in {}".format(err))
            uuid_missing = True
        except KeyError as err:
            sys.stderr.write("ERROR: Cannot read config file option in {}".format(err))
            uuid_missing = True

        # if no UUID exists,
        if uuid_missing:
            self.server_UUID = uuid.uuid4()
            sila2_config['server'] = {}
            sila2_config['server']['UUID'] = str(self.server_UUID)

            with open(config_filename, 'w') as file_config:
                sila2_config.write(file_config)
                
        logging.debug("Server UUID: {server_uuid}".format(server_uuid=self.server_UUID))
