"""_____________________________________________________________________

:PROJECT: SiLA2_python

*SiLA2 python3 library setup*

:details: SiLA2 python3 libray setup. This installs the SiLA2 Python3 library.
          
:authors: mark doerr (mark@uni-greifswald.de)
          Maximilian Schulz (max@unitelabs.ch)
          
:date: (creation)          

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.1.7"

import os
import sys

from setuptools import setup, find_packages

pkg_name = 'sila2lib'

def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read()


setup(name=pkg_name,
    version=__version__,
    description='sila2lib - a SiLA 2 python3 library ',
    long_description=read('README.md'),
    author=' Mark Doerr, Timm Severin (timm.severin@tum.de), Robert Giessmann (robert.giessmann@tu-berlin.de), Maximilian Schulz (max@unitelabs.ch)',
    author_email='mark.doerr@uni-greifswald.de',
    keywords=('SiLA 2, lab automation,  laboratory, instruments,' 
              'experiments, evaluation, visualisation, serial interface, robots'),
    url='https://gitlab.com/SiLA2/sila_python',
    license='MIT',
    packages=find_packages(),
    install_requires = ["grpcio>=1.7", "grpcio-tools", "lxml", "zeroconf"],
    extras_require={
        'com':  ["pyserial"]
    }
    test_suite='',
    classifiers=[  'License :: OSI Approved :: MIT License',
                   'Intended Audience :: Developers',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Utilities',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator',
                   'Topic :: Scientific/Engineering :: Information Analysis'],
      include_package_data=True,
      package_data={pkg_name: ['std_features/*.xml',
                               'std_features/*.pickle',
                               'schema/*.xsd', ]},
      )
