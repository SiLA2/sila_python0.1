# sila_python todo list

v0.1.6
    1. check parameters
    1. test generation
    1. sila client: move parts to silalib 
    1 .  response gen.
    
    1. pypi package

v0.2
    1. improve/create unittests for all crucial components of sila_python (sila_server, sila_client, codegenerator parts )
    
    1. check conformity with current SiLA specification (Part A and B)
    
    1. package resources vs pickle
    
    1. (add git submodules) - shall we really ?
    
    1. encryption (TLS/SSL ?) 
       conformity with specification ?
       interoperability with C# and JAVA ?
    
    1. zeroconfig
       “{UUID}._sila._tcp.local.” s. Min#102
       conformity with specification ?
       interoperability with C# and JAVA ?
       implement auto detection (s. client code)
    
    1. error handling / standard error handling
        error handling -> sila_library (s. Min#101)
        according to standard ?
        error handling examples in implementation
        constraints

    1. meta data implementation
       - implementation of a locking feature
    
    1. support for all SiLA standard data types
    
    1. support for constraints
    
    1. large binary implementation (s. Hackathon #12)
    
    1. codegenerator: prevent overwriting
    
    1. sila_library: serial/COM interface ( ASCII / binary ) 
           
    1. rename server -> feature
    
    1. how to handle units best ?
    
    testing
    
    1. if generator is working again,
        compile all examples before making 0.2 release
       
    1. test config file settings
    
    1. test observable commands
    
    1. test observable properties
      
    1. test installer on Raspberry pi and Win10
    
    1. full feature test (s. Stefan Koch)
    
    1. observable commands / properties


## codegenerator
    
    1. check that namespaces are handled correctly !

    1. codgen options 
       -p project file (allows any project_definition JSON filename) 
       -j javascript output
       - multiparameters ...
       - move framework proto files from sila2lib to project file

   - prevent overwriting

    1. correct command line parsing in templates 
    
## repo cleanup
 - submodules
    reference commit hash (s. Min#102)
 - unittests
 
## implement std features

    1. Init
    1. run control Start/Stop
    1. Rename server
    
    
## examples

 - all datatypes
 - thermometer
 - pump
 - shaker
 - incubator
 - stacker
 - balance
 - dynamic properties
 - dynamic commands
 - encryption
 
 
## moving from XML to protobuf 

- examples

- same as error messages (using one mechanism)
- keeping XML file for every feature either as text file or included as resource
- consitency (problem of Stefan)
- reduction of depandancies (XML parser)
- speed / resources 

Proposal: 
protobuf structure (in Framework proto)


