# observable_commands_demo

This demo illustrates how to work with observable commands

To test this example, follow these steps:

  1. activate your virtual environment
  1. change to the demo directory: `cd observable_commands_demo`
  1. start the server: `python3 HelloSiLA2_server.py`
  1. start the testclient in annother shell/shell: `python3 HelloSiLA2_testclient.py`
