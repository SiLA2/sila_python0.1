# HelloSiLA2 examples

## HelloSiLA2_minimal
This is a bare minimum example of what you need to establish a 
SiLA2 server-client communication.

To test the example, follow these steps:

  1. activate your virtual environment
  1. change to the demo directory: `cd HelloSiLA2_minimal`
  1. start the server: `python3 HelloSiLA2_server.py`
  1. start the testclient in annother shell/shell: `python3 HelloSiLA2_testclient.py`


## HelloSiLA2_full
This is a more comprehensive HelloSiLA2 example, demonstrating also the SiLAService functionality.

  1. activate your virtual environment
  1. change to the demo directory: `cd HelloSiLA2_full`
  1. start the server: `python3 HelloSiLA2_server.py`
  1. start the testclient in annother shell/shell: `python3 HelloSiLA2_testclient.py`
