"""
________________________________________________________________________

:PROJECT: SiLA2_python

*greetingprovider_server_simulation *

:details: greetingprovider_server_simulation: Minimum Feature Definition as example. Provides a Greeting. 
           
:file:    greetingprovider_server_simulation.py
:authors: ben lear

:date: (creation)          2019-02-03T00:28:29.025844
:date: (last modification) 2019-02-03T00:28:29.025844

.. note:: Code generated automatically by SiLA2codegenerator v0.1.6!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"


import logging
# importing protobuf and gRPC handler/stubs
import sila2lib.SiLAFramework_pb2 as fwpb2
import GreetingProvider_pb2 as pb2
import GreetingProvider_pb2_grpc as pb2_grpc


class GreetingProviderSimulation():
    """ GreetingProviderSimulation - Minimum Feature Definition as example. Provides a Greeting """
    def __init__ (self):
        """ GreetingProviderSimulation class initialiser """
        logging.debug("init class: GreetingProviderSimulation ")


    def SayHello(self, request, context):
        """Does what it says
            :param request: gRPC request
            :param context: gRPC context
            :param request.Name: Your Name

        """
        logging.debug("SayHello - Mode: simulation ")

        return_val = request.Name.value
        return pb2.SayHello_Responses(Greeting=fwpb2.String(value="DEFAULTstring" + return_val))

    def Get_StartYear(self, request, context):
        """The year the server has been started
            :param request: gRPC request
            :param context: gRPC context
            :param response.StartYear: The year the server has been started

        """
        logging.debug("Get_StartYear - Mode: simulation ")

        return_val = 2020
        return pb2.StartYear_Responses( StartYear=fwpb2.Integer(value=return_val) )




