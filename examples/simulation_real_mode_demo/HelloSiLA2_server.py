#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelloSiLA2 simulation - real mode implementation*

:details: HelloSiLA2: simulation - real mode implementation. 
          It starts illustrates, how to switch between simulation and real mode. 
           
:file:    HelloSiLA2.py
:authors: Mark Doerr 
          Maximilian Schulz ()

:date: (creation)          20181230
:date: (last modification) 20190123

.. note:: - 
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.6"

import os
import sys
import logging
import argparse

import sila2lib.sila_server as slss

import GreetingProvider_pb2_grpc as gp_pb_grpc
import GreetingProvider_pb2 as pb2
import sila2lib.SiLAFramework_pb2 as fwpb2

from GreetingProvider_simulation import GreetingProviderSimulation
from GreetingProvider_real import GreetingProviderReal

class GreetingProvider(gp_pb_grpc.GreetingProviderServicer):
    """ GreetingProvider - Minimum Feature Definition as example. Provides a Greeting """
    def __init__ (self):
        """ GreetingProvider class initialiser """
        logging.debug("init class: GreetingProvider servicer ")
        
        # if self.implementation is set to None, it will use
        # the fallback simulation mode as default
        # if required, one could also create a simulation module and set this to the default implementation, like:
        #~ self.injectImplementation(GreetingProviderSimulation())
        
        self.implementation = None # this corresponds to the simple, fallback simulation mode
    
    # dependency injection - setter injection s. https://en.wikipedia.org/wiki/Dependency_injection     
    def injectImplementation(self, implementation):
        self.implementation = implementation
        
    def SayHello(self, request, context):
        """Does what it says
        :param Name: Your Name
        Name - constraints: 
        Name - max lenth: -1
        """
        if self.implementation is not None: 
            return self.implementation.sayHello(request, context)
        else:
            return pb2.SayHello_Responses(Greeting=fwpb2.String(value="Sim fallback:Greeting Hello SiLA2 world: {} !".format(request.Name.value) ))
        
    def Get_StartYear(self, request, context):
        """The year the server has been started
            :param request: gRPC request
            :param context: gRPC context
        """
        if self.implementation is not None:
            return self.implementation.get_StartYear(request, context)
        else:
            return pb2.StartYear_Responses(StartYear=fwpb2.Integer(value=2018))

class HelloSiLA2Server(slss.SiLA2Server):
    """ Class doc """
    def __init__ (self, parsed_args=None):
        super().__init__(name=parsed_args.servername,
                         description=parsed_args.description,
                         version=__version__,
                         uuid=None,
                         vendor_url="lara.uni-greifswald.de",
                         ip='127.0.0.1', port=55001, 
                         key='sila_server.key', cert='sila_server.crt' )
                             
        self.GreetingProvider_servicer = GreetingProvider() # this uses the in-build/default simulation mode
    
        gp_pb_grpc.add_GreetingProviderServicer_to_server(self.GreetingProvider_servicer, self.grpc_server)
        
        # registering 'GreetingProvider' feature
        self.addFeature('GreetingProvider', '.')
        
        self.run() 
        
    def switchToSimMode(self):
        """ switching to simulation mode"""
        self.simulation_mode = True
        self.GreetingProvider_servicer.injectImplementation(GreetingProviderSimulation())
        success = True
        logging.debug("switch sim {}".format(success) )

    def switchToRealMode(self):
        """please overwrite this method in your server !"""
        self.simulation_mode = False
        self.GreetingProvider_servicer.injectImplementation(GreetingProviderReal())
        success = True
        logging.debug("switch real {}".format(success) )

        
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    # parsing command line
    help = "SiLA2 service: HelloSiLA2"
    parser = argparse.ArgumentParser(description="A SiLA2 service: HelloSiLA2")
    parser.add_argument('-s','--servername', action='store',
                         default="HelloSiLA2full", help='start SiLA service with [servername]' )
    parser.add_argument('-d','--description', action='store',
                         default="This is a test hello-SiLA2 service", help='SiLA server description' )
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    parsed_args = parser.parse_args()
    
    # running the server
    if parsed_args.servername :
        logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.servername))
        
        sila_server = HelloSiLA2Server(parsed_args=parsed_args)
        
