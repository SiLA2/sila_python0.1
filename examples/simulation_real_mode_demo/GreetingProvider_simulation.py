"""
________________________________________________________________________

:PROJECT: SiLA2_python

*greetingprovider_server_simulation *

:details: greetingprovider_server_simulation: Minimum Feature Definition as example. Provides a Greeting. 
           
:file:    greetingprovider_server_simulation.py
:authors: ben lear

:date: (creation)          20181230
:date: (last modification) 20190123

.. note:: code generated automatically by SiLA2codegenerator !


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.6"


import logging
# importing protobuf definitions
import sila2lib.SiLAFramework_pb2 as fwpb2
import GreetingProvider_pb2 as pb2

class GreetingProviderSimulation:
    """ GreetingProvider - Minimum Feature Definition as example. Provides a Greeting """
    def __init__ (self):
        """ GreetingProvider class initialiser """
        logging.debug("init implementation class: GreetingProvider ")

    def sayHello(self, request, context):
        """Does what it says
        :param Name: Your Name
        Name - constraints: 
        Name - max lenth: -1
        """
        logging.debug("SayHello - Mode: simulation ")

        return pb2.SayHello_Responses(Greeting=fwpb2.String(value="Sim:Greeting Hello SiLA2 world: {} !".format(request.Name.value) ))

    def get_StartYear(self, request, context):
        """The year the server has been started
            :param request: gRPC request
            :param context: gRPC context
        """
        logging.debug("Get_StartYear - Mode: simulation ")
        
        return pb2.StartYear_Responses(StartYear=fwpb2.Integer(value=2019))
