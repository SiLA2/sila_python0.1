#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelloSiLA2 client *

:details: HelloSiLA2 client: This is a test hello-SiLA2 service. 
           
:file:    HelloSiLA2_client.py
:authors: ben lear

:date: (creation)          20181230
:date: (last modification) 20190103

.. note:: - 
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.3"

import logging
import argparse
import time

from threading import Thread

import grpc

from sila2lib.sila_service_detection import SiLA2ServiceDetection

import sila2lib.SiLAFramework_pb2 as fwpb2

from sila2lib.std_features import SiLAService_pb2 as spb2
from sila2lib.std_features import SiLAService_pb2_grpc as spb2g
from sila2lib.std_features import SimulationController_pb2 as scpb2
from sila2lib.std_features import SimulationController_pb2_grpc as scpb2g

import GreetingProvider_pb2
import GreetingProvider_pb2_grpc


class HelloSiLA2Client:
    """ Class doc """
    def __init__ (self, service_name=None, sila_server_name="localhost", 
                  ip='127.0.0.1', port=50001, cert=None):
        """ Class initialiser 
            param cert=None: server certificate filename, e.g. 'sila_server.crt'  """
        
        # auto service detection
        if service_name is not None:
            # automatic connection
            connect_param = SiLA2ServiceDetection.findServiceByName(service_name)
            
            sila_server_name = connect_param['server_name']
            port = connect_param['port']
            
        if sila_server_name != "" and port != 0 :
            # manual connection
            if cert is not None: # encryption
                credentials = grpc.ssl_channel_credentials(open(cert, 'rb').read()) #grpc.ssl_channel_credentials(root_certificates=trusted_certs)
                self.channel = grpc.secure_channel(':'.join((sila_server_name, str(port) )), credentials)
            else:
                self.channel = grpc.insecure_channel(':'.join((sila_server_name, str(port))))
                
        # this is required to access the SiLAService functionality
        self.SiLAService_serv_stub = spb2g.SiLAServiceStub(self.channel)
        self.simulation_ctrl_stub = scpb2g.SimulationControllerStub(self.channel)
                
        self.GreetingProvider_serv_stub = GreetingProvider_pb2_grpc.GreetingProviderStub(self.channel)

    
    def run(self):
        try:
            print("SiLA client / client HelloSiLA2 -------- commands ------------")
            
            # --> put your remote calls here, e.g. (this is really only an example, please use the your corresponding calls)
            
            # calling SiLAService
            response = self.SiLAService_serv_stub.Get_ImplementedFeatures(spb2.ImplementedFeatures_Parameters() )
            for feature_id in response.ImplementedFeatures :
                logging.debug("implemented feature:{}".format(feature_id.Identifier.value) )
            
            try:
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                   QualifiedFeatureIdentifier=spb2.DataType_Identifier(Identifier=fwpb2.String(value="SiLAService") )) )
                #~ logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            try: 
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                    QualifiedFeatureIdentifier=spb2.DataType_Identifier(Identifier=fwpb2.String(value="NoFeature") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
                
            try: 
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                    QualifiedFeatureIdentifier=spb2.DataType_Identifier(Identifier=fwpb2.String(value="GreetingProvider") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            response = self.SiLAService_serv_stub.Get_ServerDisplayName(spb2.ServerDisplayName_Parameters() )
            logging.debug("display name:{}".format(response.ServerDisplayName.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerDescription(spb2.ServerDescription_Parameters())
            logging.debug("description:{}".format(response.ServerDescription.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerVersion(spb2.ServerVersion_Parameters())
            logging.debug("version:{}".format(response.ServerVersion.value) )
            
            # --> calling GreetingProvider
            
            # simulation mode calls
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="fallback sim Hello")))
                logging.debug("SayHello response: {}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            
            # change to real mode    
            try :
                response = self.simulation_ctrl_stub.StopSimulationMode(scpb2.StopSimulationMode_Parameters())
                logging.debug("StopSimMode  response:{}".format(response.SimulationStopped.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            
            # real  mode calls    
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="real Hello")))
                logging.debug("SayHello response: {}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            # change back to sim  mode    
            try :
                response = self.simulation_ctrl_stub.StartSimulationMode(scpb2.StartSimulationMode_Parameters())
                logging.debug("StopSimMode  response:{}".format(response.SimulationStarted.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="Sim Hello")))
                logging.debug("SayHello response: {}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
            

            
        except grpc._channel._Rendezvous as err :
            self.error_handler(err)

    def error_handler(self, errors):
        logging.error(errors._state)    

if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    help = "SiLA2 service: HelloSiLA2"
    
    parser = argparse.ArgumentParser(description="A SiLA2 client: HelloSiLA2")
    
    parser.add_argument('-s','--sila-service', action='store', dest='servicename',
                         help='SiLA service to connect with [servicename]' ) # default="HelloSiLA2"

    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
        
    parsed_args = parser.parse_args()
    
    if parsed_args.servicename :
        logging.info("starting SiLA2 client with service name: {servicename}".format(servicename=parsed_args.servicename))
        #~ serve(parsed_args.servicename)
    else :
        logging.info("starting SiLA2 client: HelloSiLA2")
        sila_client = HelloSiLA2Client(ip='127.0.0.1', port=55001 )
        sila_client.run()
