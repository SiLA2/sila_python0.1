# simulation_real_mode_demo

This demo illustrates SiLA2 server simulation / real Mode switching

To test this example, follow these steps:

  1. activate your virtual environment
  1. change to the demo directory: `cd simulation_real_mode_demo`
  1. start the server: `python3 HelloSiLA2_server.py`
  1. start the testclient in annother shell/shell: `python3 HelloSiLA2_testclient.py`
