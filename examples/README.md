# SiLA2 examples
All examples were generated using the powerful __sila2codegenerator__ in 
python_sila/sila_tools/sila2codegenerator.
Please have a look there for more information.

For the instructions how to run the demo, 
please refer to the __README.md__ files within the demo directory

## HelloSiLA2 examples

### HelloSiLA2_minimal
This is a bare minimum example of what you need to establish a 
SiLA2 server-client communication.

### HelloSiLA2_full
This is a more comprehensive HelloSiLA2 example, demonstrating also the SiLAService functionality.

## simulation_real_mode_demo

This demo illustrates SiLA2 server simulation / real Mode switching

## observable_commands_demo

This demo illustrates how to work with observable commands

## dynamic_properties_demo

This demo illustrates how to work with dynamic/observable properties

## all_datatypes_demo

This demo shows the usage of all basic datatypes
